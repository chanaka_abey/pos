<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="resources/images/favicon.png">
    <title>POS SYSTEM</title>
    <link rel="stylesheet" href="resources/css/loginstyle.css">
    <!-- Bootstrap Core CSS -->
    <link href="resources/css/bootstrap22.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="resources/css/style22.css" rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <link href="resources/css/blue22.css" id="theme" rel="stylesheet">

</head>
<body>

<div id="particles-js">
     <!--    <div class="login-register" style="background-image:url(resources/images/login-register.jpg);">  -->
          <center>
            <div class="login-box card" style="margin-top:10%;margin-left:35%;position:absolute;">
            <div class="card-block">
                <form:form class="form-horizontal form-material" action="login.htm" method="POST" commandName="loginForm">
                    <h3 class="box-title m-b-20">Log in</h3>
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <form:input placeholder="Username" class="form-control" path="username"/> 
                            <FONT color="red"><form:errors path="username" /></FONT>
                         </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <form:password placeholder="Password" class="form-control"  path="password"/> 
                            <FONT color="red"><form:errors path="password" /></FONT>
                            
                        </div>
                    </div>
                    <%-- <div class="form-group">
                    	<div class="col-xs-12">
                    	<form:select class="form-control custom-select" path="role">
                         <option>Loggin as</option>
                         <option>Admin</option>
                         <option>User</option>
                         </form:select>
                    	</div>
                    </div> --%>
                    
                    
                    <div class="form-group">
                        <div class="col-md-12">
                            <div class="checkbox checkbox-primary pull-left p-t-0">
                                <input id="checkbox-signup" type="checkbox">
                                <label for="checkbox-signup"> Remember me </label>
                            </div> <a href="javascript:void(0)" id="to-recover" class="text-dark pull-right"><i class="fa fa-lock m-r-5"></i> Forgot pwd?</a> </div>
                    </div>
                    
                   <label for="checkbox-signup"><FONT color="red">${myModel.err}</FONT></label> 
                    
                    <div class="form-group text-center m-t-20">
                        <div class="col-xs-12">
                            <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Log In</button>
                        </div>
                    </div>
                    
                    
                </form:form>
                <%-- <form class="form-horizontal" id="recoverform" action="index.htm">
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <h3>Recover Password</h3>
                            <p class="text-muted">Enter your Email and instructions will be sent to you! </p>
                        </div>
                    </div>
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <input class="form-control" type="text" required="" placeholder="Email"> </div>
                    </div>
                    <div class="form-group text-center m-t-20">
                        <div class="col-xs-12">
                            <button class="btn btn-primary btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Reset</button>
                        </div>
                    </div>
                </form> --%>
            </div>
          </div>
          </center>
        </div>
       
        
   
    
    <!-- particles.js lib - https://github.com/VincentGarreau/particles.js --> 
<!-- <script src="http://cdn.jsdelivr.net/particles.js/2.0.0/particles.min.js"></script>  -->
<!-- stats.js lib --> 
<!-- <script src="http://threejs.org/examples/js/libs/stats.min.js"></script> -->
    
    <script  src="resources/js/particles.min.js"></script>
    <script  src="resources/js/loginindex.js"></script>
    <script src="resources/js/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="resources/js/tether.min.js"></script>
    <script src="resources/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="resources/js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="resources/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="resources/js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="resources/js/sticky-kit.min.js"></script>
    <script src="resources/js/jquery.sparkline.min.js"></script>
    <!--Custom JavaScript -->
    <script src="resources/js/custom.min.js"></script>
    <!-- ============================================================== -->
    <!-- Style switcher -->
    <!-- ============================================================== -->
    <script src="resources/js/jQuery.style.switcher.js"></script>
    
    </body>