<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<script>

var itemid = document.getElementById('id').value;
if(itemid == 0 || itemid=="" || itemid==null){
	document.getElementById("btn_change").style.display = "none";
	document.getElementById("btn_save").innerText = "Create";
	
	document.getElementById("header_id").innerText = "Create Item";
	document.getElementById("breadcrumb_id").innerText = "Add New Item";
	
}else{
	document.getElementById("btn_change").style.display = "initial";
	document.getElementById('btn_save').innerText = "Update";
	document.getElementById("header_id").innerText = "Edit Item";
	document.getElementById("breadcrumb_id").innerText = "Edit Item";
}
</script>


<script>
function stockupdate(){
	var itemid = document.getElementById('itemid').value;
	var itemname = document.getElementById('itemname').value;
	var category = document.getElementById('category').value;
	var unitprice = document.getElementById('unitprice').value;
	var salesprice = document.getElementById('salesprice').value;
	var quantity = document.getElementById('quantity').value;
	var reorderlevel = document.getElementById('reorderlevel').value;
	
	/* if(itemid ="" || itemname ="" || category="" || ) */
	$.ajax({
		method:"POST",
		url: 'getstockpage.htm',
		data: {
			itemid : itemid,
			itemname : itemname,
			category : category,
			unitprice : unitprice,
			salesprice : salesprice,
			quantity : quantity,
			reorderlevel : reorderlevel
		},
		dataType: "json",
		cache: false,
		success: function(){
			
			alert("success");
		},
		error: function(){
			//alert("Successfully Stock Update..! " );
			//location.reload();
		}
	});
	
}

function addnewitemfunc(){
	
	var id = document.getElementById('id').value;
	var itemid = document.getElementById('itemid').value;
	var itemname = document.getElementById('itemname').value;
	var category = document.getElementById('category').value;
	var unitprice = document.getElementById('unitprice').value;
	var salesprice = document.getElementById('salesprice').value;
	var quantity = document.getElementById('quantity').value;
	var position = document.getElementById('position').value;
	var reorderlevel = document.getElementById('reorderlevel').value;
	
	//alert("id " + id + " itemid " +itemid +" itemname " + itemname +" category " + category +" unitprice " + unitprice + " salesprice " +salesprice+" quantity " +quantity+" reorderlevel " + reorderlevel);
	
	$.ajax({
		method:"POST",
		url: 'saveItem',
		data: {
			id : id,
			itemid : itemid,
			itemname : itemname,
			category : category,
			position : position,
			unitprice : unitprice,
			salesprice : salesprice,
			quantity : quantity,
			reorderlevel : reorderlevel
		},
		dataType: "json",
		cache: false,
		success: function(){
			
		//	alert("success");
		},
		error: function(){
		//	alert("Please Enter madnetary fields..! " );
			//location.reload();
		}
	});
}

function changeitemquantity(){
	
	
	var id = document.getElementById('id').value;
	var itemid = document.getElementById('itemid').value;
	var itemname = document.getElementById('itemname').value;
	var category = document.getElementById('category').value;
	var position = document.getElementById('position').value;
	var unitprice = document.getElementById('unitprice').value;
	var salesprice = document.getElementById('salesprice').value;
	var quantity = document.getElementById('quantity').value;
	var reorderlevel = document.getElementById('reorderlevel').value;
	
	//alert("id " + id + " itemid " +itemid +" itemname " + itemname +" category " + category +" unitprice " + unitprice + " salesprice " +salesprice+" quantity " +quantity+" reorderlevel " + reorderlevel);
	if(quantity == "" || quantity == null){
		alert("Please Edit Quantity..!");
	}else{
		
	
	$.ajax({
		method:"POST",
		url: 'changeItem',
		data: {
			id : id,
			itemid : itemid,
			itemname : itemname,
			category : category,
			position : position,
			unitprice : unitprice,
			salesprice : salesprice,
			quantity : quantity,
			reorderlevel : reorderlevel
		},
		dataType: "json",
		cache: false,
		success: function(){
			
			alert("success");
		},
		error: function(){
			//alert("Error " );
			loadPage('items.htm', 'container');
			//location.reload();
		}
	});
	
	}
	
}

</script>


<br>
<nav class="breadcrumb" style="background-color:#c0d8f1;">
                                    <a class="breadcrumb-item" href="index.htm">Home</a>
                                    <a class="breadcrumb-item" href="#" onclick="loadPage('items.htm', 'container');">Items</a>
                                    <span class="breadcrumb-item active" id="breadcrumb_id">Add new Item</span>
      </nav>
  <br>

<div class="row">
                    <!-- Column -->
                    <div class="col-lg-4 col-md-6">
                        <div class="card">
                        	<a href="#" onclick="loadPage('add_new_item.htm', 'container');">
                            <div class="card-body">
                                <div class="d-flex flex-row">
                                    <div class="round round-lg align-self-center round-info"><i class="mdi mdi-tag-plus"></i></div>
                                    <div class="m-l-10 align-self-center">
                                        
                                        <h5 class="text-muted m-b-0">Add New Item</h5></div>
                                </div>
                            </div>
                            </a>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="col-lg-4 col-md-6">
                        <div class="card">
                         <a href="#" onclick="loadPage('items.htm', 'container');">
                            <div class="card-body">
                                <div class="d-flex flex-row">
                                    <div class="round round-lg align-self-center round-warning"><i class="mdi mdi-tag-multiple"></i></div>
                                    <div class="m-l-10 align-self-center">
                                        
                                        <h5 class="text-muted m-b-0">Item List</h5></div>
                                </div>
                            </div>
                            </a>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <!-- chanaka implemtation -->
                   <!--  <div class="col-lg-3 col-md-6">
                    <a href="#" onclick="loadPage('stock_reports.htm', 'container');">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex flex-row">
                                    <div class="round round-lg align-self-center round-primary"><i class="mdi mdi-file-chart"></i></div>
                                    <div class="m-l-10 align-self-center">
                                        
                                        <h5 class="text-muted m-b-0">Stock Reports</h5></div>
                                </div>
                            </div>
                        </div>
                        </a>
                    </div> -->
                    <!-- Column -->
                    <!-- Column -->
                    <div class="col-lg-4 col-md-6">
                        <div class="card">
                        <a href="#" onclick="loadPage('reorder_list.htm', 'container');">
                            <div class="card-body">
                                <div class="d-flex flex-row">
                                    <div class="round round-lg align-self-center round-danger"><i class="mdi mdi-format-line-weight"></i></div>
                                    <div class="m-l-10 align-self-center">
                                        
                                        <h5 class="text-muted m-b-0">Reorder List</h5></div>
                                </div>
                            </div>
                            </a>
                        </div>
                    </div>
                    <!-- Column -->
                </div>
                
                
                
             <div class="row">
                    <div class="col-12">
                       
                        	<div class="card card-outline-info">
                        	<div class="card-header">
                                <h4 class="m-b-0 text-white" id="header_id">Add / Update Item</h4>
                            </div>
                        
                            <div class="card-body">
                                
                               
                                <form class="form-horizontal p-t-20" novalidate action="saveItem">
                                    <div class="form-group row">
                                        <label  for="exampleInputuname3" class="col-sm-3 control-label">Item ID<span class="text-danger">*</span></label>
                                        <div class="col-sm-9">
                                            <div class="controls">
                                            <input id="id" class="form-control" value="${myModel.itemobject.id}" style="display: none;"/>
                                            <input  id="itemid" type="text" name="text" class="form-control" value="${myModel.itemobject.itemid}" data-validation-required-message="Item ID Cannot be empty"/> </div>
                                        	<errors  cssClass="error"/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label  for="exampleInputEmail3" class="col-sm-3 control-label">Item Name<span class="text-danger">*</span></label>
                                        <div class="col-sm-9">
                                            <div class="controls">
                                            <input  id="itemname" type="text" name="text" class="form-control" value="${myModel.itemobject.itemname}" data-validation-required-message="Item Name Cannot be empty"/> </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label  for="web" class="col-sm-3 control-label">Category</label>
                                        <div class="col-sm-9">
                                            <input  id="category" type="text" name="text" class="form-control" value="${myModel.itemobject.category}"/>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group row">
                                        <label  for="web" class="col-sm-3 control-label">Position</label>
                                        <div class="col-sm-9">
                                            <input  id="position" type="text" name="text" class="form-control" value="${myModel.itemobject.position}"/>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group row">
                                        <label  for="unitprice" class="col-sm-3 control-label">Unit Price<span class="text-danger">*</span></label>
                                        <div class="col-sm-9">
                                        	<div class="controls">
                                            <div class="input-group"> <span class="input-group-addon">Rs</span>
                                            <input  id="unitprice" type="number" name="onlyNum" class="form-control" value="${myModel.itemobject.unitprice}" data-validation-required-message="Unit price Cannot be empty" min="1"/> <span class="input-group-addon">.00</span> </div>
                                       		</div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label  for="salesprice" class="col-sm-3 control-label">Sales Price<span class="text-danger">*</span></label>
                                        <div class="col-sm-9">
                                            <div class="controls">
                                            <div class="input-group"> <span class="input-group-addon">Rs</span>
                                            <input  id="salesprice" type="number" name="onlyNum" class="form-control" value="${myModel.itemobject.salesprice}" data-validation-required-message="Sales price Cannot be empty" min="1"/> <span class="input-group-addon">.00</span> </div>
                                        	</div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label  for="exampleInputEmail3" class="col-sm-3 control-label">Quantity<span class="text-danger">*</span></label>
                                        <div class="col-sm-9">
                                            <div class="controls">
                                            <input  id="quantity" type="number" name="onlyNum" class="form-control" placeholder="${myModel.itemobject.quantity} available in stock. please update new quantity here" value="" data-validation-required-message="Quantity cannot be empty" min="0"/> </div>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group row">
                                        <label  for="exampleInputEmail3" class="col-sm-3 control-label">Reorder level<span class="text-danger">*</span></label>
                                        <div class="col-sm-9">
                                            <div class="controls">
                                           <input  id="reorderlevel" type="number" name="minNum" class="form-control" value="${myModel.itemobject.reorderlevel}" data-validation-required-message="Reorder level cannot be empty" min="1"/></div>
                                        </div>
                                    </div>
                                    
                                    <div class="text-xs-right">
                                        <button type="submit" id="btn_save" class="btn btn-info" id="saveItem" onclick="addnewitemfunc();stockupdate();" >Save</button>
                                        <button type="reset" class="btn btn-inverse">Cancel</button>
                                        
                                        <button type="button" id="btn_change" style="display: none;" class="btn btn-danger" onclick="changeitemquantity();">Change Qty</button>
                                    </div>
                                </form>
                            </div>
                            </div>
                        
                    </div>
                </div>  
                    
        
         <script>
    ! function(window, document, $) {
        "use strict";
        $("input,select,textarea").not("[type=submit]").jqBootstrapValidation(), $(".skin-square input").iCheck({
            checkboxClass: "icheckbox_square-green",
            radioClass: "iradio_square-green"
        }), $(".touchspin").TouchSpin(), $(".switchBootstrap").bootstrapSwitch();
    }(window, document, jQuery);
    </script>     
      