<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<br>

<nav class="breadcrumb" style="background-color:#c0d8f1;">
                                    <a class="breadcrumb-item" href="index.htm">Home</a>
                                    <a class="breadcrumb-item" href="#" onclick="loadPage('sales.htm', 'container');">Sales</a>
                                    <span class="breadcrumb-item active">Edit Sales</span>
      </nav>
  <br>

<div class="row">
                    <!-- Column -->
                    <div class="col-lg-3 col-md-6">
                        <div class="card">
                        	<a href="#" onclick="loadPage('sales.htm', 'container');">
                            <div class="card-body">
                                <div class="d-flex flex-row">
                                    <div class="round round-lg align-self-center round-info"><i class="ti-wallet"></i></div>
                                    <div class="m-l-10 align-self-center">
                                        
                                        <h5 class="text-muted m-b-0">Sales List</h5></div>
                                </div>
                            </div>
                            </a>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="col-lg-3 col-md-6">
                    <a href="#" onclick="loadPage('items.htm', 'container');">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex flex-row">
                                    <div class="round round-lg align-self-center round-warning"><i class="mdi mdi-cellphone-link"></i></div>
                                    <div class="m-l-10 align-self-center">
                                        
                                        <h5 class="text-muted m-b-0">Item List</h5></div>
                                </div>
                            </div>
                        </div>
                        </a>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="col-lg-3 col-md-6">
                        <div class="card">
                        <a href="#" onclick="loadPage('sales_reports.htm', 'container');">
                            <div class="card-body">
                                <div class="d-flex flex-row">
                                    <div class="round round-lg align-self-center round-primary"><i class="mdi mdi-cart-outline"></i></div>
                                    <div class="m-l-10 align-self-center">
                                        
                                        <h5 class="text-muted m-b-0">Sales Reports</h5></div>
                                </div>
                            </div>
                            </a>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="col-lg-3 col-md-6">
                        <div class="card">
                        <a href="#" onclick="loadPage('invoice_reports.htm', 'container');">
                            <div class="card-body">
                                <div class="d-flex flex-row">
                                    <div class="round round-lg align-self-center round-danger"><i class="mdi mdi-bullseye"></i></div>
                                    <div class="m-l-10 align-self-center">
                                        
                                        <h5 class="text-muted m-b-0">Invoice Reports</h5></div>
                                </div>
                            </div>
                            </a>
                        </div>
                    </div>
                    <!-- Column -->
                </div>
                
                
                
             <div class="row">
                    <div class="col-12">
                       
                        	<div class="card card-outline-info">
                        	<div class="card-header">
                                <h4 class="m-b-0 text-white">Edit Sales</h4>
                            </div>
                        
                            <div class="card-body">
                                
                               
                                <form:form class="form-horizontal p-t-20" modelAttribute="sales" method="post" action="saveSales">
                                    <div class="form-group row" >
                                        <form:label path="invoicenumber" for="exampleInputuname3" class="col-sm-3 control-label">Invoice No<span class="text-danger">*</span></form:label>
                                        <div class="col-sm-9">
                                            <div class="controls">
                                            <form:hidden path="id" value="${sales.id}"/>
                                            <form:input path="invoicenumber" type="text" name="text" class="form-control" value="${sales.invoicenumber}" data-validation-required-message="Item ID Cannot be empty" /> </div>
                                        	<form:errors path="invoicenumber" cssClass="error"/>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group row">
                                        <form:label path="itemid" for="exampleInputEmail3" class="col-sm-3 control-label">Item ID<span class="text-danger">*</span></form:label>
                                        <div class="col-sm-9">
                                            <div class="controls">
                                            <form:input path="itemid" type="text" name="text" class="form-control" value="${sales.itemid}" data-validation-required-message="Item Name Cannot be empty" /> </div>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group row">
                                        <form:label path="itemname" for="exampleInputEmail3" class="col-sm-3 control-label">Item Name<span class="text-danger">*</span></form:label>
                                        <div class="col-sm-9">
                                            <div class="controls">
                                            <form:input path="itemname" type="text" name="text" class="form-control" value="${sales.itemname}" data-validation-required-message="Item Name Cannot be empty" /> </div>
                                        </div>
                                    </div>
                                     <div class="form-group row">
                                        <form:label path="purchasedate" for="exampleInputEmail3" class="col-sm-3 control-label">Purchase Date<span class="text-danger">*</span></form:label>
                                        <div class="col-sm-9">
                                            <div class="controls">
                                            <form:input path="purchasedate" type="text" name="text" class="form-control" value="${sales.purchasedate}" data-validation-required-message="Item Name Cannot be empty"/> </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <form:label path="purchasetime" for="exampleInputEmail3" class="col-sm-3 control-label">Purchase Time<span class="text-danger">*</span></form:label>
                                        <div class="col-sm-9">
                                            <div class="controls">
                                            <form:input path="purchasetime" type="time" name="time" class="form-control" value="${sales.purchasetime}" data-validation-required-message="Item Name Cannot be empty"/> </div>
                                        </div>
                                    </div>
                                   
                                    <div class="form-group row">
                                        <form:label path="salesprice" for="salesprice" class="col-sm-3 control-label">Sales Price<span class="text-danger">*</span></form:label>
                                        <div class="col-sm-9">
                                            <div class="controls">
                                            <div class="input-group"> <span class="input-group-addon">Rs</span>
                                            <form:input path="salesprice" type="number" name="onlyNum" class="form-control" value="${sales.salesprice}" data-validation-required-message="Sales price Cannot be empty" min="1"/> <span class="input-group-addon">.00</span> </div>
                                        	</div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <form:label path="quantity" for="exampleInputEmail3" class="col-sm-3 control-label">Quantity<span class="text-danger">*</span></form:label>
                                        <div class="col-sm-9">
                                            <div class="controls">
                                            <form:input path="quantity" type="number" name="onlyNum" class="form-control" value="${sales.quantity}" data-validation-required-message="Quantity cannot be empty" min="1"/> </div>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group row">
                                        <form:label path="discount" for="exampleInputEmail3" class="col-sm-3 control-label">Discount<span class="text-danger">*</span></form:label>
                                        <div class="col-sm-9">
                                            <div class="controls">
                                           <form:input path="discount" type="number" name="minNum" class="form-control" value="${sales.discount}" data-validation-required-message="Reorder level cannot be empty" min="0"/></div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <form:label path="totalsalesprice" for="salesprice" class="col-sm-3 control-label">Total Sales Price<span class="text-danger">*</span></form:label>
                                        <div class="col-sm-9">
                                            <div class="controls">
                                            <div class="input-group"> <span class="input-group-addon">Rs</span>
                                            <form:input path="totalsalesprice" type="number" name="onlyNum" class="form-control" value="${sales.totalsalesprice}" data-validation-required-message="Sales price Cannot be empty" min="1"/> <span class="input-group-addon">.00</span> </div>
                                        	</div>
                                        </div>
                                    </div>
                                    
                                   <%--  <div class="form-group row">
                                        <form:label path="item_id" for="exampleInputEmail4" class="col-sm-3 control-label">Item_id</form:label>
                                        <div class="col-sm-9">
                                            <div class="controls">
                                            <form:input path="item_id" type="text" name="item_id" class="form-control" value="${sales.item_id}"/> </div>
                                        </div>
                                    </div> --%>
                                    
                                    <div class="text-xs-right">
                                        <button type="submit" class="btn btn-info" id="saveSales">Submit</button>
                                        <button type="reset" class="btn btn-inverse">Cancel</button>
                                    </div>
                                </form:form>
                            </div>
                            </div>
                        
                    </div>
                </div>  
                    
        
             
      