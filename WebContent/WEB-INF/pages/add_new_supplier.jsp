<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<script>

var id = document.getElementById("id").value;
if(id == 0 || id=="" || id==null){
	document.getElementById("addsupplierheaderid").innerText = "Add New Supplier";
	document.getElementById("addnersupplier_id").innerText = "Add New Supplier";
	
}else{
	document.getElementById("addnersupplier_id").innerText = "Edit Supplier";
	document.getElementById("addsupplierheaderid").innerText = "Edit Supplier";
}
</script> 

<script>
	$('#mdate').bootstrapMaterialDatePicker({
		weekStart : 0,
		time : false
	});
</script>


<br>

<nav class="breadcrumb" style="background-color:#c0d8f1;">
                                    <a class="breadcrumb-item" href="index.htm">Home</a>
                                    <a class="breadcrumb-item" href="#" onclick="loadPage('load_supplier_stocks.htm', 'container');">Suppliers</a>
                                    <span class="breadcrumb-item active" id="addsupplierheaderid">Add New Supplier</span>
      </nav>
  <br>

<div class="row" id="itemstat">
	
		<!-- Column -->
		<div class="col-lg-3 col-md-6">
			<div class="card">
			<a href="#" onclick="loadPage('add_new_supplier_stock.htm', 'container');">
				<div class="card-body">
					<div class="d-flex flex-row">
						<div class="round round-lg align-self-center round-primary">
							<i class="mdi mdi-store"></i>
						</div>
						<div class="m-l-10 align-self-center">

							<h5 class="text-muted m-b-0">Add new Stock</h5>
						</div>
					</div>
				</div>
				</a>
			</div>
		</div>
		<!-- Column -->
		<!-- Column -->
		<div class="col-lg-3 col-md-6">
			<div class="card">
				<a href="#" onclick="loadPage('add_new_supplier.htm', 'container');">
					<div class="card-body">
						<div class="d-flex flex-row">
							<div class="round round-lg align-self-center round-info">
								<i class="mdi mdi-truck"></i>
							</div>
							<div class="m-l-10 align-self-center">

								<h5 class="text-muted m-b-0">Add New Supplier</h5>
							</div>
						</div>
					</div>
				</a>
			</div>
		</div>
		<!-- Column -->
		<!-- Column -->
		<div class="col-lg-3 col-md-6">
			<div class="card">
			<a href="#" onclick="loadPage('suppliers.htm', 'container');">
				<div class="card-body">
					<div class="d-flex flex-row">
						<div class="round round-lg align-self-center round-danger">
							<i class="mdi mdi-truck-delivery"></i>
						</div>
						<div class="m-l-10 align-self-center">

							<h5 class="text-muted m-b-0">Supplier List</h5>
						</div>
					</div>
				</div>
				</a>
			</div>
		</div>
		<!-- Column -->
		<!-- Column -->
<!-- 		<div class="col-lg-3 col-md-6">
			<div class="card">
			<a href="#"  onclick="loadPage('supplier_reports.htm', 'container');">
				<div class="card-body">
					<div class="d-flex flex-row">
						<div class="round round-lg align-self-center round-warning">
							<i class="mdi mdi-chart-areaspline"></i>
						</div>
						<div class="m-l-10 align-self-center">

							<h5 class="text-muted m-b-0">Suppliers Reports</h5>
						</div>
					</div>
				</div>
				</a>
			</div>
		</div> -->
		<!-- Column -->
		
		
	</div>



<div class="row">
	<div class="col-12">

		<div class="card card-outline-info">
			<div class="card-header">
				<h4 class="m-b-0 text-white" id="addnersupplier_id">Add New Supplier</h4>
			</div>

			<div class="card-body">


				<form:form class="form-horizontal p-t-20" modelAttribute="suppliers"
					method="post" action="saveSupplier">
					<div class="form-group row">
						<form:label path="name" for="exampleInputuname3"
							class="col-sm-3 control-label">Supplier Name<span
								class="text-danger">*</span>
						</form:label>
						<div class="col-sm-9">
							<div class="controls">
								<form:hidden path="id" id="id" value="${supplier.id}" />
								<form:input path="name" type="text" name="text"
									class="form-control" value="${supplier.name}"
									data-validation-required-message="Item ID Cannot be empty" />
							</div>
							<form:errors path="name" cssClass="error" />
						</div>
					</div>

					<div class="form-group row">
						<form:label path="address" for="exampleInputEmail3"
							class="col-sm-3 control-label">Address<span
								class="text-danger">*</span>
						</form:label>
						<div class="col-sm-9">
							<div class="controls">
								<form:input path="address" type="text" name="text"
									class="form-control" value="${supplier.address}"
									data-validation-required-message="Item Name Cannot be empty" />
							</div>
						</div>
					</div>

					<div class="form-group row">
						<form:label path="telephone" for="exampleInputEmail3"
							class="col-sm-3 control-label">Telephone<span
								class="text-danger">*</span>
						</form:label>
						<div class="col-sm-9">
							<div class="controls">
								<form:input path="telephone" type="text" name="text"
									class="form-control" value="${supplier.telephone}"
									data-validation-required-message="Item Name Cannot be empty" />
							</div>
						</div>
					</div>

					<div class="form-group row">
						<form:label path="date" for="exampleInputEmail3"
							class="col-sm-3 control-label">Create Date<span
								class="text-danger">*</span>
						</form:label>
						<div class="col-sm-9">
							<div class="controls">
								<form:input path="date" type="text" placeholder="select date"
									id="mdate" class="form-control" value="${supplier.date}"
									data-validation-required-message="Item Name Cannot be empty" />

							</div>
						</div>
					</div>


					<div class="text-xs-right">
						<button type="submit" class="btn btn-info" id="saveSupplier">Submit</button>
						<button type="reset" class="btn btn-inverse">Cancel</button>
					</div>
				</form:form>
			</div>
		</div>

	</div>
</div>



