<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<br>

<nav class="breadcrumb" style="background-color:#c0d8f1;">
                                    <a class="breadcrumb-item" href="index.htm">Home</a>
                                    <a class="breadcrumb-item" href="#" onclick="loadPage('user.htm', 'container');">Users</a>
                                    <span class="breadcrumb-item active">Add New User</span>
      </nav>
  <br>

<div class="row">
                    <!-- Column -->
                    <div class="col-lg-6 col-md-8">
                        <div class="card">
                        	<a href="#">
                            <div class="card-body">
                                <div class="d-flex flex-row">
                                    <div class="round round-lg align-self-center round-info le"><i class="mdi mdi-account-plus"></i></div>
                                    <div class="m-l-10 align-self-center">
                                        
                                        <h5 class="text-muted m-b-0">Add New User</h5></div>
                                </div>
                            </div>
                            </a>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                    <div class="col-lg-6 col-md-8">
                        <div class="card">
                        <a href="#" onclick="loadPage('user.htm', 'container');">
                            <div class="card-body">
                                <div class="d-flex flex-row">
                                    <div class="round round-lg align-self-center round-warning le"><i class="mdi mdi-account-multiple"></i></div>
                                    <div class="m-l-10 align-self-center">
                                        
                                        <h5 class="text-muted m-b-0">User List</h5></div>
                                </div>
                            </div>
                            </a>
                        </div>
                    </div>
                    <!-- Column -->
                    <!-- Column -->
                   <!--  chanaka implementation -->
                    <!-- <div class="col-lg-4 col-md-6">
                        <div class="card">
                        <a href="#" onclick="loadPage('rpt_activity_by_user.htm', 'container');">
                            <div class="card-body">
                                <div class="d-flex flex-row">
                                    <div class="round round-lg align-self-center round-primary"><i class="mdi mdi-account-card-details"></i></div>
                                    <div class="m-l-10 align-self-center">
                                        
                                        <h5 class="text-muted m-b-0">User Reports</h5></div>
                                </div>
                            </div>
                            </a>
                        </div>
                    </div> -->
                    <!-- Column -->
                    <!-- Column -->
                    <!-- <div class="col-lg-3 col-md-6">
                        <div class="card">
                            <div class="card-body" href="#">
                                <div class="d-flex flex-row">
                                    <div class="round round-lg align-self-center round-danger"><i class="mdi mdi-cash-multiple"></i></div>
                                    <div class="m-l-10 align-self-center">
                                        
                                        <h5 class="text-muted m-b-0">Salary</h5></div>
                                </div>
                            </div>
                        </div>
                    </div> -->
                    <!-- Column -->
                </div>
                
                
                
             <div class="row">
                    <div class="col-12">
                       
                        	<div class="card card-outline-info">
                        	<div class="card-header">
                                <h4 class="m-b-0 text-white">Add New User</h4>
                            </div>
                        
                            <div class="card-body">
                                
                               
                                <form:form class="form-horizontal p-t-20" modelAttribute="user" method="post" action="saveUser">
                                    <div class="form-group row">
                                        <form:label path="username" for="exampleInputuname3" class="col-sm-3 control-label">Username<span class="text-danger">*</span></form:label>
                                        <div class="col-sm-9">
                                            <div class="controls">
                                           
                                            <form:input path="username" type="text"  name="text" class="form-control" value="${user.username}" data-validation-required-message="Item ID Cannot be empty"/> </div>
                                        	<form:errors path="username" cssClass="error"/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <form:label path="password" for="exampleInputEmail3" class="col-sm-3 control-label">Password<span class="text-danger">*</span></form:label>
                                        <div class="col-sm-9">
                                            <div class="controls">
                                            <form:input path="password" type="password" name="text" class="form-control" value="${user.password}" data-validation-required-message="Item Name Cannot be empty"/> </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <form:label path="role" for="web" class="col-sm-3 control-label">Role</form:label>
                                        <div class="col-sm-9">
                                            <form:select class="form-control custom-select" path="role">
                        						 <option >Loggin as</option>
                         						 <option>Admin</option>
                         						 <option>User</option>
                        					 </form:select>
                                        </div>
                                    </div>
                                    
                                    
                                    <div class="text-xs-right">
                                        <button type="submit" class="tst3 btn btn-success" >Submit</button>
                                        <button type="reset" class="btn btn-inverse">Cancel</button>
                                        
                                    </div>
                                </form:form>
                            </div>
                            </div>
                        
                    </div>
                </div>  
                    
        
             
      