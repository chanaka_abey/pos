<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


	<br>
	<div class="row" id="indexstat">
		<!-- Column -->
		<div class="col-lg-4 col-md-6">
			<div class="card">
				<a href="#" onclick="loadPage('rpt_charts_daily_sales.htm', 'container');">
					<div class="card-body">
						<div class="d-flex flex-row">
							<div class="round round-lg align-self-center round-info">
								<i class="ti-wallet"></i>
							</div>
							<div class="m-l-10 align-self-center">

								<h5 class="text-muted m-b-0">Daily Sales Chart</h5>
							</div>
						</div>
					</div>
				</a>
			</div>
		</div>
		<!-- Column -->
		<!-- Column -->
		<div class="col-lg-4 col-md-6">
			<div class="card">
			<a href="">
				<div class="card-body">
					<div class="d-flex flex-row">
						<div class="round round-lg align-self-center round-warning">
							<i class="mdi mdi-cellphone-link"></i>
						</div>
						<div class="m-l-10 align-self-center">

							<h5 class="text-muted m-b-0">Daily Profit Chart</h5>
						</div>
					</div>
				</div>
				</a>
			</div>
		</div>
		<!-- Column -->
		<!-- Column -->
		<div class="col-lg-4 col-md-6">
			<div class="card">
			<a href="rpt_charts_overall.htm">
				<div class="card-body">
					<div class="d-flex flex-row">
						<div class="round round-lg align-self-center round-primary">
							<i class="mdi mdi-cart-outline"></i>
						</div>
						<div class="m-l-10 align-self-center">

							<h5 class="text-muted m-b-0">Summary Chart</h5>
						</div>
					</div>
				</div>
				</a>
			</div>
		</div>
		<!-- Column -->
		
	</div>


	<div class="row" id="indexstat">
		<!-- Column -->
		<div class="col-lg-4 col-md-6">
			<div class="card">
				<a href="">
					<div class="card-body">
						<div class="d-flex flex-row">
							<div class="round round-lg align-self-center round-info">
								<i class="ti-wallet"></i>
							</div>
							<div class="m-l-10 align-self-center">

								<h5 class="text-muted m-b-0">Below Reorder level Report</h5>
							</div>
						</div>
					</div>
				</a>
			</div>
		</div>
		<!-- Column -->
		<!-- Column -->
		<div class="col-lg-4 col-md-6">
			<div class="card">
			<a href="">
				<div class="card-body">
					<div class="d-flex flex-row">
						<div class="round round-lg align-self-center round-warning">
							<i class="mdi mdi-cellphone-link"></i>
						</div>
						<div class="m-l-10 align-self-center">

							<h5 class="text-muted m-b-0">Item wise Report</h5>
						</div>
					</div>
				</div>
				</a>
			</div>
		</div>
		<!-- Column -->
		<!-- Column -->
		<div class="col-lg-4 col-md-6">
			<div class="card">
			<a href="">
				<div class="card-body">
					<div class="d-flex flex-row">
						<div class="round round-lg align-self-center round-primary">
							<i class="mdi mdi-cart-outline"></i>
						</div>
						<div class="m-l-10 align-self-center">

							<h5 class="text-muted m-b-0">Stock Updates Report</h5>
						</div>
					</div>
				</div>
				</a>
			</div>
		</div>
		<!-- Column -->
		
	</div>



