<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<script>

var id = document.getElementById("id44").value;
if(id == 0 || id=="" || id==null){
	document.getElementById("supplier_hdid").innerText = "Create Supplier Item";
	document.getElementById("supplierstockheaderid").innerText = "Create Supplier Item";
	
}else{
	document.getElementById("supplierstockheaderid").innerText = "Edit Supplier Item";
	document.getElementById("supplier_hdid").innerText = "Edit Supplier Item";
}
</script>

<script>
	$('#mdate').bootstrapMaterialDatePicker({
		weekStart : 0,
		time : false
	});
</script>

<script>
	jQuery(document).ready(function() {
		// Switchery

		// For select 2
		$(".itemselect3").select2();
		$('.selectpicker').selectpicker();
		//Bootstrap-TouchSpin

		// For multiselect

		$(".ajax").select2({
			ajax : {
				url : "https://api.github.com/search/repositories",
				dataType : 'json',
				delay : 250,
				data : function(params) {
					return {
						q : params.term, // search term
						page : params.page
					};
				},
				processResults : function(data, params) {
					// parse the results into the format expected by Select2
					// since we are using custom formatting functions we do not need to
					// alter the remote JSON data, except to indicate that infinite
					// scrolling can be used
					params.page = params.page || 1;
					return {
						results : data.items,
						pagination : {
							more : (params.page * 30) < data.total_count
						}
					};
				},
				cache : true
			},
			escapeMarkup : function(markup) {
				return markup;
			}, // let our custom formatter work
			minimumInputLength : 1,
			templateResult : formatRepo, // omitted for brevity, see the source of this page
			templateSelection : formatRepoSelection
		// omitted for brevity, see the source of this page
		});
	});
</script>

<script>
	function CreateItem() {
		var suplliername= document.getElementById("suplliernameid").value;
		var itemname = document.getElementById("itemnameid").value;
		var date = document.getElementById("mdate").value;
		var quantity = document.getElementById("quantityid").value;
		var itemid = document.getElementById("itemid").value;
		var category = document.getElementById("categoryid").value;
		var position = document.getElementById("positionid").value;
		var unitprice = document.getElementById("unitpriceid").value;
		var salesprice = document.getElementById("salespriceid").value;
		var reorder = document.getElementById("reorderlevelid").value;

		//alert(suplliername + " / " + itemid +" / " + itemname +" / " + category +" / " +position +" / " +  unitprice +" / " + salesprice +" / " + date +" / " + quantity +" / " + reorder);

		  $.ajax({
			method : "POST",
			url : 'saveSupplierItem',
			data : {
				itemid : itemid,
				itemname : itemname,
				category : category,
				position : position,
				unitprice : unitprice,
				salesprice : salesprice,
				quantity : quantity,
				reorder : reorder
			},
			dataType : "json",
			cache : false,
			success : function() {

			},
			error : function() {

			}
		});  

	}
</script>

<script>
function stockCreate(){
	var suplliername= document.getElementById("suplliernameid").value;
	var itemname = document.getElementById("itemnameid").value;
	var quantity = document.getElementById("quantityid").value;
	var date = document.getElementById("mdate").value;
	

	
	 $.ajax({
		method : "POST",
		url : 'createsupplieritemstock',
		data : {
			suplliername : suplliername,
			itemname : itemname,
			quantity : quantity,
			date : date
		},
		dataType : "json",
		cache : false,
		success : function() {

			//alert("success");
		},
		error : function() {

		}
	}); 
	 
	
	
}
</script>



<!-- <script>
var checksec = document.getElementById("itemidid").value;
if(checksec =="" || checksec == null){
	document.getElementById("itemnameid").disabled = false;
}else{
	document.getElementById("itemnameid").disabled = true;
}
</script> -->



<br>

<nav class="breadcrumb" style="background-color:#c0d8f1;">
                                    <a class="breadcrumb-item" href="index.htm">Home</a>
                                    <a class="breadcrumb-item" href="#" onclick="loadPage('load_supplier_stocks.htm', 'container');">Suppliers</a>
                                    <span class="breadcrumb-item active" id="supplier_hdid">Add New Stock</span>
      </nav>
  <br>
 

<div class="row" id="itemstat">
	
		<!-- Column -->
		<div class="col-lg-3 col-md-6">
			<div class="card">
			<a href="#" onclick="loadPage('add_new_supplier_stock.htm', 'container');">
				<div class="card-body">
					<div class="d-flex flex-row">
						<div class="round round-lg align-self-center round-primary">
							<i class="mdi mdi-store"></i>
						</div>
						<div class="m-l-10 align-self-center">

							<h5 class="text-muted m-b-0">Add new Stock</h5>
						</div>
					</div>
				</div>
				</a>
			</div>
		</div>
		<!-- Column -->
		<!-- Column -->
		<div class="col-lg-3 col-md-6">
			<div class="card">
				<a href="#" onclick="loadPage('add_new_supplier.htm', 'container');">
					<div class="card-body">
						<div class="d-flex flex-row">
							<div class="round round-lg align-self-center round-info">
								<i class="mdi mdi-truck"></i>
							</div>
							<div class="m-l-10 align-self-center">

								<h5 class="text-muted m-b-0">Add New Supplier</h5>
							</div>
						</div>
					</div>
				</a>
			</div>
		</div>
		<!-- Column -->
		<!-- Column -->
		<div class="col-lg-3 col-md-6">
			<div class="card">
			<a href="#" onclick="loadPage('suppliers.htm', 'container');">
				<div class="card-body">
					<div class="d-flex flex-row">
						<div class="round round-lg align-self-center round-danger">
							<i class="mdi mdi-truck-delivery"></i>
						</div>
						<div class="m-l-10 align-self-center">

							<h5 class="text-muted m-b-0">Supplier List</h5>
						</div>
					</div>
				</div>
				</a>
			</div>
		</div>
		<!-- Column -->
		<!-- Column -->
		<div class="col-lg-3 col-md-6">
			<div class="card">
			<a href="#"  onclick="loadPage('supplier_reports.htm', 'container');">
				<div class="card-body">
					<div class="d-flex flex-row">
						<div class="round round-lg align-self-center round-warning">
							<i class="mdi mdi-chart-areaspline"></i>
						</div>
						<div class="m-l-10 align-self-center">

							<h5 class="text-muted m-b-0">Create Supplier Item</h5>
						</div>
					</div>
				</div>
				</a>
			</div>
		</div>
		<!-- Column -->
		
		
	</div>



<div class="row">
	<div class="col-12">

		<div class="card card-outline-info">
			<div class="card-header">
				<h4 class="m-b-0 text-white" id="supplierstockheaderid">Create Supplier Item</h4>
			</div>

			<div class="card-body">


				<form class="form-horizontal p-t-20" novalidate action="saveSupplierItem">
					<div class="form-group row">
						<label for="exampleInputuname3" class="col-sm-3 control-label">Supplier
							Name<span class="text-danger">*</span>
						</label>
						<div class="col-sm-7">
							<div class="controls">
								<input id="id44" value="${myModel.idss}" style="display: none;" />
								<div class="controls">
									<select class="itemselect3" style="width: 100%"
										id="suplliernameid" name="itemname" required class="form-control">
										<option>${myModel.sup_name}</option>
										<option value="">Select Supplier</option>

										<c:forEach items="${myModel.supplierslist}" var="suppliers"
											varStatus="status">

											<option value="${suppliers.name}">${suppliers.name}</option>

										</c:forEach>

									</select>
								</div>


								<errors cssClass="error" />
							</div>
						</div>
					</div>


					<div class="form-group row">
						<label for="exampleInputEmail3" class="col-sm-3 control-label">Item
							ID<span class="text-danger">*</span>
						</label>
						<div class="col-sm-7">
							<div class="controls">
								<input type="text" name="text" id="itemid"
									class="form-control" data-validation-required-message="Item ID Cannot be empty" />
							</div>

						</div>
					</div>
					
					
					<div class="form-group row">
						<label for="exampleInputEmail3" class="col-sm-3 control-label">Item
							Name<span class="text-danger">*</span>
						</label>
						<div class="col-sm-7">
							<div class="controls">
								<input type="text" name="text" id="itemnameid"
									class="form-control" data-validation-required-message="Item Name Cannot be empty" />
							</div>

						</div>
					</div>
					
					
					<div class="form-group row">
						<label for="exampleInputEmail3" class="col-sm-3 control-label">
						Category
						</label>
						<div class="col-sm-7">
							<div class="controls">
								<input type="text" name="text" id="categoryid"
									class="form-control" />
							</div>

						</div>
					</div>
					
					
					<div class="form-group row">
						<label for="exampleInputEmail3" class="col-sm-3 control-label">
						Position
						</label>
						<div class="col-sm-7">
							<div class="controls">
								<input type="text" name="text" id="positionid"
									class="form-control" />
							</div>

						</div>
					</div>
					
					
					
					<div class="form-group row">
                                        <label  for="unitprice" class="col-sm-3 control-label">Unit Price<span class="text-danger">*</span></label>
                                        <div class="col-sm-7">
                                        	<div class="controls">
                                            <div class="input-group"> <span class="input-group-addon">Rs</span>
                                            <input  id="unitpriceid" type="number" name="onlyNum" class="form-control"  data-validation-required-message="Unit price Cannot be empty" min="1"/> <span class="input-group-addon">.00</span> </div>
                                       		</div>
                                        </div>
                                    </div>
                                    
                                    
                      <div class="form-group row">
                                        <label  for="unitprice" class="col-sm-3 control-label">Sales Price<span class="text-danger">*</span></label>
                                        <div class="col-sm-7">
                                        	<div class="controls">
                                            <div class="input-group"> <span class="input-group-addon">Rs</span>
                                            <input  id="salespriceid" type="number" name="onlyNum" class="form-control"  data-validation-required-message="Sales price Cannot be empty" min="1"/> <span class="input-group-addon">.00</span> </div>
                                       		</div>
                                        </div>
                                    </div>              
					
					
					
					<%-- <input style="display: none;" type="text" id="itemidid" value="${myModel.item_id}" class="form-control">
					<input style="display: none;" type="number" id="updequantityid" value="${myModel.quantity}" class="form-control" >
					<input style="display: none;" type="number" id="updequantityid2" value="0" class="form-control" > --%>
					

					<div class="form-group row">
						<label for="exampleInputEmail3" class="col-sm-3 control-label">Supply
							Date<span class="text-danger">*</span>
						</label>
						<div class="col-sm-7">
							<div class="controls">
								<input type="text" placeholder="select date" id="mdate"
									class="form-control" value="${myModel.date}"
									data-validation-required-message="Please Select Date" />

							</div>
						</div>
					</div>

					<div class="form-group row">
						<label for="exampleInputEmail3" class="col-sm-3 control-label">Quantity<span
							class="text-danger">*</span></label>
						<div class="col-sm-7">
							<div class="controls">
								<input type="number" placeholder="Enter Quantity" name="text"
									class="form-control" min="0" id="quantityid"
									value="${myModel.quantity}"
									data-validation-required-message="Quantity Cannot be empty" />
							</div>
						</div>
					</div>
					
					
					<div class="form-group row">
						<label for="exampleInputEmail3" class="col-sm-3 control-label">Reorder Level<span
							class="text-danger">*</span></label>
						<div class="col-sm-7">
							<div class="controls">
								<input type="number" placeholder="Enter Reorder Level" name="text"
									class="form-control" min="0" id="reorderlevelid"
									data-validation-required-message="Reorder Level Cannot be empty" />
							</div>
						</div>
					</div>




					<div class="text-xs-right">
						<button type="submit" onclick="CreateItem();stockCreate();"
							class="btn btn-info" id="saveSupplier" >Submit</button>
						<button type="reset" class="btn btn-inverse">Cancel</button>
					</div>

				</form>
			</div>
		</div>

	</div>
</div>


<script>
	!function(window, document, $) {
		"use strict";
				$("input,select,textarea").not("[type=submit]")
						.jqBootstrapValidation(), $(".skin-square input")
						.iCheck({
							checkboxClass : "icheckbox_square-green",
							radioClass : "iradio_square-green"
						}), $(".touchspin").TouchSpin(), $(".switchBootstrap")
						.bootstrapSwitch();
	}(window, document, jQuery);
</script>


