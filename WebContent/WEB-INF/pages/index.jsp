<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">


<!-- Mirrored from wrappixel.com/demos/admin-templates/material-pro/material/index2.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 16 Dec 2017 08:20:02 GMT -->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- Tell the browser to be responsive to screen width -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<!-- Favicon icon -->
<link rel="icon" type="image/png" sizes="16x16"
	href="resources/images/favicon.png">
<title>POS System</title>
<!-- Bootstrap Core CSS -->
<link href="resources/css/bootstrap.min.css" rel="stylesheet">


<link href="resources/css/bootstrap-material-datetimepicker.css"
	rel="stylesheet">
<link href="resources/css/bootstrap-datepicker.min.css" rel="stylesheet">

<link href="resources/css/select2.min.css" rel="stylesheet">
<link href="resources/css/bootstrap-select.min.css" rel="stylesheet">

<!-- chartist CSS -->
<link href="resources/css/chartist.min.css" rel="stylesheet">
<link href="resources/css/chartist-init.css" rel="stylesheet">
<link href="resources/css/chartist-plugin-tooltip.css" rel="stylesheet">
<link href="resources/css/css-chart.css" rel="stylesheet">

<link href="resources/css/sweetalert.css" rel="stylesheet" />
<!--This page css - Morris CSS -->
<link href="resources/css/c3.min.css" rel="stylesheet">
<!-- Vector CSS -->
<link href="resources/css/jquery-jvectormap-2.0.2.css" rel="stylesheet" />
<!-- Custom CSS -->
<link href="resources/css/jquery.toast.css" rel="stylesheet" />

<link href="resources/css/style.css" rel="stylesheet">
<!-- You can change the theme colors from here -->
<link href="resources/css/blue.css" id="theme" rel="stylesheet">
 <style>
    #tbl_index_filter{
   
    float:left;
    }
    .dataTables_filter input{
     width:500px;
    }
    </style>






<script>
function calcTotal(rowIndex) {
	
   
	var Row = document.getElementById(rowIndex);
//	alert("Row Number : " + rowIndex);

	
	var salesprice = document.getElementById('salesprice'+rowIndex).value;
	var unitprice = document.getElementById('unitprice'+rowIndex).value;
//	alert("unitprice : " + unitprice);
	var quantity = document.getElementById('qty'+rowIndex).value;
//	alert("quantity : " + quantity);
	var discount = document.getElementById('disc'+rowIndex).value;
//	 alert("discount : " + discount);
	 
	 
	 var discountprice = ((salesprice * quantity)/100)*discount;
	 var total = (salesprice * quantity) - discountprice;
	 var unittotal = (unitprice * quantity);
	 document.getElementById('total'+rowIndex).value = total; 
	 document.getElementById('unittotal'+rowIndex).value = unittotal;
	 

}




function addCart(rowIndex){
	var Rowvalue = document.getElementById(rowIndex);
	//alert("Row Number : " + rowIndex);
	var id = document.getElementById('id'+rowIndex).value;
	var itemid = document.getElementById('itemid'+rowIndex).value;
	var discount = document.getElementById('disc'+rowIndex).value;
	
	var salesprice = document.getElementById('salesprice'+rowIndex).value;
	//alert(" id " + id);
	
	var itemname = document.getElementById('itemname'+rowIndex).value;
	//alert("itemname : " + itemname);
	//var itemname = document.getElementById('itemname'+rowIndex).value;
	//alert("itemname : " + itemname);
	var quantity = document.getElementById('qty'+rowIndex).value;
	//alert("quantity : " + quantity);
	var subtotal = document.getElementById('total'+rowIndex).value;
	var unittotal = document.getElementById('unittotal'+rowIndex).value;
	//alert("unittotal : " + unittotal);
	
	var unitprice = document.getElementById('unitprice'+rowIndex).value;
	var category = document.getElementById('category'+rowIndex).value;
	var reorderlevel = document.getElementById('reorderlevel'+rowIndex).value;
	
	 var table = document.getElementById("abctable");
	// var xx =  document.getElementById("abctable").rows.length
	// var cls = document.getElementById("abctable").getElementsByTagName("td");
	
	
	
	var btn = document.createElement("BUTTON");
    var t = document.createTextNode("X");
    btn.style.backgroundColor = "#f95e4a";
    btn.appendChild(t);
    // var removebtn = document.body.appendChild(btn);
	
	// var property=document.getElementById(btn);
	 
	
	    var row = table.insertRow(-1);
	    var cell1 = row.insertCell(0);
	    var cell2 = row.insertCell(1);
	   var cell3 = row.insertCell(2);
	   var cell4 = row.insertCell(3);
	   var cell5 = row.insertCell(4);
	   var cell6 = row.insertCell(5);
	   var cell7 = row.insertCell(6);
	   var cell8 = row.insertCell(7);
	   var cell9 = row.insertCell(8);
	   var cell10 = row.insertCell(9);
	   var cell11 = row.insertCell(10);
	   var cell12 = row.insertCell(11);
	   cell1.className = "itemnames";
	   cell2.className = "quntities";
	   cell3.className = "countable";
	   cell4.className = "countable2";
	   cell5.className = "countable3";
	   cell6.className = "countable4";
	   cell7.className = "countable5";
	   cell8.className = "itmids";
	   cell9.className = "unipric";
	   cell10.className = "catey";
	   cell11.className = "reoder";
	    cell1.innerHTML = itemname;
	    cell2.innerHTML = quantity;
	    cell3.innerHTML = subtotal;
	    cell4.innerHTML = unittotal;
	    cell5.innerHTML = itemid;
	    cell6.innerHTML = discount;
	    cell7.innerHTML = salesprice;
	    cell8.innerHTML = id;
	    cell9.innerHTML = unitprice;
	    cell10.innerHTML = category;
	    cell11.innerHTML = reorderlevel;
	    cell12.appendChild(btn);
	    
	    
	    var cls = document.getElementById("abctable").getElementsByTagName("td");
        console.log("cls[i].length " +cls.length);
	    var sum = 0;
	    var sum2 = 0;
	    
	    btn.onclick = function deleteRow(r) {
			
		    var i = this.parentNode.parentNode.rowIndex;
		    //alert("kkkkkkkk" + i);
		    document.getElementById("abctable").deleteRow(i);
		   
		    
		    var cls = document.getElementById("abctable").getElementsByTagName("td");
	      //  alert("cls[i].length " +cls.length);
		    var sum = 0;
		    var sum2 = 0;
		    
		    for (var i = 0; i < cls.length; i++){
		    	
		        if(cls[i].className == "countable"){
		        	//alert("countable " + cls[i].innerHTML);
		            sum += isNaN(cls[i].innerHTML) ? 0 : parseInt(cls[i].innerHTML);
		            //alert("SUM " + sum);
		        }

		    }
		    document.getElementById('sub_total').innerText = sum; 
		    var totaldiscount = document.getElementById('total_discount').value;
		    var discountprice = (sum/100)*totaldiscount;
			var totalamount = sum - discountprice;
		    document.getElementById('total_amount').innerText = totalamount; 
		}
	    
	    
	   // document.getElementByClass('countable2').style.display='none';
	    
	    
	  //  alert("cls[i].length " +cls.length);
	  //  alert("cls[i].cl " +cls[2].innerHTML);
	    for (var i = 0; i < cls.length; i++){
	    	
	        if(cls[i].className == "countable"){
	        	//alert("countable " + cls[i].innerHTML);
	            sum += isNaN(cls[i].innerHTML) ? 0 : parseInt(cls[i].innerHTML);
	            
	        }

	    }
	    
	     for (var i = 0; i < cls.length; i++){
	        if(cls[i].className == "countable2"){
	        	//alert("countable " + cls[i].innerHTML);
	            sum2 += isNaN(cls[i].innerHTML) ? 0 : parseInt(cls[i].innerHTML);
	            cls[i].style.display='none';
	        }
	        if(cls[i].className == "countable3"){
	        	cls[i].style.display='none';
	        }
	        if(cls[i].className == "countable4"){
	        	cls[i].style.display='none';
	        }
	         if(cls[i].className == "countable5"){
	        	cls[i].style.display='none';
	        } 
	         
	         if(cls[i].className == "itmids"){
		        	cls[i].style.display='none';
		        } 
	         if(cls[i].className == "unipric"){
		        	cls[i].style.display='none';
		        }
	         if(cls[i].className == "catey"){
		        	cls[i].style.display='none';
		        }
	         if(cls[i].className == "reoder"){
		        	cls[i].style.display='none';
		        } 
	    } 
	    
	  //  alert('sum2 is ' + sum2);
	    document.getElementById('sub_total').innerText = sum; 
	    document.getElementById('unit_total').innerText = sum2; 
	    var totaldiscount = document.getElementById('total_discount').value;
	//    alert("total_discount " + totaldiscount);
	   var discountprice = (sum/100)*totaldiscount;
		 var totalamount = sum - discountprice;
		 document.getElementById('total_amount').innerText = totalamount; 
		 var profitcal = totalamount - sum2;
		 document.getElementById('profit').innerText = profitcal;
	   
}

function addTotalDiscount(){
	var totaldiscount = document.getElementById('total_discount').value;
	var unit_total = document.getElementById('unit_total').innerText;
	
  //  alert("unit_total " + unit_total);
    var subtotal = document.getElementById('sub_total').innerText;
  //  alert("sub_total " + subtotal);
    var discountprice = (subtotal/100)*totaldiscount;
	 var totalamount = subtotal - discountprice;
	 document.getElementById('total_amount').innerText = totalamount;
	 
	 var profitcal = totalamount - unit_total;
	 document.getElementById('profit').innerText = profitcal;
}

function getinvoice(){
	//alert("invoiceid");
	//var total_amount = document.getElementById('total_amount').value();
	var total_amount = document.getElementById('total_amount').innerText;
	var unit_total = document.getElementById('unit_total').innerText;
	var profit = document.getElementById('profit').innerText;
	var total_discount = document.getElementById('total_discount').value;
	var invoiceid = document.getElementById('invoiceid').innerText;
	//alert("total_discount" + total_discount);
	
	$.ajax({
		method:"POST",
		url: 'invoicepage.htm?total_amount='+total_amount+'&unit_total='+unit_total+'&profit='+profit+'&total_discount='+total_discount,
		data: {
			total_amount : total_amount,
			unit_total : unit_total,
			profit : profit,
			total_discount : total_discount,
			invoiceid : invoiceid
		},
		dataType: "json",
		cache: false,
		success: function(){
			
			alert("success" + total_amount);
		},
		error: function(){
			alert("Successfully proceed..! " );
			//location.reload();
		}
	});
}

function addToSales(){
	
	 
	 var myTable = document.getElementById("abctable");
	
	// alert("bbbb " + myTable); 
	 for (var r=1, n = myTable.rows.length; r < n; r++) {
         // this loop is getting each colomn/cells
        // alert("n n  " + n);
for (var c = 0, m = myTable.rows[r].cells.length; c < m; c++) {
//alert("myTable.rows[r].cells[1].innerHTML "+ myTable.rows[r].cells[c].innerHTML);
//if(c == 0){
	var itemname = myTable.rows[r].cells[0].innerHTML;
//}
//if(c == 1){
	var quntity = myTable.rows[r].cells[1].innerHTML;
//}
//if(c == 2){
	var salesprice = myTable.rows[r].cells[2].innerHTML;
//}
//if(c == 3){
	var unitprice = myTable.rows[r].cells[3].innerHTML;
//}
	var itemid = myTable.rows[r].cells[4].innerHTML;
	
	var discount = myTable.rows[r].cells[5].innerHTML;
	
	var salesprice2 = myTable.rows[r].cells[6].innerHTML;
	
	var id  = myTable.rows[r].cells[7].innerHTML;
	
	var unitprice33 = myTable.rows[r].cells[8].innerHTML;
	
	var category = myTable.rows[r].cells[9].innerHTML;
	
	var reorder = myTable.rows[r].cells[10].innerHTML;
	

}
var invoiceid = document.getElementById('invoiceid').innerText;
//alert("ID : "+id+"itemname " + itemname + " unitprice "+ unitprice33 );
//getsalespage.htm
//alert("Item id ---> " + id);
//stock
$.ajax({
	method:"POST",
	url: 'getsalespage.htm?itemname='+itemname+'&quntity='+quntity+'&salesprice='+salesprice+'&unitprice='+unitprice,
	data: {
		id : id,
		itemname : itemname,
		quntity : quntity,
		salesprice : salesprice,
		unitprice : unitprice,
		itemid : itemid,
		discount : discount,
		salesprice2 : salesprice2,
		unitprice33 : unitprice33,
		category : category,
		reorder : reorder,
		invoiceid : invoiceid
	},
	dataType: "json",
	cache: false,
	success: function(){
		
		alert("success" + total_amount);
	}
});



}
	 
}


</script>



<script>

function loadPrintInvoice(){
	
	//clearTable();
	
	var subtotal = document.getElementById('sub_total').innerText;
	document.getElementById('prnt_sub_total').innerText = subtotal;
	
	var totaldiscount22 = document.getElementById('total_discount').value;
	document.getElementById('prnt_total_discount').innerText = totaldiscount22;
	
	var totalprice = document.getElementById('total_amount').innerText;
	document.getElementById('prnt_total_price').innerText = totalprice;
	
	
	n =  new Date();
	y = n.getFullYear();
	m = n.getMonth() + 1;
	d = n.getDate();
	document.getElementById("prnt_date").innerHTML = y + "/" + m + "/" + d;
	
	
	var date=new Date();
	var time=date.toLocaleTimeString();
	document.getElementById("prnt_time").innerHTML=time;
	
	
	
	 var myTable = document.getElementById("abctable");
		
		// alert("bbbb " + myTable); 
		 for (var r=1, n = myTable.rows.length; r < n; r++) {
	         // this loop is getting each colomn/cells
	        // alert("n n  " + n);
	for (var c = 0, m = myTable.rows[r].cells.length; c < m; c++) {
	//alert("myTable.rows[r].cells[1].innerHTML "+ myTable.rows[r].cells[c].innerHTML);
	//if(c == 0){
		var itemname = myTable.rows[r].cells[0].innerHTML;
	//}
	//if(c == 1){
		var quntity = myTable.rows[r].cells[1].innerHTML;
	//}
	//if(c == 2){
		var salesprice = myTable.rows[r].cells[2].innerHTML;
	//}
	//if(c == 3){
		var unitprice = myTable.rows[r].cells[3].innerHTML;
	//}
		var itemid = myTable.rows[r].cells[4].innerHTML;
		
		var discount = myTable.rows[r].cells[5].innerHTML;
		
		var salesprice2 = myTable.rows[r].cells[6].innerHTML;
		
		var id  = myTable.rows[r].cells[7].innerHTML;
		
		var unitprice33 = myTable.rows[r].cells[8].innerHTML;
		
		var category = myTable.rows[r].cells[9].innerHTML;
		
		var reorder = myTable.rows[r].cells[10].innerHTML;
		

	}
	//alert("itemname " + itemname +" quntity " +quntity + " salesprice" + salesprice);
	
	
	var table = document.getElementById("prnt_table");
	
	 var row = table.insertRow(-1);
	 var cell1 = row.insertCell(0);
	 var cell2 = row.insertCell(1);
	 var cell3 = row.insertCell(2);
	 var cell4 = row.insertCell(3);
	 
	// alert("cell1 " + cell1.innerHTML + "  cell2 " + cell2.innerHTML + "  cell3 " + cell3.innerHTML );
	
	  cell1.innerHTML = itemname;
	  cell2.innerHTML = quntity;
	  cell3.innerHTML = discount + " %";
	  cell4.innerHTML = salesprice;
	  
	
		 }
	
	
	
	
}

</script>

<script>

function clearTable(){
	var Table = document.getElementById("prnt_table");
	//Table.innerHTML = "";
	 //alert(" Table.rows.length " +  Table.rows.length);
	 for (var r=4; r <= Table.rows.length + 10; r++) {
	//	 alert("Delete Row : " + r);
	document.getElementById("prnt_table").deleteRow(3);
	 }
	
}

</script>


<%-- <% 
String roless = (String) session.getAttribute("role"); 
	session.setAttribute("role", roless);
%> --%>

<script>
	function loadnotificaton(){
		
		if(${fn:length(myModel.itemlist2)} >= 1){
			document.getElementById("notifyid").style.display = "block";
			document.getElementById("notifynewid").style.display = "block";
			
		}
		else{
			document.getElementById("notifyid").style.display = "none";
			document.getElementById("notifynewid").style.display = "none";
		}
	}
	</script>

<%
	String username = (String) session.getAttribute("username");
	session.setAttribute("username", username);

	String role = (String) session.getAttribute("role");
	session.setAttribute("role", role);
%>


<script>
	function checkRole(){
	//alert("checkRole");
	var rolename = document.getElementById('rolename').innerText;
	//alert("rolename " + rolename);
	
	if(rolename == "Admin"){
		
		document.getElementById("indexstat").style.display = "flex";
		document.getElementById("adminpanelid").style.display = "flex";
		
		
		//document.getElementById("adminedititem1").style.display = "none";	
	}
	else{
		document.getElementById("indexstat").style.display = "none";
		document.getElementById("adminpanelid").style.display = "none";
		//document.getElementById("adminedititem1").style.display = "none";
	}
	
}
	

	

	function checkRole2(){
		//alert("rolename2 start");
		var rolename2 = document.getElementById('rolename2').innerText;
		//alert("rolename2 " + rolename2);
		
		
		if(rolename2 == "User"){
		document.getElementById("itemeditth").style.display = "none";
		document.getElementById("itemedeleteth").style.display = "none";
		document.getElementById("itemeditth2").style.display = "none";
		document.getElementById("itemdeleteth2").style.display = "none";
		}
	}

</script>





</head>

<body class="fix-header fix-sidebar card-no-border"
	onload="loadnotificaton();checkRole();">
	<!-- ============================================================== -->
	<!-- Preloader - style you can find in spinners.css -->
	<!-- ============================================================== -->
	<%-- <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
    </div> --%>
	<!-- ============================================================== -->
	<!-- Main wrapper - style you can find in pages.scss -->
	<!-- ============================================================== -->
	<div id="main-wrapper">
		<!-- ============================================================== -->
		<!-- Topbar header - style you can find in pages.scss -->
		<!-- ============================================================== -->
		<header class="topbar"> <nav
			class="navbar top-navbar navbar-expand-md navbar-light"> <!-- ============================================================== -->
		<!-- Logo --> <!-- ============================================================== -->
		<div class="navbar-header">
			<a class="navbar-brand" href="index.htm"> <!-- Logo icon --> <b>
					<!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
					<!-- Dark Logo icon --> <img src="resources/images/logo.png"
					alt="homepage" class="dark-logo" /> <!-- Light Logo icon --> <img
					src="resources/images/logo.png" alt="homepage" class="light-logo" />
			</b> <!--End Logo icon --> <!-- Logo text --> <span> <!-- dark Logo text -->
					<img src="resources/images/logo-text.png" alt="homepage"
					class="dark-logo" /> <!-- Light Logo text --> <img
					src="resources/images/pos-dark-logo.png" class="light-logo"
					alt="homepage"  style="height: 32px;width: 160px;"/></span>
			</a>
		</div>
		<!-- ============================================================== -->
		<!-- End Logo --> <!-- ============================================================== -->
		<div class="navbar-collapse">
			<!-- ============================================================== -->
			<!-- toggle and nav items -->
			<!-- ============================================================== -->
			<ul class="navbar-nav mr-auto mt-md-0">
				<!-- This is  -->
				<li class="nav-item"><a
					class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark"
					href="javascript:void(0)"><i class="mdi mdi-menu"></i></a></li>
				<li class="nav-item"><a
					class="nav-link sidebartoggler hidden-sm-down text-muted waves-effect waves-dark"
					href="javascript:void(0)"><i class="ti-menu"></i></a></li>
				<!-- ============================================================== -->
				<!-- Search -->
				<!-- ============================================================== -->
				<!-- <li class="nav-item hidden-sm-down search-box"><a
					class="nav-link hidden-sm-down text-muted waves-effect waves-dark"
					href="javascript:void(0)"><i class="ti-search"></i></a>
					<form class="app-search">
						<input type="text" class="form-control"
							placeholder="Search & enter"> <a class="srh-btn"><i
							class="ti-close"></i></a>
					</form></li> -->
				<!-- ============================================================== -->
				<!-- Messages -->
				<!-- ============================================================== -->
				<li class="nav-item dropdown mega-dropdown" id="adminpanelid" style="display: none;"><a
					class="nav-link dropdown-toggle text-muted waves-effect waves-dark"
					href="rpt_sales_today.htm" data-toggle="dropdown" aria-haspopup="true"
					aria-expanded="false"><i class="mdi mdi-view-grid"></i></a>
					<div class="dropdown-menu scale-up-left">
						<ul class="mega-dropdown-menu row">
							<li class="col-lg-2 col-xlg-2 m-b-30">
								<h4 class="m-b-20">ADMIN</h4> <!-- CAROUSEL -->
								<ul class="list-style-none">
									<li><a href="#" onclick="loadPage('items.htm', 'container');" data-toggle="dropdown" ><i
											class="mdi mdi-server"></i>&nbsp; Change Stock Items</a></li>
									<li><a href="#" onclick="loadPage('items.htm', 'container');" data-toggle="dropdown" ><i
											class="mdi mdi-tag-multiple mdi-18px"></i>&nbsp; Items</a></li>		
									<li><a href="#" onclick="loadPage('sales.htm', 'container');" data-toggle="dropdown" ><i
											class="fa fa-money"></i>&nbsp; Sales</a></li>
									<li><a href="#" onclick="loadPage('invoice.htm', 'container');" data-toggle="dropdown" ><i
											class="mdi mdi-newspaper"></i>&nbsp; Invoice</a></li>
					
									<li><a href="#" onclick="loadPage('reports.htm', 'container');" data-toggle="dropdown" ><i
											class="mdi mdi-chart-bar"></i>&nbsp; Analyse</a></li> 
									<li><a href="#" onclick="loadPage('load_supplier_stocks.htm', 'container');" data-toggle="dropdown" ><i
											class="mdi mdi-truck-delivery"></i>&nbsp; Suppliers</a></li>			
									<li><a href="#" onclick="loadPage('user.htm', 'container');" data-toggle="dropdown" ><i
											class="mdi mdi-account mdi-18px"></i>&nbsp; Users</a></li>
									<li><a href="#" onclick="loadPage('backup.htm', 'container');" data-toggle="dropdown" ><i
											class="mdi mdi-database"></i>&nbsp; Backup</a></li>	 
									
								</ul> <!-- End CAROUSEL -->
							</li>
		 					<li class="col-lg-2 m-b-30">
						 		<h4 class="m-b-20">REPORTS</h4> Accordian
								<ul class="list-style-none">
									<li><a href="#" onclick="loadPage('stock_reports.htm', 'container');" data-toggle="dropdown" ><i
											class="mdi mdi-file-document"></i>&nbsp;Stock Reports</a></li>
									<li><a href="#" onclick="loadPage('sales_reports.htm', 'container');" data-toggle="dropdown" ><i
											class="mdi mdi-file-chart"></i>&nbsp;Sales Reports</a></li>
									<li><a href="#" onclick="loadPage('invoice_reports.htm', 'container');" data-toggle="dropdown"><i
											class="mdi mdi-newspaper"></i>&nbsp;Invoice Reports</a></li>
									<li><a href="#" onclick="loadPage('activity_reports.htm', 'container');" data-toggle="dropdown"><i
											class="mdi mdi-account-card-details"></i>&nbsp; Activity Reports</a></li>
									<li><a href="#" onclick="loadPage('supplier_reports.htm', 'container');" data-toggle="dropdown"><i
											class="mdi mdi-file-image"></i>&nbsp; Suppliers Reports</a></li>							
								</ul>  
								<br>
							<h4 class="m-b-20">CHARTS</h4>
								<ul class="list-style-none">
								<li><a href="#" onclick="loadPage('rpt_charts_daily_sales.htm', 'container');" data-toggle="dropdown"><i
											class="mdi mdi-chart-line"></i>&nbsp; Daily Sales Chart</a></li>
									<li><a href="#" onclick="loadPage('rpt_charts_daily_profit.htm', 'container');" data-toggle="dropdown"><i
											class="mdi mdi-chart-areaspline"></i>&nbsp; Daily Profit Chart</a></li>
									<li><a href="#" onclick="loadPage('rpt_charts_date_range_sales.htm', 'container');" data-toggle="dropdown"><i
											class="mdi mdi-chart-bar"></i>&nbsp;Sales Range Chart</a></li>	
									<li><a href="#" onclick="loadPage('rpt_charts_date_range_profit.htm', 'container');" data-toggle="dropdown"><i
											class="ti-bar-chart"></i>&nbsp;Profit Range Chart</a></li> 
								
								</ul> 
								
							</li> 
			 			<li class="col-lg-8  m-b-30">
								<h4 class="m-b-20">TODAY SALES</h4> <!-- Contact -->
								<div class="table-responsive">
								<table id="tbl_rpt_today_sales"
							class="display nowrap table table-hover table-striped table-bordered"
							cellspacing="0" width="100%">
							<thead>
								<tr>
									<th>Purchase Time</th>
									<th>Item Name</th>
									<th>Quantity</th>
									<th>Total Sales Price</th>
									
								</tr>
							</thead>

							<tbody>
								<c:forEach items="${myModel.saleslist}" var="sales"
									varStatus="status">

									<tr>

										<td><c:out value="${sales[5]}" /></td>
										<td><c:out value="${sales[3]}" /></td>
										<td><c:out value="${sales[7]}" /></td>
										<td><c:out value="${sales[9]}" /></td>

									</tr>
								</c:forEach>
							</tbody>
						</table>
						</div>
								
								
							</li> 
							<li class="col-lg-3 col-xlg-4 m-b-30">
								<h4 class="m-b-20">List style</h4> List style
								<ul class="list-style-none">
									<li><a href="javascript:void(0)"><i
											class="fa fa-check text-success"></i> You can give link</a></li>
									<li><a href="javascript:void(0)"><i
											class="fa fa-check text-success"></i> Give link</a></li>
									<li><a href="javascript:void(0)"><i
											class="fa fa-check text-success"></i> Another Give link</a></li>
									<li><a href="javascript:void(0)"><i
											class="fa fa-check text-success"></i> Forth link</a></li>
									<li><a href="javascript:void(0)"><i
											class="fa fa-check text-success"></i> Another fifth link</a></li>
								</ul>
							</li> 
						</ul>
					</div></li>

				<li><label id="rolename" style="display: none;"><%=role%></label>
				</li>
				<!-- ============================================================== -->
				<!-- End Messages -->
				<!-- ============================================================== -->


			</ul>
			<!-- ============================================================== -->
			<!-- User profile and search -->
			<!-- ============================================================== -->
			<ul class="navbar-nav my-lg-0">
				<!-- ============================================================== -->
				<!-- Comment -->
				<!-- ============================================================== -->
				<!-- <li class="nav-item dropdown"><a
					class="nav-link dropdown-toggle text-muted text-muted waves-effect waves-dark"
					href="#" data-toggle="dropdown" aria-haspopup="true"
					aria-expanded="false"> <i class="mdi mdi-message"></i>
						<div class="notify"></div>
				</a>
					<div class="dropdown-menu dropdown-menu-right mailbox scale-up">
						<ul>
							<li>
								<div class="drop-title">Notifications</div>
							</li>
							<li>
								<div class="message-center">
									Message
									<a href="#">
										<div class="btn btn-danger btn-circle">
											<i class="fa fa-link"></i>
										</div>
										<div class="mail-contnet">
											<h5>Luanch Admin</h5>
											<span class="mail-desc">Just see the my new admin!</span> <span
												class="time">9:30 AM</span>
										</div>
									</a>
									Message
									<a href="#">
										<div class="btn btn-success btn-circle">
											<i class="ti-calendar"></i>
										</div>
										<div class="mail-contnet">
											<h5>Event today</h5>
											<span class="mail-desc">Just a reminder that you have
												event</span> <span class="time">9:10 AM</span>
										</div>
									</a>
									Message
									<a href="#">
										<div class="btn btn-info btn-circle">
											<i class="ti-settings"></i>
										</div>
										<div class="mail-contnet">
											<h5>Settings</h5>
											<span class="mail-desc">You can customize this
												template as you want</span> <span class="time">9:08 AM</span>
										</div>
									</a>
									Message
									<a href="#">
										<div class="btn btn-primary btn-circle">
											<i class="ti-user"></i>
										</div>
										<div class="mail-contnet">
											<h5>Pavan kumar</h5>
											<span class="mail-desc">Just see the my admin!</span> <span
												class="time">9:02 AM</span>
										</div>
									</a>
								</div>
							</li>
							<li><a class="nav-link text-center"
								href="javascript:void(0);"> <strong>Check all
										notifications</strong> <i class="fa fa-angle-right"></i>
							</a></li>
						</ul>
					</div></li> -->
				<!-- ============================================================== -->
				<!-- End Comment -->
				<!-- ============================================================== -->
				<!-- ============================================================== -->
				<!-- Messages -->
				<!-- ============================================================== -->
				<li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle text-muted waves-effect waves-dark"
					href="#" id="2" data-toggle="dropdown" aria-haspopup="true"
					aria-expanded="false"> <i class="mdi mdi-email"></i>
					 <div id="notifynewid" class="notify" style="top:-12px;right:-4px;display: none;"> <span class="heartbit"></span> <span class="point"></span> </div>
						<div>
							<span id="notifyid" class="round round-danger"
								style="width: 22px; height: 22px; margin-top: -10px; position: sticky; line-height: 20px; display: none;"><font
								size="2px">${fn:length(myModel.itemlist2)}</font></span>
						</div>
				</a>
					<div class="dropdown-menu mailbox dropdown-menu-right scale-up"
						aria-labelledby="2">
						<ul>
							<li>
								<div class="drop-title">${fn:length(myModel.itemlist2)}
									items below reorder level</div>
							</li>
							<li>
								<div class="message-center">


									<table cellspacing="0" width="100%">
										<thead>

										</thead>

										<tbody>
											<c:forEach items="${myModel.itemlist2}" var="items"
												varStatus="status">




												<tr>
													<a href="#"
														onclick="loadPage('editItem?id=${items.id}', 'container');">
														<div class="btn btn-success btn-circle">
															<i class="mdi mdi-email mdi-18px"></i>
														</div>
														<div class="mail-contnet">
															<h5><b>${items.quantity}</b> &nbsp;${items.itemname}
																remaning in stock
															</h5>
														</div> <%-- <td><c:out value="${items}" /></td> --%>
													</a>
												</tr>

											</c:forEach>
										</tbody>
									</table>


								</div>
							</li>
							<li><a class="nav-link text-center" href="#"
								onclick="loadPage('reorder_list.htm', 'container');"> <strong>See full Reorder List </strong>
							</a></li>
						</ul>
					</div></li>
				<!-- ============================================================== -->
				<!-- End Messages -->
				<!-- ============================================================== -->

				<!-- ============================================================== -->
				<!-- Profile -->
				<!-- ============================================================== -->
				<li class="nav-item dropdown"><a
					class="nav-link dropdown-toggle text-muted waves-effect waves-dark"
					href="#" data-toggle="dropdown" aria-haspopup="true"
					aria-expanded="false"><img src="resources/images/user.png"
						alt="user" class="profile-pic" /></a>
					<div class="dropdown-menu dropdown-menu-right scale-up">
						<ul class="dropdown-user">
							<li>
								<div class="dw-user-box">
									<div class="u-img">
										<img src="resources/images/user.png" alt="user">
									</div>
									<div class="u-text">
										<h4><%=username%></h4>
										<p class="text-muted"><%=role%></p>
									<a href="#"
											class="btn btn-rounded btn-danger btn-sm" onclick="loadPage('editUser?username=<c:out value='<%=username%>'/>', 'container');">View Profile</a> 
									</div>
								</div>
							</li>
							<li role="separator" class="divider"></li>
						    <li><a href="#" onclick="loadPage('editUser?username=<c:out value='<%=username%>'/>', 'container');"><i class="ti-user"></i> My Profile</a></li> 
							<li role="separator" class="divider"></li>
							<li><a href="getselectdata.htm"><i class="fa fa-power-off"></i>
									Logout</a></li>
						</ul>
					</div></li>
				<!-- ============================================================== -->
				<!-- Language -->
				<!-- ============================================================== -->
				<!-- <li class="nav-item dropdown"><a
					class="nav-link dropdown-toggle text-muted waves-effect waves-dark"
					href="#" data-toggle="dropdown" aria-haspopup="true"
					aria-expanded="false"> <i class="flag-icon flag-icon-us"></i></a>
					<div class="dropdown-menu dropdown-menu-right scale-up">
						<a class="dropdown-item" href="#"><i
							class="flag-icon flag-icon-in"></i> India</a> <a
							class="dropdown-item" href="#"><i
							class="flag-icon flag-icon-fr"></i> French</a> <a
							class="dropdown-item" href="#"><i
							class="flag-icon flag-icon-cn"></i> China</a> <a
							class="dropdown-item" href="#"><i
							class="flag-icon flag-icon-de"></i> Dutch</a>
					</div></li> -->
			</ul>
		</div>
		</nav> </header>
		<!-- ============================================================== -->
		<!-- End Topbar header -->
		<!-- ============================================================== -->
		<!-- ============================================================== -->
		<!-- Left Sidebar - style you can find in sidebar.scss  -->
		<!-- ============================================================== -->
		<aside class="left-sidebar"> <!-- Sidebar scroll-->
		<div class="scroll-sidebar">
			<!-- User profile -->

			<!-- End User profile text-->
			<!-- Sidebar navigation-->
			<nav class="sidebar-nav">
			<ul id="sidebarnav">

				<!-- <li><a class="waves-effect waves-dark" href="#"
					aria-expanded="false" onclick="index.htm"><i class="mdi mdi-home"></i><span
						class="hide-menu">Home</span></a></li> -->
				<li><a class="waves-effect waves-dark" href="#"
					onclick="loadPage('items.htm', 'container');" aria-expanded="false"><i
						class="mdi mdi-tag-multiple"></i><span class="hide-menu">Items</span></a></li>
				<li><a class="waves-effect waves-dark" href="#"
					onclick="loadPage('sales.htm', 'container');" aria-expanded="false"><i
						class="fa fa-money"></i><span class="hide-menu">Sales</span></a></li>
						
				<li><a class="waves-effect waves-dark" href="#"
				onclick="loadPage('load_supplier_stocks.htm', 'container');"
					aria-expanded="false"><i class="mdi mdi-truck-delivery"></i><span
						class="hide-menu">Suppliers</span></a></li>		
 		<li><a class="waves-effect waves-dark" href="#"
					onclick="loadPage('reports.htm', 'container');"
					aria-expanded="false"><i class="mdi mdi-chart-bar"></i><span
						class="hide-menu">Analyse</span></a></li> 
				
				<li class="nav-devider"></li>
				<li class="nav-small-cap">Account &amp; Settings</li>
			<!-- 	<li><a class="waves-effect waves-dark" href="#"
					aria-expanded="false"><i class="mdi mdi-square-inc-cash"></i><span
						class="hide-menu">Expenses</span></a></li> -->
				<li><a class="waves-effect waves-dark" href="#"
					onclick="loadPage('user.htm', 'container');" aria-expanded="false"><i
						class="mdi mdi-account"></i><span class="hide-menu">Users</span></a>

				</li>
				<li><a class="waves-effect waves-dark" href="#"
					onclick="loadPage('backup.htm', 'container');"
					aria-expanded="false"><i class="mdi mdi-database"></i><span
						class="hide-menu">Backup</span></a></li>



			</ul>
			</nav>
			<!-- End Sidebar navigation -->
		</div>
		<!-- End Sidebar scroll--> <!-- Bottom points-->
		<div class="sidebar-footer">
			<!-- item-->
			<a href="#" class="link" data-toggle="tooltip" title="Settings"><i
				class="ti-settings"></i></a>
			<!-- item-->
			<a href="#" class="link" data-toggle="tooltip" title="Email"><i
				class="mdi mdi-gmail"></i></a>
			<!-- item-->
			<a href="login.htm" class="link" data-toggle="tooltip" title="Logout"><i
				class="mdi mdi-power"></i></a>
				
		</div>
		<!-- End Bottom points--> </aside>
		<!-- ============================================================== -->
		<!-- End Left Sidebar - style you can find in sidebar.scss  -->
		<!-- ============================================================== -->
		<!-- ============================================================== -->
		<!-- Page wrapper  -->
		<!-- ============================================================== -->
		<div class="page-wrapper">

			<!-- end of the header page -->





			<div class="container-fluid" id="container">
				<!-- ============================================================== -->
				<!-- Bread crumb and right sidebar toggle -->
				<!-- ============================================================== -->
				<br>
				<!-- ============================================================== -->
				<!-- End Bread crumb and right sidebar toggle -->
				<!-- ============================================================== -->
				<!-- ============================================================== -->
				<!-- Start Page Content -->
				<!-- ============================================================== -->

				<div class="row" id="indexstat" style="display: none;">
					<!-- Column -->
					<div class="col-lg-3 col-md-6">
						<div class="card" style="background-color: rgba(72, 133, 237, 0.6);">
							<div class="card-body">
								<div class="d-flex flex-row">
									<div class="round round-lg align-self-center round-info" style="background-color:#7393ca">
										<i class="ti-wallet"></i>
									</div>
									<div class="m-l-10 align-self-center">
										<%-- <h3 class="m-b-0 font-light">Rs ${myModel.todaySalessum + 0.0}</h3> --%>
										<h3 style="color:#ffffff;" class='numscroller' data-slno='1' data-min='0' data-max=${myModel.todaySalessum} data-delay='10' data-increment="50" ></h3>
										<h5 class="text-muted m-b-0"><font color="white">Today Revenue</font></h5>
									</div>
								</div>
							</div>
							<!-- <button class="tst3 btn btn-success">Message</button> -->
						</div>
					</div>
					<!-- Column -->
					<!-- Column -->
					<div class="col-lg-3 col-md-6">
						<div class="card" style="background-color: rgba(219, 50, 54, 0.6);">
							<div class="card-body">
								<div class="d-flex flex-row">
									<div class="round round-lg align-self-center round-warning" style="background-color:#c17576">
										<i class="mdi mdi-cash-usd"></i>
									</div>
									<div class="m-l-10 align-self-center">
										<%-- <h3 class="m-b-0 font-lgiht">Rs ${myModel.todayProfitsum + 0.0}</h3> --%>
										<h3 style="color:#ffffff;" class='numscroller' data-slno='1' data-min='0' data-max=${myModel.todayProfitsum} data-delay='10' data-increment="20"></h3>
										<h5 class="text-muted m-b-0"><font color="white">Today Profit</font></h5>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- Column -->
					<!-- Column -->
					<div class="col-lg-3 col-md-6">
						<div class="card" style="background-color: rgba(244, 194, 13, 0.6);">
							<div class="card-body">
								<div class="d-flex flex-row">
									<div class="round round-lg align-self-center round-primary" style="background-color:#d4be6e">
										<i class="mdi mdi-cash-multiple"></i>
									</div>
									<div class="m-l-10 align-self-center">
										<%-- <h3 class="m-b-0 font-lgiht">Rs ${myModel.invoice + 0.0}</h3> --%>
										<h3 style="color:#ffffff;" class='numscroller' data-slno='1' data-min='0' data-max=${myModel.invoice} data-delay='10' data-increment="500"></h3>
										<h5 class="text-muted m-b-0"><font color="white">Total Revenue</font></h5>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- Column -->
					<!-- Column -->
					<div class="col-lg-3 col-md-6">
						<div class="card" style="background-color: rgba(60, 186, 84, 0.6);">
							<div class="card-body">
								<div class="d-flex flex-row">
									<div class="round round-lg align-self-center round-danger" style="background-color:#60b370">
										<i class="mdi mdi-cash-100"></i>
									</div>
									<div class="m-l-10 align-self-center">
										<%-- <h3 class="m-b-0 font-lgiht">Rs ${myModel.profitsum + 0.0}</h3> --%>
										<h3 style="color:#ffffff" class='numscroller' data-slno='1' data-min='0' data-max=${myModel.profitsum} data-delay='10' data-increment="200"></h3>
										<h5 class="text-muted m-b-0"><font color="white">Total Profit</font></h5>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- Column -->
				</div>
				<!-- Row -->
				<div class="row">

					<div class="col-lg-8 col-md-12">
						<div class="card">
							<div class="card-body">
								
								<div class="table-responsive">
									<table id="tbl_index"
										class="display nowrap table table-hover table-striped table-bordered"
										cellspacing="0" width="100%">
										<thead>
											<tr>
												<th width="15%" style="display:none;">Id</th>
												<th width="15%">Code</th>
												<th width="15%">Name</th>
												<th width="15%" style="display: none;">Category</th>
												<th width="10%">Pos</th>
												<th width="15%" style="display: none;">Unit price</th>
												<th width="20%">Cost</th>
												<th width="10%">Qty</th>
												<th width="10%">Dis%</th>
												<th width="20%" style="display: none;">Unit Total</th>
												<th width="20%">Total</th>
												<th width="20%" style="display: none;">reorder</th>
												<th width="10%">Add</th>
											</tr>
										</thead>

										<tbody>
											<c:forEach items="${myModel.itemlist}" var="items"
												varStatus="status">
												<%-- ${status.index} --%>
												<c:set value="${items.quantity}" var="qty" />
												<c:set value="${items.reorderlevel}" var="reorder" />
												<c:choose>
												
													<c:when test="${qty == 0}">


														<tr id="test" style="background-color: #ff5b5b;" >
															<td width="15%" style="display:none;"><input
																type="hidden" id="id${status.index}" value="${items.id}" />
															<c:out value="${items.id}" /></td>
															<td width="15%"><input type="hidden"
																id="itemid${status.index}" value="${items.itemid}" />
															<c:out value="${items.itemid}" /></td>
															<td width="15%"><input type="hidden"
																id="itemname${status.index}" value="${items.itemname}" />
																<c:out value="${items.itemname} " /></td>
															<td width="15%" style="display: none;"><input
																type="hidden" id="category${status.index}"
																value="${items.category}" />
															<c:out value="${items.category}" /></td>
															<td><c:out value="${items.position}" /></td>
															<td width="15%" style="display: none;"><input
																type="hidden" id="unitprice${status.index}"
																value="${items.unitprice}" /> <c:out
																	value="${items.unitprice}" /></td>
															<td width="26%"><input type="hidden"
																id="salesprice${status.index}"
																value="${items.salesprice}" /> <c:out
																	value="${items.salesprice} " /></td>
															<td width="10%"><input type="number"
																style="width: 40px" name="qty${status.index}"
																id="qty${status.index}" value="1"
																onClick="calcTotal(${status.index})" min="1"
																max="${items.quantity}" disabled="disabled"></td>
															<td width="7%"><input type="number"
																style="width: 40px" name="disc${status.index}"
																id="disc${status.index}"
																onClick="calcTotal(${status.index})" value="0" min="0"
																max="80" step="5" disabled="disabled"></td>
															<td width="25%" style="display: none;"><input
																class="form-control" type="text"
																name="unittotal${status.index}"
																id="unittotal${status.index}" value="${items.unitprice}"
																readonly=""></td>
															<td width="25%"><input class="form-control"
																type="text" name="total${status.index}"
																id="total${status.index}" value="${items.salesprice}"
																readonly=""></td>

															<td width="15%" style="display: none;"><input
																type="hidden" id="reorderlevel${status.index}"
																value="${items.reorderlevel}" />
															<c:out value="${items.reorderlevel}" /></td>
															<td width="2%"><button type="button"
																	style="width: 38px" onClick="addCart(${status.index})"
																	class="btn btn-primary btn-circle " disabled="disabled">
																	<i class="fa fa-plus"></i>
																</button></td>
														</tr>
													</c:when>
												
												
												
													<c:when test="${qty <= reorder}">


														<tr id="test" style="background-color: #ffff4d;">
															<td width="15%" style="display:none;"><input
																type="hidden" id="id${status.index}" value="${items.id}" />
															<c:out value="${items.id}" /></td>
															<td width="15%"><input type="hidden"
																id="itemid${status.index}" value="${items.itemid}" />
															<c:out value="${items.itemid}" /></td>
															<td width="15%"><input type="hidden"
																id="itemname${status.index}" value="${items.itemname}" />
																<c:out value="${items.itemname} " /></td>
															<td width="15%" style="display: none;"><input
																type="hidden" id="category${status.index}"
																value="${items.category}" />
															<c:out value="${items.category}" /></td>
															<td><c:out value="${items.position}" /></td>
															<td width="15%" style="display: none;"><input
																type="hidden" id="unitprice${status.index}"
																value="${items.unitprice}" /> <c:out
																	value="${items.unitprice}" /></td>
															<td width="26%"><input type="hidden"
																id="salesprice${status.index}"
																value="${items.salesprice}" /> <c:out
																	value="${items.salesprice} " /></td>
															<td width="10%"><input type="number"
																style="width: 40px" name="qty${status.index}"
																id="qty${status.index}" value="1"
																onClick="calcTotal(${status.index})" min="1"
																max="${items.quantity}"></td>
															<td width="7%"><input type="number"
																style="width: 40px" name="disc${status.index}"
																id="disc${status.index}"
																onClick="calcTotal(${status.index})" value="0" min="0"
																max="80" step="5"></td>
															<td width="25%" style="display: none;"><input
																class="form-control" type="text"
																name="unittotal${status.index}"
																id="unittotal${status.index}" value="${items.unitprice}"
																readonly=""></td>
															<td width="25%"><input class="form-control"
																type="text" name="total${status.index}"
																id="total${status.index}" value="${items.salesprice}"
																readonly=""></td>

															<td width="15%" style="display: none;"><input
																type="hidden" id="reorderlevel${status.index}"
																value="${items.reorderlevel}" />
															<c:out value="${items.reorderlevel}" /></td>
															<td width="2%"><button type="button"
																	style="width: 38px" onClick="addCart(${status.index})"
																	class="btn btn-primary btn-circle ">
																	<i class="fa fa-plus"></i>
																</button></td>
														</tr>
													</c:when>

													<c:otherwise>

														<tr id="test">
															<td width="15%" style="display:none;"><input
																type="hidden" id="id${status.index}" value="${items.id}" />
															<c:out value="${items.id}" /></td>
															<td width="15%"><input type="hidden"
																id="itemid${status.index}" value="${items.itemid}" />
															<c:out value="${items.itemid}" /></td>
															<td width="15%"><input type="hidden"
																id="itemname${status.index}" value="${items.itemname}" />
																<c:out value="${items.itemname} " /></td>
															<td width="15%" style="display: none;"><input
																type="hidden" id="category${status.index}"
																value="${items.category}" />
															<c:out value="${items.category}" /></td>
															<td><c:out value="${items.position}" /></td>
															<td width="15%" style="display: none;"><input
																type="hidden" id="unitprice${status.index}"
																value="${items.unitprice}" /> <c:out
																	value="${items.unitprice}" /></td>
															<td width="26%"><input type="hidden"
																id="salesprice${status.index}"
																value="${items.salesprice}" /> <c:out
																	value="${items.salesprice} " /></td>
															<td width="10%"><input type="number"
																style="width: 40px" name="qty${status.index}"
																id="qty${status.index}" value="1"
																onClick="calcTotal(${status.index})" min="1"
																max="${items.quantity}"></td>
															<td width="7%"><input type="number"
																style="width: 40px" name="disc${status.index}"
																id="disc${status.index}"
																onClick="calcTotal(${status.index})" value="0" min="0"
																max="80" step="5"></td>
															<td width="25%" style="display: none;"><input
																class="form-control" type="text"
																name="unittotal${status.index}"
																id="unittotal${status.index}" value="${items.unitprice}"
																readonly=""></td>
															<td width="25%"><input class="form-control"
																type="text" name="total${status.index}"
																id="total${status.index}" value="${items.salesprice}"
																readonly=""></td>

															<td width="15%" style="display: none;"><input
																type="hidden" id="reorderlevel${status.index}"
																value="${items.reorderlevel}" />
															<c:out value="${items.reorderlevel}" /></td>												
															<td width="2%"><button type="button"
																	style="width: 38px" onClick="addCart(${status.index})"
																	class="btn btn-primary btn-circle ">
																	<i class="fa fa-plus"></i>
																</button></td>
														</tr>
													</c:otherwise>
												</c:choose>


											</c:forEach>
										</tbody>
									</table>
								</div>


							</div>
						</div>
					</div>
					<div class="col-lg-4 col-md-12">
						<div class="card">
						<!-- <div class="card-body printableArea"> -->
							<div class="card-body">
								<div class="table-responsive">
									<tr>
										<td>Invoice No :</td> &nbsp;&nbsp;&nbsp;
										 <td><label id="invoiceid">IV_${myModel.num23}</label></td> 

									</tr>
									<table class="table color-bordered-table info-bordered-table"
										id="abctable">
										<thead>
											<tr>
												<th>Item Name</th>
												<th>Qty</th>
												<th>#</th>
												<th style="display: none;">id</th>
												<th style="display: none;">unipr</th>
												<th style="display: none;">category</th>
												<th style="display: none;">reorder</th>
												<th style="display: none;">unittoal</th>
												<th>X</th>
											</tr>
										</thead>
										<tbody>


										</tbody>
									</table>
								</div>

								<div class="table-responsive">
									<table
										class="table color-bordered-table primary-bordered-table">

										<tbody>
											<tr>
												<td>Sub Total</td>
												<td><label id="sub_total">0.0</label></td>
											</tr>
											<tr style="display: none;">
												<td>Unit Total</td>
												<td><label id="unit_total">0.0</label></td>
											</tr>
											<tr>
												<td style="width: 50%">Discount %</td>
												<td style="width: 50%"><input class="form-control"
													id="total_discount" type="number"
													onClick="addTotalDiscount()"
													onkeypress="addTotalDiscount()" min="0" max="80" value="0"
													step="5"></td>
											</tr>

											<tr>
												<td><h2>Total (Rs)</h2></td>
												<td><h2>
														<label id="total_amount">0.0</label>
														<h2></td>
											</tr>
											<tr style="display: none;">
												<td>Profit</td>
												<td><label id="profit">0.0</label></td>
											</tr>
										</tbody>
									</table>


							<button class="btn btn-danger" onclick="loadPrintInvoice();"
										data-toggle="modal" data-target=".sam"
										class="model_img img-responsive "><span>Proceed to payment</span></button>
										
									<!--#fc4b6c   <button onclick="getinvoice();addToSales();"
										class="btn btn-danger" type="submit">Proceed to
										payment</button> -->
									&nbsp;&nbsp;
									<!-- <button id="print22" class="btn btn-info" type="button">
										<span><i class="fa fa-print"></i> Print</span>
									</button> -->

									

									<!-- <button class="btn btn-sucess" onclick="addToSales();">Add</button> -->
									<br>


								</div>

							</div>
						</div>
					</div>

				</div>
				<!-- Row -->
				<!-- Row -->




				<div class="modal fade sam" tabindex="-1" role="dialog"
					aria-labelledby="myLargeModalLabel" aria-hidden="true"
					style="display: none;">
					<div class="modal-dialog modal-lg">
						<div class="modal-content">
							<%-- <div class="modal-header printableArea">
								<!-- <h4 class="modal-title" id="myLargeModalLabel">Invoice</h4> -->

			<div><center><img src="resources/images/pos-logo.png"></center></div> <br>
			

							</div> --%>




							<div class="modal-body printableArea" >

								


								<table class="table color-bordered-table muted-bordered-table"
									id="prnt_table">
									<thead>
										<tr style="background-color:red;">
										
											<th colspan="4"><center><img src="resources/images/pos-logo.png"></center></th>
										

										</tr>
										<tr>
										
											<th colspan="2"><center>No 123 Colombo 06</center></th>
											<th colspan="2"><center>Tel : 0112876456</center></th>
										

										</tr>
										<tr>
											<th>Item Name</th>
											<th>Quantity</th>
											<th>Discount</th>
											<th>Price</th>

										</tr>
									</thead>
									<tbody>


									</tbody>
								</table>

								<table class="table color-bordered-table muted-bordered-table">

									<tbody>
										<tr>
											<td></td>
											<td></td>
											<td>Sub Total :</td>
											<td>Rs <label id="prnt_sub_total"></label>
											</td>
										</tr>

										<tr>
										<td></td>
											<td></td>
											<td>Total Discount :</td>
											<td><label id="prnt_total_discount"></label> %</td>
										</tr>

										<tr style="border: double;">
										<td></td>
											<td></td>
											<td>Total :</td>
											<td >Rs <label id="prnt_total_price"></label></td>
										</tr>


									</tbody>

								</table>
								
								<table class="table">

									<tbody>

										<tr>
											<td>Invoice no : IV_${myModel.invoiceid.get(0)+ 1}</td>
											<td>Date of Issue : <label id="prnt_date"></label></td>
											<td>Time of Issue : <label id="prnt_time"></label></td>

										</tr>

									</tbody>
								</table>


							</div>
							<div class="modal-footer">



								<button type="button"
									class="btn btn-danger waves-effect text-left"
									data-dismiss="modal" onclick="clearTable();">Close</button>
									
									<button id="print" class="btn btn-info" type="button" onclick="getinvoice();addToSales();">
										<span><i class="fa fa-print"></i> Print</span>
									</button>
							</div>
						</div>
						<!-- /.modal-content -->
					</div>
					<!-- /.modal-dialog -->
				</div>







			</div>









			<!-- start the footer page -->


			<!-- footer -->
			<!-- ============================================================== -->
			<footer class="footer">
			<center>� 2020 ZeroBug Team . All Right Reserved &#9786;</center>
			</footer>
			<!-- ============================================================== -->
			<!-- End footer -->
			<!-- ============================================================== -->
		</div>
		<!-- ============================================================== -->
		<!-- End Page wrapper  -->
		<!-- ============================================================== -->
	</div>
	<!-- ============================================================== -->
	<!-- End Wrapper -->
	<!-- ============================================================== -->
	<!-- ============================================================== -->
	<!-- All Jquery -->
	<!-- ============================================================== -->
	<script src="resources/js/jquery.min.js"></script>
	<script src="resources/js/script.js"></script>

	<!-- Bootstrap tether Core JavaScript -->
	<script src="resources/js/popper.min.js"></script>
	<script src="resources/js/bootstrap.min.js"></script>
	<!-- slimscrollbar scrollbar JavaScript -->
	<script src="resources/js/jquery.slimscroll.js"></script>
	<!--Wave Effects -->
	<script src="resources/js/waves.js"></script>
	<!--Menu sidebar -->
	<script src="resources/js/sidebarmenu.js"></script>
	<!--stickey kit -->
	<script src="resources/js/sticky-kit.min.js"></script>
	<script src="resources/js/jquery.sparkline.min.js"></script>
	<!--Custom JavaScript -->
	<script src="resources/js/custom.min.js"></script>
	<script src="resources/js/validation.js"></script>
	<!-- ============================================================== -->
	<!-- This page plugins -->
	<!-- ============================================================== -->
	<!-- chartist chart -->
	<script src="resources/js/chartist.min.js"></script>
	<script src="resources/js/chartist-plugin-tooltip.min.js"></script>
	<!--c3 JavaScript -->
	<script src="resources/js/d3.min.js"></script>
	<script src="resources/js/c3.min.js"></script>
	<!-- Vector map JavaScript -->
	<script src="resources/js/jquery-jvectormap-2.0.2.min.js"></script>
	<script src="resources/js/jquery-jvectormap-us-aea-en.js"></script>
	<script src="resources/js/dashboard2.js"></script>
	<!-- ============================================================== -->

	<script src="resources/js/sweetalert.min.js"></script>
	<script src="resources/js/jquery.sweet-alert.custom.js"></script>

	<script src="resources/js/moment.js"></script>
	<script src="resources/js/bootstrap-material-datetimepicker.js"></script>
	<script src="resources/js/bootstrap-datepicker.min.js"></script>

	<script src="resources/js/select2.full.min.js"></script>
	<script src="resources/js/bootstrap-select.min.js"></script>

	<!-- This is data table -->
	<script src="resources/js/jquery.dataTables.min.js"></script>
	<!-- start - This is for export functionality only -->
	<script src="resources/js/dataTables.buttons.min.js"></script>
	<script src="resources/js/buttons.flash.min.js"></script>
	<script src="resources/js/jszip.min.js"></script>
	<script src="resources/js/pdfmake.min.js"></script>
	<script src="resources/js/vfs_fonts.js"></script>
	<script src="resources/js/buttons.html5.min.js"></script>
	<script src="resources/js/buttons.print.min.js"></script>
	<script src="resources/js/canval_imal.js"></script>
	<script src="resources/js/canvas.js"></script>
	
	<script src="resources/js/numscroller-1.0.js"></script>

	<script src="resources/js/jquery.toast.js"></script>
	<script src="resources/js/toastr.js"></script>
	
	<script src="https://cdn.rawgit.com/serratus/quaggaJS/0420d5e0/dist/quagga.min.js"></script>

<script>
	$('#tbl_rpt_today_sales').DataTable({
    dom: 'Bfrtip',
    buttons: [
        'copy', 'csv', 'excel', 'pdf', 'print'
    ]
});
</script>
	
	<script>
    // MAterial Date picker    
   // MAterial Date picker    
    $('#mdate').bootstrapMaterialDatePicker({
        weekStart: 0,
        time: false
    });
    $('#mdate2').bootstrapMaterialDatePicker({
        weekStart: 0,
        time: false
    });
    $('#mdate3').bootstrapMaterialDatePicker({
        weekStart: 0,
        time: false
    });
    $('#mdate4').bootstrapMaterialDatePicker({
        weekStart: 0,
        time: false
    });
    $('#mdate5').bootstrapMaterialDatePicker({
        weekStart: 0,
        time: false
    });
    $('#mdate6').bootstrapMaterialDatePicker({
        weekStart: 0,
        time: false
    });
    $('#mdate7').bootstrapMaterialDatePicker({
        weekStart: 0,
        time: false
    });
    $('#mdate8').bootstrapMaterialDatePicker({
        weekStart: 0,
        time: false
    });
    </script>

	<!-- end - This is for export functionality only -->
	<script>
    $(document).ready(function() {
    	
    	
     /* chanaka js implementation start */
    	
    	
    	/* var _scannerIsRunning = false;

        function startScanner() {
            Quagga.init({
                inputStream: {
                    name: "Live",
                    type: "LiveStream",
                    target: document.querySelector('#scanner-container'),
                    constraints: {
                        width: 480,
                        height: 320,
                        facingMode: "environment"
                    },
                },
                decoder: {
                    readers: [
                        "code_128_reader",
                        "ean_reader",
                        "ean_8_reader",
                        "code_39_reader",
                        "code_39_vin_reader",
                        "codabar_reader",
                        "upc_reader",
                        "upc_e_reader",
                        "i2of5_reader"
                    ],
                    debug: {
                        showCanvas: true,
                        showPatches: true,
                        showFoundPatches: true,
                        showSkeleton: true,
                        showLabels: true,
                        showPatchLabels: true,
                        showRemainingPatchLabels: true,
                        boxFromPatches: {
                            showTransformed: true,
                            showTransformedBox: true,
                            showBB: true
                        }
                    }
                },

            }, function (err) {
                if (err) {
                    console.log(err);
                    return
                }

                console.log("Initialization finished. Ready to start");
                Quagga.start();

                // Set flag to is running
                _scannerIsRunning = true;
            });

            Quagga.onProcessed(function (result) {
                var drawingCtx = Quagga.canvas.ctx.overlay,
                drawingCanvas = Quagga.canvas.dom.overlay;

                if (result) {
                    if (result.boxes) {
                        drawingCtx.clearRect(0, 0, parseInt(drawingCanvas.getAttribute("width")), parseInt(drawingCanvas.getAttribute("height")));
                        result.boxes.filter(function (box) {
                            return box !== result.box;
                        }).forEach(function (box) {
                            Quagga.ImageDebug.drawPath(box, { x: 0, y: 1 }, drawingCtx, { color: "green", lineWidth: 2 });
                        });
                    }

                    if (result.box) {
                        Quagga.ImageDebug.drawPath(result.box, { x: 0, y: 1 }, drawingCtx, { color: "#00F", lineWidth: 2 });
                    }

                    if (result.codeResult && result.codeResult.code) {
                        Quagga.ImageDebug.drawPath(result.line, { x: 'x', y: 'y' }, drawingCtx, { color: 'red', lineWidth: 3 });
                    }
                }
            });


            Quagga.onDetected(function (result) {
                console.log("Barcode detected and processed : [" + result.codeResult.code + "]", result);
            });
        }


        // Start/stop scanner
        document.getElementById("btn").addEventListener("click", function () {
            if (_scannerIsRunning) {
                Quagga.stop();
            } else {
                startScanner();
            }
        }, false); */ 
        
        
    	
    	/* chanaka js implemetation end */
    	
        $('#myTable').DataTable();
        $(document).ready(function() {
            var table = $('#example').DataTable({
                "columnDefs": [{
                    "visible": false,
                    "targets": 2
                }],
                "order": [
                    [2, 'asc']
                ],
                "displayLength": 25,
                "drawCallback": function(settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;
                    api.column(2, {
                        page: 'current'
                    }).data().each(function(group, i) {
                        if (last !== group) {
                            $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                            last = group;
                        }
                    });
                }
            });
            // Order by the grouping
            $('#example tbody').on('click', 'tr.group', function() {
                var currentOrder = table.order()[0];
                if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                    table.order([2, 'desc']).draw();
                } else {
                    table.order([2, 'asc']).draw();
                }
            });
        });
    });
    
    $('#tbl_index').DataTable({
        dom: 'Bfrtip',
        buttons: [
        ]
    });
    </script>
   



	<script>
    jQuery(document).ready(function() {
        // Switchery
       
        // For select 2
        $(".select2").select2();
        $('.selectpicker').selectpicker();
        //Bootstrap-TouchSpin
        
        // For multiselect
       
        $(".ajax").select2({
            ajax: {
                url: "https://api.github.com/search/repositories",
                dataType: 'json',
                delay: 250,
                data: function(params) {
                    return {
                        q: params.term, // search term
                        page: params.page
                    };
                },
                processResults: function(data, params) {
                    // parse the results into the format expected by Select2
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data, except to indicate that infinite
                    // scrolling can be used
                    params.page = params.page || 1;
                    return {
                        results: data.items,
                        pagination: {
                            more: (params.page * 30) < data.total_count
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function(markup) {
                return markup;
            }, // let our custom formatter work
            minimumInputLength: 1,
            templateResult: formatRepo, // omitted for brevity, see the source of this page
            templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
        });
    });
    </script>


	<script>
	$('#example44').DataTable({
		dom : 'Bfrtip',
		buttons : [ 'copy', 'csv', 'excel', 'pdf', 'print' ]
	});
</script>

	<script src="resources/js/jquery.PrintArea.js" type="text/JavaScript"></script>
	<script>
    $(document).ready(function() {
    	
    	
    	
    	
        $("#print").click(function() {
            var mode = 'iframe'; //popup
            var close = mode == "popup";
            var options = {
                mode: mode,
                popClose: close
            };
            $("div.printableArea").printArea(options);
        });
    });
    </script>

	<script>
    ! function(window, document, $) {
        "use strict";
        $("input,select,textarea").not("[type=submit]").jqBootstrapValidation(), $(".skin-square input").iCheck({
            checkboxClass: "icheckbox_square-green",
            radioClass: "iradio_square-green"
        }), $(".touchspin").TouchSpin(), $(".switchBootstrap").bootstrapSwitch();
    }(window, document, jQuery);
    </script>


	
	
</body>



</html>

