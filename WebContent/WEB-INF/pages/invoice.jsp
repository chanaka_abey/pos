<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<script>
    $(document).ready(function() {
        $('#myTable').DataTable();
        $(document).ready(function() {
            var table = $('#example').DataTable({
                "columnDefs": [{
                    "visible": false,
                    "targets": 2
                }],
                "order": [
                    [2, 'asc']
                ],
                "displayLength": 25,
                "drawCallback": function(settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;
                    api.column(2, {
                        page: 'current'
                    }).data().each(function(group, i) {
                        if (last !== group) {
                            $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                            last = group;
                        }
                    });
                }
            });
            // Order by the grouping
            $('#example tbody').on('click', 'tr.group', function() {
                var currentOrder = table.order()[0];
                if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                    table.order([2, 'desc']).draw();
                } else {
                    table.order([2, 'asc']).draw();
                }
            });
        });
    });
    
    $('#tbl_rpt_invoice_overall').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
    </script>


</script>

<script>
    function selectRow(rowIndex){
    	//alert(rowIndex);
    	//var Rowvalue = document.getElementById(rowIndex);
    	var id = document.getElementById('id'+rowIndex).value;
    	var invoicenumber = document.getElementById('invoicenumber'+rowIndex).value;
    //	alert("id  : " + id);
    	
    	
    	
    //Parameter
    $('#sa-params'+rowIndex).click(function(){
        swal({   
            title: "Are you sure? Delete "+invoicenumber,   
            text: "You will not be able to recover "+invoicenumber+" this invoice!",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#DD6B55",   
            confirmButtonText: "Yes, delete it!",   
            cancelButtonText: "No, cancel !",   
            closeOnConfirm: false,   
            closeOnCancel: false 
        }, function(isConfirm){   
            if (isConfirm) {     
                swal("Deleted!", invoicenumber +" has been deleted.", "success");  
               // alert("delete id " + id);
                loadPage('deleteInvoice?id='+id, 'container');
                getSalesFromInvoiceno(rowIndex);
            } else {     
                swal("Cancelled", invoicenumber+ " is safe ", "error");   
            } 
        });
    });
    
    
   
    
    }
    
    </script>

<script>
    
    function getPreviewID(rowIndex){
    	var invoicenumber = document.getElementById('invoicenumber'+rowIndex).value;
    	//alert("Preview ID : " + invoicenumber);
    	
    	var purchasedate = document.getElementById('purchasedate'+rowIndex).value;
    	var purchasetime = document.getElementById('purchasetime'+rowIndex).value;
    	var totalsaleprice = document.getElementById('totalsalesprice'+rowIndex).value;
    	var totaldiscount = document.getElementById('totaldiscount'+rowIndex).value;
    	
    	//alert(purchasedate+ " " + purchasetime +"  " + totalsaleprice +" " + totaldiscount);
    	
    	var discountprice = 100 - totaldiscount;
		var salesamount = totalsaleprice * 100;
		var salesprice = salesamount / discountprice;
    	
    	document.getElementById('prnt_sub_total').innerText = salesprice;
    	document.getElementById('prnt_total_discount').innerText = totaldiscount;

    	document.getElementById('prnt_total_price').innerText = totalsaleprice;
    	
    	document.getElementById('prnt_invoicenumber').innerText = invoicenumber;
    	document.getElementById('prnt_date').innerText = purchasedate;
    	document.getElementById('prnt_time').innerText = purchasetime;
    	
    	//loadPage('load_sales_preview.htm?invoicenumber='+encodeURIComponent(invoicenumber), 'container');
    
    	$.ajax({
    		 url: "load_sales_by_invoice_no.htm",
    		  type: "GET",
    		  data: { invoicenumber : invoicenumber},
    		  dataType: "json",
    		  cache: false,
    		  success: function(data) {
    			 	//alert("Success");	
    			 	// var contact = JSON.parse(data);
    			 //	 alert(contact);
    			  console.log(data);
    			 	 console.log(data[0].Itemname);
    			 	 
    			 	for (var i = 0, len = data.length; i < len; i++){
    			 		console.log(data[i]);
    			 		var obj = JSON.parse(data[i]);
    			 		 
    			 		console.log(obj.Itemname);
    			 	    console.log(obj.Quantity);
    			 	    console.log(obj.Discount);
    			 	    console.log(obj.Price);
    			 	   // alert(obj.Itemname +"  "+obj.Quantity+"  "+obj.Discount+"  " + obj.Price);
    			 		
    			 	   var table = document.getElementById("prnt_table");
    			 		
    			 		 var row = table.insertRow(-1);
    			 		 var cell1 = row.insertCell(0);
    			 		 var cell2 = row.insertCell(1);
    			 		 var cell3 = row.insertCell(2);
    			 		 var cell4 = row.insertCell(3);
    			 		 
    			 		// alert("cell1 " + cell1.innerHTML + "  cell2 " + cell2.innerHTML + "  cell3 " + cell3.innerHTML );
    			 		
    			 		  cell1.innerHTML = obj.Itemname;
    			 		  cell2.innerHTML = obj.Quantity;
    			 		  cell3.innerHTML = obj.Discount + " %";
    			 		  cell4.innerHTML = obj.Price;
    			 		  
    			 		
    			 			 
    			 	    
    			 	    
    			 		}
    		},
    		error: function(data) {
    			alert("error");	
    		 
    		          console.log(data);           
    		                }
    						});
    
    }
    
    </script>

<script>
    
    function getSalesFromInvoiceno(rowIndex){
    	var invoicenumber = document.getElementById('invoicenumber'+rowIndex).value;
    	
    	$.ajax({
   		 url: "getSalesFromInvoicenumber",
   		  type: "GET",
   		  data: { invoicenumber : invoicenumber},
   		  dataType: "json",
   		  cache: false,
   		  success: function(data) {
   			 //	alert("Success");	
   			 	// var contact = JSON.parse(data);
   			 //	 alert(contact);
   			  console.log(data);
   			 	 console.log(data[0].Itemname);
   			 	 
   			 	for (var i = 0, len = data.length; i < len; i++){
   			 		console.log(data[i]);
   			 		var obj = JSON.parse(data[i]);
   			 		 
   			 		//console.log(obj.Itemname);
   			 	   // console.log(obj.Quantity);
   			 	   // console.log(obj.salesid);
   			 	    //console.log(obj.Price);
   			 	  // alert(obj.Itemname +"  "+obj.Quantity+"  "+obj.salesid);

   			 	deletesalesfrominvoice(obj.salesid,obj.item_id,obj.Quantity);
   			 		}
   		},
   		error: function(data) {
   			alert("error");	
   		 
   		          console.log(data);           
   		                }
   						});				
    }
    
    </script>
    
    <script>
    
    function deletesalesfrominvoice(id,item_id,qty){
    //	alert("  item_id  " + item_id);
    	
    	
    	$.ajax({
			method : "POST",
			url : 'deletesalefrominvoice',
			data : {
				id : id,
				item_id : item_id,
				qty : qty
			},
			dataType : "json",
			cache : false,
			success : function() {

				//alert("success");
			},
			error : function() {
				//alert("Error..! " );
				//location.reload();
			}
		}); 
    	
    }
    
    </script>


<script>

function loadPrintInvoice(){
	
	alert("loadPrintInvoice");
	
	 var myTable = document.getElementById("abctable");
	 
	 alert("myTable.rows.length : " + myTable.rows.length);
	 for (var r=1, n = myTable.rows.length; r < n; r++) {

for (var c = 0, m = myTable.rows[r].cells.length; c < m; c++) {


	
	var itemname = myTable.rows[r].cells[0].innerHTML;
	var quntity = myTable.rows[r].cells[1].innerHTML;
	var discount = myTable.rows[r].cells[2].innerHTML;
	var price = myTable.rows[r].cells[3].innerHTML;


	

}
alert("itemname " + itemname +" quntity " +quntity + " price" + price);


/* var table = document.getElementById("prnt_table");

 var row = table.insertRow(-1);
 var cell1 = row.insertCell(0);
 var cell2 = row.insertCell(1);
 var cell3 = row.insertCell(2);
 var cell4 = row.insertCell(3); */
	 
	 }
	 
	 
	 
}
</script>



<br>

<nav class="breadcrumb" style="background-color: #c0d8f1;">
	<a class="breadcrumb-item" href="index.htm">Home</a> <a
		class="breadcrumb-item" href="#"
		onclick="loadPage('sales.htm', 'container');">Sales</a> <span
		class="breadcrumb-item active">Invoice List</span>
</nav>
<br>


<div class="row" id="indexstat">
	<!-- Column -->
	<div class="col-lg-6 col-md-8">
		<div class="card">
			<a href="#" onclick="loadPage('sales.htm', 'container');">
				<div class="card-body">
					<div class="d-flex flex-row">
						<div class="round round-lg align-self-center round-info le">
							<i class="mdi mdi-cash-multiple"></i>
						</div>
						<div class="m-l-10 align-self-center">

							<h5 class="text-muted m-b-0">Sales List</h5>
						</div>
					</div>
				</div>
			</a>
		</div>
	</div>
	<!-- Column -->
	<!-- Column -->
	<div class="col-lg-6 col-md-8">
		<div class="card">
			<a href="#" onclick="loadPage('invoice.htm', 'container');">
				<div class="card-body">
					<div class="d-flex flex-row">
						<div class="round round-lg align-self-center round-warning le">
							<i class="mdi mdi-newspaper"></i>
						</div>
						<div class="m-l-10 align-self-center">

							<h5 class="text-muted m-b-0">Invoice List</h5>
						</div>
					</div>
				</div>
			</a>
		</div>
	</div>
	<!-- Column -->
	<!-- Column -->
	<!-- chanaka implemetation -->
	<!-- <div class="col-lg-3 col-md-6">
		<div class="card">
			<a href="#" onclick="loadPage('sales_reports.htm', 'container');">
				<div class="card-body">
					<div class="d-flex flex-row">
						<div class="round round-lg align-self-center round-primary">
							<i class="mdi mdi-chart-line"></i>
						</div>
						<div class="m-l-10 align-self-center">

							<h5 class="text-muted m-b-0">Sales Reports</h5>
						</div>
					</div>
				</div>
			</a>
		</div>
	</div> -->
	<!-- Column -->
	<!-- Column -->
	<!-- chanaka implementation -->
<!-- 	<div class="col-lg-4 col-md-6">
		<div class="card">
			<a href="#" onclick="loadPage('invoice_reports.htm', 'container');">
				<div class="card-body">
					<div class="d-flex flex-row">
						<div class="round round-lg align-self-center round-danger">
							<i class="mdi mdi-chart-areaspline"></i>
						</div>
						<div class="m-l-10 align-self-center">

							<h5 class="text-muted m-b-0">Invoice Reports</h5>
						</div>
					</div>
				</div>
			</a>
		</div>
	</div> -->
	<!-- Column -->
</div>

<div class="row">
	<div class="col-12">
		<div class="card">
			<div class="card-body">

				<div class="card card-outline-info">
					<div class="card-header">
						<center>
							<h4 class="m-b-0 text-white">Invoice List</h4>
						</center>
					</div>
				</div>

				<div class="table-responsive">
					<table id="tbl_rpt_invoice_overall"
						class="display nowrap table table-hover table-striped table-bordered"
						cellspacing="0" width="100%">
						<thead>
							<tr>

								<th>Invoice No</th>
								<th>Purchase Date</th>
								<th>Purchase Time</th>
								<th>Total Salesprice</th>
								<th>Total Unitprice</th>
								<th>Total Disc</th>
								<th>Profit</th>
								<th style="display: none;">ID</th>
								<th><button type="button" class="btn btn-secondary">
										<i class="fa fa-photo"></i>
									</button></th>
								<th><button type="button" class="btn btn-secondary">
										<i class="fa fa-trash-o"></i>
									</button></th>

							</tr>
						</thead>

						<tbody>
							<c:forEach items="${myModel.invoicelist}" var="invoice"
								varStatus="status">

								<tr>

									<td><input type="hidden" id="invoicenumber${status.index}"
										value="${invoice.invoicenumber}" /> <c:out
											value="${invoice.invoicenumber}" /></td>
									<td><input type="hidden" id="purchasedate${status.index}"
										value="${invoice.purchasedate}" />
									<c:out value="${invoice.purchasedate}" /></td>
									<td><input type="hidden" id="purchasetime${status.index}"
										value="${invoice.purchasetime}" />
									<c:out value="${invoice.purchasetime}" /></td>
									<td><input type="hidden"
										id="totalsalesprice${status.index}"
										value="${invoice.totalsalesprice}" />
									<c:out value="${invoice.totalsalesprice}" /></td>
									<td><c:out value="${invoice.totalunitprice}" /></td>
									<td><input type="hidden" id="totaldiscount${status.index}"
										value="${invoice.totaldiscount}" />
									<c:out value="${invoice.totaldiscount}" /></td>
									<td><c:out value="${invoice.profit}" /></td>
									<td style="display: none;"><input type="hidden"
										id="id${status.index}" value="${invoice.id}" /> <c:out
											value="${invoice.id}" /></td>
									<td><button type="button" class="btn btn-success"
											onClick="getPreviewID(${status.index});" data-toggle="modal"
											data-target=".sam" class="model_img img-responsive">
											<i class="fa fa-photo"></i>
										</button></td>
									<td id="sa-params${status.index}"><button type="button"
											class="btn btn-danger" onClick="selectRow(${status.index});">
											<i class="fa fa-trash-o"></i>
										</button></td>

								</tr>
							</c:forEach>
						</tbody>
					</table>



				</div>


			</div>
		</div>
	</div>
</div>

<div class="modal fade sam" tabindex="-1" role="dialog"
	aria-labelledby="myLargeModalLabel" aria-hidden="true"
	style="display: none;">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">

			<div class="modal-body printableArea">


				<table class="table color-bordered-table muted-bordered-table"
					id="prnt_table">
					<thead>
						<tr style="background-color: red;">

							<th colspan="4"><center>
									<img src="resources/images/pos-logo.png">
								</center></th>


						</tr>
						<tr>

							<th colspan="2"><center>No 123 Colombo 06</center></th>
							<th colspan="2"><center>Tel : 0112876456</center></th>


						</tr>
						<tr>
							<th>Item Name</th>
							<th>Quantity</th>
							<th>Discount</th>
							<th>Price</th>

						</tr>
					</thead>

					<tbody>

					</tbody>

				</table>

				<table class="table color-bordered-table muted-bordered-table">

					<tbody>
						<tr>
							<td></td>
							<td></td>
							<td>Sub Total :</td>
							<td>Rs <label id="prnt_sub_total"></label>
							</td>
						</tr>

						<tr>
							<td></td>
							<td></td>
							<td>Total Discount :</td>
							<td><label id="prnt_total_discount"></label> %</td>
						</tr>

						<tr style="border: double;">
							<td></td>
							<td></td>
							<td>Total :</td>
							<td>Rs <label id="prnt_total_price"></label></td>
						</tr>


					</tbody>

				</table>

				<table class="table">

					<tbody>

						<tr>
							<td>Invoice no : <label id="prnt_invoicenumber"></label></td>
							<td>Date of Issue : <label id="prnt_date"></label></td>
							<td>Time of Issue : <label id="prnt_time"></label></td>

						</tr>

					</tbody>
				</table>


			</div>
			<div class="modal-footer">



				<button type="button" class="btn btn-danger waves-effect text-left"
					data-dismiss="modal" onclick="clearTable();">Close</button>

				<button id="print" class="btn btn-info" type="button">
					<span><i class="fa fa-print"></i> Print</span>
				</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>

