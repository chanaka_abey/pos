<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

	<br>
	
		<nav class="breadcrumb" style="background-color:#c0d8f1;">
                                    <a class="breadcrumb-item" href="index.htm">Home</a>
                                    <a class="breadcrumb-item" href="#" onclick="loadPage('reports.htm', 'container');">Reports</a>
                                    <span class="breadcrumb-item active">Invoice Reports</span>
      </nav>
  <br>
	
	<div class="row" id="indexstat">
		<!-- Column -->
		<div class="col-lg-4 col-md-6">
			<div class="card">
				<a href="#" onclick="loadPage('rpt_invoice_overall.htm', 'container');">
					<div class="card-body">
						<div class="d-flex flex-row">
							<div class="round round-lg align-self-center round-info">
								<i class="ti-wallet"></i>
							</div>
							<div class="m-l-10 align-self-center">

								<h5 class="text-muted m-b-0">Overall Invoices Report</h5>
							</div>
						</div>
					</div>
				</a>
			</div>
		</div>
		<!-- Column -->
		<!-- Column -->
		<!-- <div class="col-lg-4 col-md-6">
			<div class="card">
			<a href="#" onclick="loadPage('rpt_invoices_by_date.htm', 'container');">
				<div class="card-body">
					<div class="d-flex flex-row">
						<div class="round round-lg align-self-center round-warning">
							<i class="mdi mdi-cellphone-link"></i>
						</div>
						<div class="m-l-10 align-self-center">

							<h5 class="text-muted m-b-0">Invoices by date Report</h5>
						</div>
					</div>
				</div>
				</a>
			</div>
		</div> -->
		<!-- Column -->
		<!-- Column -->
		<div class="col-lg-4 col-md-6">
			<div class="card">
			<a href="#" onclick="loadPage('rpt_invoices_by_date_range.htm', 'container');">
				<div class="card-body">
					<div class="d-flex flex-row">
						<div class="round round-lg align-self-center round-primary">
							<i class="mdi mdi-cart-outline"></i>
						</div>
						<div class="m-l-10 align-self-center">

							<h5 class="text-muted m-b-0">Invoices by date range Report</h5>
						</div>
					</div>
				</div>
				</a>
			</div>
		</div>
		<!-- Column -->
		<div class="col-lg-4 col-md-6">
			<div class="card">
			<a href="#" onclick="loadPage('rpt_profit_by_date_range.htm', 'container');">
				<div class="card-body">
					<div class="d-flex flex-row">
						<div class="round round-lg align-self-center round-warning">
							<i class="mdi mdi-cellphone-link"></i>
						</div>
						<div class="m-l-10 align-self-center">

							<h5 class="text-muted m-b-0">Total Profit by Date Range</h5>
						</div>
					</div>
				</div>
				</a>
			</div>
		</div>
	</div>


	<div class="row" id="indexstat">
		<!-- Column -->
	<!-- 	<div class="col-lg-4 col-md-6">
			<div class="card">
				<a href="#" onclick="loadPage('rpt_profit_by_date.htm', 'container');">
					<div class="card-body">
						<div class="d-flex flex-row">
							<div class="round round-lg align-self-center round-info">
								<i class="ti-wallet"></i>
							</div>
							<div class="m-l-10 align-self-center">

								<h5 class="text-muted m-b-0">Total Profit by Date Report</h5>
							</div>
						</div>
					</div>
				</a>
			</div>
		</div> -->
		<!-- Column -->
		<!-- Column -->
		
		<!-- Column -->
		<!-- Column -->
		<!-- <div class="col-lg-4 col-md-6">
			<div class="card">
			<a href="">
				<div class="card-body">
					<div class="d-flex flex-row">
						<div class="round round-lg align-self-center round-primary">
							<i class="mdi mdi-cart-outline"></i>
						</div>
						<div class="m-l-10 align-self-center">

							<h5 class="text-muted m-b-0">Not mention yet</h5>
						</div>
					</div>
				</div>
				</a>
			</div>
		</div> -->
		<!-- Column -->
		
	</div>


