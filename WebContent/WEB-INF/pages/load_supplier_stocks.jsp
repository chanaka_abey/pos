<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<script>
    $(document).ready(function() {
        $('#myTable').DataTable();
        $(document).ready(function() {
            var table = $('#example').DataTable({
                "columnDefs": [{
                    "visible": false,
                    "targets": 2
                }],
                "order": [
                    [2, 'asc']
                ],
                "displayLength": 25,
                "drawCallback": function(settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;
                    api.column(2, {
                        page: 'current'
                    }).data().each(function(group, i) {
                        if (last !== group) {
                            $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                            last = group;
                        }
                    });
                }
            });
            // Order by the grouping
            $('#example tbody').on('click', 'tr.group', function() {
                var currentOrder = table.order()[0];
                if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                    table.order([2, 'desc']).draw();
                } else {
                    table.order([2, 'asc']).draw();
                }
            });
        });
    });
    
    $('#tbl_item').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
    </script>
    
    <script>
    function selectRow(rowIndex){
    	//alert(rowIndex);
    	//var Rowvalue = document.getElementById(rowIndex);
    	var supplierdate = document.getElementById('supplierdate'+rowIndex).value;
    	var suppliername = document.getElementById('suppliername'+rowIndex).value;
    	//alert("itemname : " + itemname);
    	
    	
    	
    //Parameter
    $('#sa-params'+rowIndex).click(function(){
        swal({   
            title: "Are you sure? Delete "+supplierdate +" in "+suppliername +" stock",   
            text: "You will not be able to recover "+supplierdate +" in "+suppliername+" stock this store!",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#DD6B55",   
            confirmButtonText: "Yes, delete it!",   
            cancelButtonText: "No, cancel !",   
            closeOnConfirm: false,   
            closeOnCancel: false 
        }, function(isConfirm){   
            if (isConfirm) {     
                swal("Deleted!", supplierdate +" in "+suppliername +" has been deleted.", "success");   
                loadPage('deleteSupplierStock?id='+rowIndex, 'container');
                deteleQuantityFromItems(rowIndex);
            } else {     
                swal("Cancelled", supplierdate +" in "+suppliername + " is safe ", "error");   
               
            } 
        });
    });
    
    
   
    
    }
    
    </script>
    
    <script>
    function deteleQuantityFromItems(rowIndex){
    	//alert("deteleQuantityFromItems");
    	var quantity = document.getElementById('quantityid'+rowIndex).value;
    	var itemidid = document.getElementById('itemid'+rowIndex).value;
    	
    	//alert("quantity " + quantity + " and Itemid : " + itemidid);
    	
    	 $.ajax({
    			method : "POST",
    			url : 'deletequantityfromitems',
    			data : {
    				itemidid : itemidid,
    				quantity : quantity
    			},
    			dataType : "json",
    			cache : false,
    			success : function() {

    				//alert("success");
    			},
    			error : function() {
    				//alert("Error..! " );
    				//location.reload();
    			}
    		}); 
    	
    }
    </script>


	<br>
	
	<nav class="breadcrumb" style="background-color:#c0d8f1;">
                                    <a class="breadcrumb-item" href="index.htm">Home</a> 
                                    <span class="breadcrumb-item active">Suppliers</span>
      </nav>
  <br>
	
	<div class="row" id="itemstat">
	
		<!-- Column -->
		<div class="col-lg-3 col-md-6">
			<div class="card">
			<a href="#" onclick="loadPage('add_new_supplier_stock.htm', 'container');">
				<div class="card-body">
					<div class="d-flex flex-row">
						<div class="round round-lg align-self-center round-primary">
							<i class="mdi mdi-store"></i>
						</div>
						<div class="m-l-10 align-self-center">

							<h5 class="text-muted m-b-0">Add new Stock</h5>
						</div>
					</div>
				</div>
				</a>
			</div>
		</div>
		<!-- Column -->
		<!-- Column -->
		<div class="col-lg-3 col-md-6">
			<div class="card">
				<a href="#" onclick="loadPage('add_new_supplier.htm', 'container');">
					<div class="card-body">
						<div class="d-flex flex-row">
							<div class="round round-lg align-self-center round-info">
								<i class="mdi mdi-truck"></i>
							</div>
							<div class="m-l-10 align-self-center">

								<h5 class="text-muted m-b-0">Add New Supplier</h5>
							</div>
						</div>
					</div>
				</a>
			</div>
		</div>
		<!-- Column -->
		<!-- Column -->
		<div class="col-lg-3 col-md-6">
			<div class="card">
			<a href="#" onclick="loadPage('suppliers.htm', 'container');">
				<div class="card-body">
					<div class="d-flex flex-row">
						<div class="round round-lg align-self-center round-danger">
							<i class="mdi mdi-truck-delivery"></i>
						</div>
						<div class="m-l-10 align-self-center">

							<h5 class="text-muted m-b-0">Supplier List</h5>
						</div>
					</div>
				</div>
				</a>
			</div>
		</div>
		<!-- Column -->
		<!-- Column -->
		
		
		<!-- <div class="col-lg-3 col-md-6">
			<div class="card">
			<a href="#"  onclick="loadPage('supplier_reports.htm', 'container');">
				<div class="card-body">
					<div class="d-flex flex-row">
						<div class="round round-lg align-self-center round-warning">
							<i class="mdi mdi-chart-areaspline"></i>
						</div>
						<div class="m-l-10 align-self-center">

							<h5 class="text-muted m-b-0">Suppliers Reports</h5>
						</div>
					</div>
				</div>
				</a>
			</div>
		</div> -->
		
		<div class="col-lg-3 col-md-6">
			<div class="card">
			<a href="#"  onclick="loadPage('create_supplier_item.htm', 'container');">
				<div class="card-body">
					<div class="d-flex flex-row">
						<div class="round round-lg align-self-center round-warning">
							<i class="mdi mdi-chart-areaspline"></i>
						</div>
						<div class="m-l-10 align-self-center">

							<h5 class="text-muted m-b-0">Create Supplier Item</h5>
						</div>
					</div>
				</div>
				</a>
			</div>
		</div>
		
		<!-- Column -->
		
		
	</div>



	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-body">
				
				<div class="card card-outline-info">
				<div class="card-header"><center><h4 class="m-b-0 text-white">Supplier Stocks</h4></center></div>
				</div>

					<div class="table-responsive">
						<table id="tbl_item"
							class="display nowrap table table-hover table-striped table-bordered"
							cellspacing="0" width="100%">
							<thead>
								<tr>
								<!--  <th>ID</th> -->
									<th>Date</th>
									<th>Supplier</th>
									<th>Itemname</th>
									<th>Quantity</th>
									<th style="display: none;">Item_id</th>
									<th id="itemeditth"><button type="button" class="btn btn-secondary"><i class="fa fa-edit"></i></button></th>
									<th id="itemedeleteth"><button type="button" class="btn btn-secondary"><i class="fa fa-trash-o"></i></button></th>
								</tr>
							</thead>

							<tbody>
								<c:forEach items="${supplierStocks}" var="supplier" varStatus="status">
              						 
              						  <tr>
              						  
										<td><input type="hidden" id="supplierdate${supplier.id}" value="${supplier.date}" /><c:out value="${supplier.date}" /></td>
										<td><input type="hidden" id="suppliername${supplier.id}" value="${supplier.supplier_name}" /><c:out value="${supplier.supplier_name}" /></td>
										<td><c:out value="${supplier.itemname}" /></td>
										<td><input type="hidden" id="quantityid${supplier.id}" value="${supplier.quantity}" /><c:out value="${supplier.quantity}" /></td>
										<td style="display: none;"><input type="hidden" id="itemid${supplier.id}" value="${supplier.item_id}" /><c:out value="${supplier.item_id}" /></td>
										<td class="itemeditth2"><button type="button" class="btn btn-success" onclick="loadPage('editSupplierStock?id=<c:out value='${supplier.id}'/>', 'container');"><i class="fa fa-edit"></i></button></td>
									    <td class="itemdeleteth2" id="sa-params${supplier.id}"><button type="button" class="btn btn-danger" onClick="selectRow(${supplier.id});"><i class="fa fa-trash-o"></i></button></td>
									</tr>
              						  
              						
								</c:forEach>
							</tbody>
						</table>
					</div>


				</div>
			</div>
		</div>
	</div>

