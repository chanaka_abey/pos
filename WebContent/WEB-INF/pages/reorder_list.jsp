<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<script>
    $(document).ready(function() {
        $('#myTable').DataTable();
        $(document).ready(function() {
            var table = $('#example').DataTable({
                "columnDefs": [{
                    "visible": false,
                    "targets": 2
                }],
                "order": [
                    [2, 'asc']
                ],
                "displayLength": 25,
                "drawCallback": function(settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;
                    api.column(2, {
                        page: 'current'
                    }).data().each(function(group, i) {
                        if (last !== group) {
                            $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                            last = group;
                        }
                    });
                }
            });
            // Order by the grouping
            $('#example tbody').on('click', 'tr.group', function() {
                var currentOrder = table.order()[0];
                if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                    table.order([2, 'desc']).draw();
                } else {
                    table.order([2, 'asc']).draw();
                }
            });
        });
    });
    
    $('#tbl_reorder_list').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
    </script>

<script>
    function selectRow(rowIndex){
    	//alert(rowIndex);
    	//var Rowvalue = document.getElementById(rowIndex);
    	var itemname = document.getElementById('itemname'+rowIndex).value;
    	//alert("itemname : " + itemname);
    	
    	
    	
    //Parameter
    $('#sa-params'+rowIndex).click(function(){
        swal({   
            title: "Are you sure? Delete "+itemname,   
            text: "You will not be able to recover "+itemname+" this store!",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#DD6B55",   
            confirmButtonText: "Yes, delete it!",   
            cancelButtonText: "No, cancel !",   
            closeOnConfirm: false,   
            closeOnCancel: false 
        }, function(isConfirm){   
            if (isConfirm) {     
                swal("Deleted!", itemname +" has been deleted.", "success");   
                loadPage('deleteItem?id='+rowIndex, 'container');
            } else {     
                swal("Cancelled", itemname+ " is safe ", "error");   
            } 
        });
    });
    
    
   
    
    }
    
    </script>


<br>

<nav class="breadcrumb" style="background-color:#c0d8f1;">
                                    <a class="breadcrumb-item" href="index.htm">Home</a>
                                    <a class="breadcrumb-item" href="#" onclick="loadPage('items.htm', 'container');">Items</a>
                                    <span class="breadcrumb-item active">Reorder List</span>
      </nav>
  <br>

<div class="row" id="itemstat">
	<!-- Column -->
	<div class="col-lg-4 col-md-6">
		<div class="card">
			<a href="#" onclick="loadPage('add_new_item.htm', 'container');">
				<div class="card-body">
					<div class="d-flex flex-row">
						<div class="round round-lg align-self-center round-info">
							<i class="mdi mdi-tag-plus"></i>
						</div>
						<div class="m-l-10 align-self-center">

							<h5 class="text-muted m-b-0">Add New Item</h5>
						</div>
					</div>
				</div>
			</a>
		</div>
	</div>
	<!-- Column -->
	<!-- Column -->
	<div class="col-lg-4 col-md-6">
		<div class="card">
			<a href="#" onclick="loadPage('items.htm', 'container');">
				<div class="card-body">
					<div class="d-flex flex-row">
						<div class="round round-lg align-self-center round-warning">
							<i class="mdi mdi-tag-multiple"></i>
						</div>
						<div class="m-l-10 align-self-center">

							<h5 class="text-muted m-b-0">Item List</h5>
						</div>
					</div>
				</div>
			</a>
		</div>
	</div>
	<!-- Column -->
	<!-- Column -->
	<!-- chanaka implementation -->
<!-- 	<div class="col-lg-3 col-md-6">
		<div class="card">
			<a href="#" onclick="loadPage('stock_reports.htm', 'container');">
				<div class="card-body">
					<div class="d-flex flex-row">
						<div class="round round-lg align-self-center round-primary">
							<i class="mdi mdi-file-chart"></i>
						</div>
						<div class="m-l-10 align-self-center">

							<h5 class="text-muted m-b-0">Stock Reports</h5>
						</div>
					</div>
				</div>
			</a>
		</div>
	</div> -->
	<!-- Column -->
	<!-- Column -->
	<div class="col-lg-4 col-md-6">
		<div class="card">
		<a href="#" onclick="loadPage('reorder_list.htm', 'container');">
			<div class="card-body">
				<div class="d-flex flex-row">
					<div class="round round-lg align-self-center round-danger">
						<i class="mdi mdi-format-line-weight"></i>
					</div>
					<div class="m-l-10 align-self-center">

						<h5 class="text-muted m-b-0">Reorder List</h5>
					</div>
				</div>
			</div>
			</a>
		</div>
	</div>
	<!-- Column -->
</div>



<div class="row">
	<div class="col-12">
		<div class="card">
			<div class="card-body">
				<div class="card card-outline-info">
				<div class="card-header"><center><h4 class="m-b-0 text-white">Reorder List</h4></center></div>
				</div>

				<div class="table-responsive">
					<table id="tbl_reorder_list"
						class="display nowrap table table-hover table-striped table-bordered"
						cellspacing="0" width="100%">
						<thead>
							<tr>
								<!--  <th>ID</th> -->
								<th>Item ID</th>
								<th>Item Name</th>
								<th>Category</th>
								<th>Unit Price</th>
								<th>Sales Price</th>
								<th>Qty</th>
								<th>Reorder Level</th>
								<th id="itemeditth"><button type="button" class="btn btn-secondary"><i class="fa fa-edit"></i></button></th>

							</tr>
						</thead>

						<tbody>
							<c:forEach items="${itemlist}" var="items" varStatus="status">
								<c:set value="${items.quantity}" var="qty" />
								<c:set value="${items.reorderlevel}" var="reorder" />
								<c:choose>
									<c:when test="${qty <= reorder}">

										<tr>
											<%-- <td><c:out value="${items.id}" /></td> --%>
											<td><c:out value="${items.itemid}" /></td>
											<td><input type="hidden" id="itemname${items.id}"
												value="${items.itemname}" />
											<c:out value="${items.itemname}" /></td>
											<td><c:out value="${items.category}" /></td>
											<td><c:out value="${items.unitprice}" /></td>
											<td><c:out value="${items.salesprice}" /></td>
											<td><c:out value="${items.quantity}" /></td>
											<td><c:out value="${items.reorderlevel}" /></td>
											<td class="itemeditth2"><button type="button" class="btn btn-success" onclick="loadPage('editItem?id=<c:out value='${items.id}'/>', 'container');"><i class="fa fa-edit"></i></button></td>

										</tr>

									</c:when>
								</c:choose>
							</c:forEach>
						</tbody>
					</table>
				</div>


			</div>
		</div>
	</div>
</div>

