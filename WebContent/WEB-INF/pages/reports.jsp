<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


	<br>
	
	<nav class="breadcrumb" style="background-color:#c0d8f1;">
                                    <a class="breadcrumb-item" href="index.htm">Home</a>
                                    <a class="breadcrumb-item active" href="#" >Reports</a>
                                    
      </nav>
  <br>
	
	Reports
	<hr>
	<div class="row" id="indexstat">
		<!-- Column -->
		<div class="col-lg-3 col-md-6">
			<div class="card">
				<a href="#" onclick="loadPage('stock_reports.htm', 'container');">
					<div class="card-body">
						<div class="d-flex flex-row">
							<div class="round round-lg align-self-center round-info">
								<i class="mdi mdi-file-document"></i>
							</div>
							<div class="m-l-10 align-self-center">

								<h5 class="text-muted m-b-0">Stock Reports</h5>
							</div>
						</div>
					</div>
				</a>
			</div>
		</div>
		<!-- Column -->
		<!-- Column -->
		<div class="col-lg-3 col-md-6">
			<div class="card">
			<a href="#" onclick="loadPage('sales_reports.htm', 'container');">
				<div class="card-body">
					<div class="d-flex flex-row">
						<div class="round round-lg align-self-center round-warning">
							<i class="mdi mdi-file-chart"></i>
						</div>
						<div class="m-l-10 align-self-center">

							<h5 class="text-muted m-b-0">Sales Reports</h5>
						</div>
					</div>
				</div>
				</a>
			</div>
		</div>
		<!-- Column -->
		<!-- Column -->
		<div class="col-lg-3 col-md-6">
			<div class="card">
			<a href="#" onclick="loadPage('invoice_reports.htm', 'container');">
				<div class="card-body">
					<div class="d-flex flex-row">
						<div class="round round-lg align-self-center round-primary">
							<i class="mdi mdi-newspaper"></i>
						</div>
						<div class="m-l-10 align-self-center">

							<h5 class="text-muted m-b-0">Invoice Reports</h5>
						</div>
					</div>
				</div>
				</a>
			</div>
		</div>
		<div class="col-lg-3 col-md-6">
			<div class="card">
				<a href="#" onclick="loadPage('supplier_reports.htm', 'container');">
					<div class="card-body">
						<div class="d-flex flex-row">
							<div class="round round-lg align-self-center round-info" style="background: #14ab58;">
								<i class="mdi mdi-truck-delivery"></i>
							</div>
							<div class="m-l-10 align-self-center">

								<h5 class="text-muted m-b-0">Supplier Report</h5>
							</div>
						</div>
					</div>
				</a>
			</div>
		<!-- Column -->
		<!-- Column -->
	<!-- 	<div class="col-lg-3 col-md-6">
			<div class="card">
			<a href="#" onclick="loadPage('activity_reports.htm', 'container');">
				<div class="card-body" href="#">
					<div class="d-flex flex-row">
						<div class="round round-lg align-self-center round-danger">
							<i class="mdi mdi-account-card-details"></i>
						</div>
						<div class="m-l-10 align-self-center">

							<h5 class="text-muted m-b-0">Activity Reports</h5>
						</div>
					</div>
				</div>
				</a>
			</div>
		</div> -->
		<!-- Column -->
	</div>
	
	<div class="row">
		<!-- Column -->
<!-- 		 -->
		</div>
		
		
	<!-- chanaka implementation -->

<!-- 	<div class="col-lg-3 col-md-6">
			<div class="card">
			<a href="#" onclick="loadPage('rpt_activity_by_user.htm', 'container');">
				<div class="card-body">
					<div class="d-flex flex-row">
						<div class="round round-lg align-self-center round-primary">
							<i class="mdi mdi-account-card-details"></i>
						</div>
						<div class="m-l-10 align-self-center">

							<h5 class="text-muted m-b-0">User Reports</h5>
						</div>
					</div>
				</div>
				</a>
			</div>
		</div> -->
	
	</div>
	<!-- <div class="row" id="indexstat">
		Column
		<div class="col-lg-3 col-md-6">
			<div class="card">
				<a href="#">
					<div class="card-body">
						<div class="d-flex flex-row">
							<div class="round round-lg align-self-center round-info">
								<i class="ti-wallet"></i>
							</div>
							<div class="m-l-10 align-self-center">

								<h5 class="text-muted m-b-0">Suppliers Reports</h5>
							</div>
						</div>
					</div>
				</a>
			</div>
		</div>
		Column
		Column
		<div class="col-lg-3 col-md-6">
			<div class="card">
				<div class="card-body">
					<div class="d-flex flex-row">
						<div class="round round-lg align-self-center round-warning">
							<i class="mdi mdi-cellphone-link"></i>
						</div>
						<div class="m-l-10 align-self-center">

							<h5 class="text-muted m-b-0">Expences Reports</h5>
						</div>
					</div>
				</div>
			</div>
		</div>
		Column
		Column
		<div class="col-lg-3 col-md-6">
			<div class="card">
				<div class="card-body">
					<div class="d-flex flex-row">
						<div class="round round-lg align-self-center round-primary">
							<i class="mdi mdi-cart-outline"></i>
						</div>
						<div class="m-l-10 align-self-center">

							<h5 class="text-muted m-b-0">Backup Reports</h5>
						</div>
					</div>
				</div>
			</div>
		</div>
		Column
		Column
		<div class="col-lg-3 col-md-6">
			<div class="card">
			<a href="#" onclick="loadPage('charts.htm', 'container');">
				<div class="card-body">
					<div class="d-flex flex-row">
						<div class="round round-lg align-self-center round-danger">
							<i class="mdi mdi-bullseye"></i>
						</div>
						<div class="m-l-10 align-self-center">

							<h5 class="text-muted m-b-0">Other Reports</h5>
						</div>
					</div>
				</div>
				</a>
			</div>
		</div>
		Column
	</div> -->
	<br>

	Charts
	<hr>
	<div class="row" id="indexstat">
		<!-- Column -->
		<div class="col-lg-3 col-md-6">
			<div class="card">
				<a href="#" onclick="loadPage('rpt_charts_daily_sales.htm', 'container');">
					<div class="card-body">
						<div class="d-flex flex-row">
							<div class="round round-lg align-self-center round-info">
								<i class="mdi mdi-chart-line"></i>
							</div>
							<div class="m-l-10 align-self-center">

								<h5 class="text-muted m-b-0">Daily Sales Chart</h5>
							</div>
						</div>
					</div>
				</a>
			</div>
		</div>
		<!-- Column -->
		<!-- Column -->
		<div class="col-lg-3 col-md-6">
			<div class="card">
			<a href="#" onclick="loadPage('rpt_charts_daily_profit.htm', 'container');">
				<div class="card-body">
					<div class="d-flex flex-row">
						<div class="round round-lg align-self-center round-warning">
							<i class="mdi mdi-chart-areaspline"></i>
						</div>
						<div class="m-l-10 align-self-center">

							<h5 class="text-muted m-b-0">Daily Profit Chart</h5>
						</div>
					</div>
				</div>
				</a>
			</div>
		</div>
		<!-- Column -->
		<!-- Column -->
		<div class="col-lg-3 col-md-6">
			<div class="card">
			<a href="#" onclick="loadPage('rpt_charts_date_range_sales.htm', 'container');">
				<div class="card-body">
					<div class="d-flex flex-row">
						<div class="round round-lg align-self-center round-primary">
							<i class="mdi mdi-chart-bar"></i>
						</div>
						<div class="m-l-10 align-self-center">

							<h5 class="text-muted m-b-0">Date Range Sales </h5>
						</div>
					</div>
				</div>
				</a>
			</div>
		</div>
		<!-- Column -->
		<!-- Column -->
		<div class="col-lg-3 col-md-6">
			<div class="card">
			<a href="#" onclick="loadPage('rpt_charts_date_range_profit.htm', 'container');">
				<div class="card-body">
					<div class="d-flex flex-row">
						<div class="round round-lg align-self-center round-danger">
							<i class="ti-bar-chart"></i>
						</div>
						<div class="m-l-10 align-self-center">

							<h5 class="text-muted m-b-0">Date Range Profit</h5>
						</div>
					</div>
				</div>
				</a>
			</div>
		</div>
		<!-- Column -->
	</div>
	



	

