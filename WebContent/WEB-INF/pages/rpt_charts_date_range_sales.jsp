<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

  <script src="resources/js/canval_imal.js"></script>
<script src="resources/js/canvas.js"></script>
    <script>   
    $('#mdate').bootstrapMaterialDatePicker({
        weekStart: 0,
        time: false
    });
    $('#mdate2').bootstrapMaterialDatePicker({
        weekStart: 0,
        time: false
    });
    </script>
    
<script>
//window.onload = function () {
function loadcharts22() {	
	var mdate = document.getElementById("mdate").value;
    var mdate2 = document.getElementById("mdate2").value;
	//alert("ttttttttttttttts");
var dataPoints = [];
var chart = new CanvasJS.Chart("chartContainer33", {
	animationEnabled: true,
	theme: "light2",
	title: {
		text: "Date range Sales"
	},
	data: [
	{
		dataPoints: []
	}]
});


$.ajax({
    type: "GET",
    url: 'loaddaterangesalesdata?mdate='+mdate+'&mdate2='+mdate2,
    cache: false,
    contentType: "application/json; charset=utf-8",
    success: function (data) {
    	//alert(data);
        //console.log(data);
        var responsePIE = jQuery.parseJSON(data);
        for (var i = 0; i < responsePIE.length; i++) {
            //console.log(responsePIE.length);
            //console.log(responsePIE[i]);
            console.log(responsePIE[i].date);
            console.log(responsePIE[i].sales);
            dataPoints.push({label: responsePIE[i].date, y: responsePIE[i].sales});
        }
        //console.log("dataPoints in Ajx : " + dataPoints);
        chart.options.data[0].dataPoints = dataPoints;
        chart.render();
    },
    error: function (response) {
        alert('Error while request..');

    }
});

}
</script>


<br>

<nav class="breadcrumb" style="background-color:#c0d8f1;">
                                    <a class="breadcrumb-item" href="index.htm">Home</a>
                                    <a class="breadcrumb-item" href="#" onclick="loadPage('reports.htm', 'container');">Charts</a>
                                    <span class="breadcrumb-item active">Sales by Date Range Chart</span>
      </nav>
  <br>
	
	<div class="row">
		<div class="col-12">

			<div class="card">
				<div class="card-body">
				
					<div class="alert alert-warning">
						<div class="row">
							
									<div class="col-lg-3 col-md-4">Select Date
										&nbsp;&nbsp;&nbsp; From:</div>
									<div class="col-lg-3 col-md-4">

										<input type="text"
											class="form-control" placeholder="select date" id="mdate" />

									</div>
									To :
									<div class="col-lg-3 col-md-4">

										<input  type="text"
											class="form-control" placeholder="select date" id="mdate2" />

									</div>
									<div class="col-lg-2 col-md-4">
										<button type="button" onclick="loadcharts22();" class="btn btn-info">Check</button>

									</div>
								
						</div>
					</div>
				
					<!-- <h3>Sales Chart</h3> -->
	
					<div id="chartContainer33" style="height: 370px; width: 100%;">
					
					</div>
					
				</div>
			</div>
		</div>
	</div>