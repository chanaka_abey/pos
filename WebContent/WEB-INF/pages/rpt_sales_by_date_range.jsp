<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<script>
    $(document).ready(function() {
        $('#myTable').DataTable();
        $(document).ready(function() {
            var table = $('#example').DataTable({
                "columnDefs": [{
                    "visible": false,
                    "targets": 2
                }],
                "order": [
                    [2, 'asc']
                ],
                "displayLength": 25,
                "drawCallback": function(settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;
                    api.column(2, {
                        page: 'current'
                    }).data().each(function(group, i) {
                        if (last !== group) {
                            $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                            last = group;
                        }
                    });
                }
            });
            // Order by the grouping
            $('#example tbody').on('click', 'tr.group', function() {
                var currentOrder = table.order()[0];
                if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                    table.order([2, 'desc']).draw();
                } else {
                    table.order([2, 'asc']).draw();
                }
            });
        });
    });
    
    $('#tbl_rpt_sales_by_date_range').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
    </script>
    <script>   
    $('#mdate').bootstrapMaterialDatePicker({
        weekStart: 0,
        time: false
    });
    $('#mdate2').bootstrapMaterialDatePicker({
        weekStart: 0,
        time: false
    });
    </script>
    <script> 
    function getDetails(){
     var mdate = document.getElementById("mdate").value;
     var mdate2 = document.getElementById("mdate2").value;
     loadPage('rpt_sales_by_date_range.htm?mdate='+mdate+"&mdate2="+mdate2, 'container');
    }
    </script>


	<br>
	
	<nav class="breadcrumb" style="background-color:#c0d8f1;">
                                    <a class="breadcrumb-item" href="index.htm">Home</a>
                                    <a class="breadcrumb-item" href="#" onclick="loadPage('reports.htm', 'container');">Reports</a>
                                    <a class="breadcrumb-item" href="#" onclick="loadPage('sales_reports.htm', 'container');">Sales Reports</a>
                                    <span class="breadcrumb-item active">Sales by Date Range Report</span>
      </nav>
  <br>


	<div class="row">
		<div class="col-12">

			<div class="card">
				<div class="card-body">
				
				<div class="card card-outline-info">
				<div class="card-header"><center><h4 class="m-b-0 text-white">Sales by Date Range Report</h4></center></div>
				</div>
				
					


					<div class="alert alert-warning">
						<div class="row">
							
									<div class="col-lg-3 col-md-4">Select Date
										&nbsp;&nbsp;&nbsp; From:</div>
									<div class="col-lg-3 col-md-4">

										<input type="text"
											class="form-control" placeholder="select date" id="mdate" />

									</div>
									To :
									<div class="col-lg-3 col-md-4">

										<input  type="text"
											class="form-control" placeholder="select date" id="mdate2" />

									</div>
									<div class="col-lg-2 col-md-4">
										<button type="button" onclick="getDetails();" class="btn btn-info">Check</button>

									</div>
								
						</div>
					</div>


					<div class="table-responsive">
						<table id="tbl_rpt_sales_by_date_range"
							class="display nowrap table table-hover table-striped table-bordered"
							cellspacing="0" width="100%">
							<thead>
								<tr>
									<th>Purchase Date</th>
									<th>Purchase Time</th>
									<th>Invoice No</th>
									<th>Item ID</th>
									<th>Item Name</th>
									<th>Sales Price</th>
									<th>Qty</th>
									<th>Disc</th>
									<th>Total Sales Price</th>
								</tr>
							</thead>

							<tbody>
								<c:forEach items="${saleslist}" var="sales" varStatus="status">

									<tr>
										<td><c:out value="${sales.purchasedate}" /></td>
										<td><c:out value="${sales.purchasetime}" /></td>
										<td><c:out value="${sales.invoicenumber}" /></td>
										<td><c:out value="${sales.itemid}" /></td>
										<td><c:out value="${sales.itemname}" /></td>
										<td><c:out value="${sales.salesprice}" /></td>
										<td><c:out value="${sales.quantity}" /></td>
										<td><c:out value="${sales.discount}" /></td>
										<td><c:out value="${sales.totalsalesprice}" /></td>

									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>


				</div>
			</div>
		</div>
	</div>

