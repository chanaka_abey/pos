<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<script>
    $(document).ready(function() {
        $('#myTable').DataTable();
        $(document).ready(function() {
            var table = $('#example').DataTable({
                "columnDefs": [{
                    "visible": false,
                    "targets": 2
                }],
                "order": [
                    [2, 'asc']
                ],
                "displayLength": 25,
                "drawCallback": function(settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;
                    api.column(2, {
                        page: 'current'
                    }).data().each(function(group, i) {
                        if (last !== group) {
                            $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                            last = group;
                        }
                    });
                }
            });
            // Order by the grouping
            $('#example tbody').on('click', 'tr.group', function() {
                var currentOrder = table.order()[0];
                if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                    table.order([2, 'desc']).draw();
                } else {
                    table.order([2, 'asc']).draw();
                }
            });
        });
    });
    
    $('#tbl_rpt_sales_itemwise').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
    </script>
    
    
    <script>
    jQuery(document).ready(function() {
        // Switchery
       
        // For select 2
        $(".itemselect3").select2();
        $('.selectpicker').selectpicker();
        //Bootstrap-TouchSpin
        
        // For multiselect
       
        $(".ajax").select2({
            ajax: {
                url: "https://api.github.com/search/repositories",
                dataType: 'json',
                delay: 250,
                data: function(params) {
                    return {
                        q: params.term, // search term
                        page: params.page
                    };
                },
                processResults: function(data, params) {
                    // parse the results into the format expected by Select2
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data, except to indicate that infinite
                    // scrolling can be used
                    params.page = params.page || 1;
                    return {
                        results: data.items,
                        pagination: {
                            more: (params.page * 30) < data.total_count
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function(markup) {
                return markup;
            }, // let our custom formatter work
            minimumInputLength: 1,
            templateResult: formatRepo, // omitted for brevity, see the source of this page
            templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
        });
    });
    </script>
    
    
<script> 
    function getitems(){
     var itemname = document.getElementById("itemname").value;
     loadPage('rpt_sales_by_itemname.htm?itemname='+encodeURIComponent(itemname), 'container');
    }
    </script>



	<br>
	
	<nav class="breadcrumb">
                                    <a class="breadcrumb-item" href="index.htm">Home</a>
                                    <a class="breadcrumb-item" href="#" onclick="loadPage('reports.htm', 'container');">Reports</a>
                                    <a class="breadcrumb-item" href="#" onclick="loadPage('sales_reports.htm', 'container');">Sales Reports</a>
                                    <span class="breadcrumb-item active">Sales by Itemname Report</span>
      </nav>
  <br>


	<div class="row">
		<div class="col-12">




			<div class="card">
				<div class="card-body">
				
				<div class="card card-outline-info">
				<div class="card-header"><center><h4 class="m-b-0 text-white">Sales by Item Name Report</h4></center></div>
				</div>
					


					<div class="alert alert-warning">
						<div class="row">
							
									<div class="col-lg-4 col-md-4">Select Item :</div>
									<div class="col-lg-4 col-md-4">

										<select class="itemselect3" style="width: 100%" id="itemname" name="itemname">
											<option>Select</option>
											<c:forEach items="${myModel.saleslist1}" var="sales"
												varStatus="status">

												<option value="${sales.itemname}">${sales.itemname}</option>

											</c:forEach>

										<select>

									</div>
									<div class="col-lg-4 col-md-4">
										<button type="button" onclick="getitems();" class="btn btn-info">Check</button>

									</div>
								
						</div>
					</div>


					<div class="table-responsive">
						<table id="tbl_rpt_sales_itemwise"
							class="display nowrap table table-hover table-striped table-bordered"
							cellspacing="0" width="100%">
							<thead>
								<tr>
									<th>Purchase Date</th>
									<th>Purchase Time</th>
									<th>Invoice No</th>
									<th>Item ID</th>
									<th>Item Name</th>
									<th>Sales Price</th>
									<th>Qty</th>
									<th>Disc</th>
									<th>Total Sales Price</th>
								</tr>
							</thead>

							<tbody>
								<c:forEach items="${myModel.saleslist2}" var="sales"
									varStatus="status">

									<tr>
										<td><c:out value="${sales.purchasedate}" /></td>
										<td><c:out value="${sales.purchasetime}" /></td>
										<td><c:out value="${sales.invoicenumber}" /></td>
										<td><c:out value="${sales.itemid}" /></td>
										<td><c:out value="${sales.itemname}" /></td>
										<td><c:out value="${sales.salesprice}" /></td>
										<td><c:out value="${sales.quantity}" /></td>
										<td><c:out value="${sales.discount}" /></td>
										<td><c:out value="${sales.totalsalesprice}" /></td>

									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>


				</div>
			</div>
		</div>
	</div>

