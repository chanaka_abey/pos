<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<script>
    $(document).ready(function() {
        $('#myTable').DataTable();
        $(document).ready(function() {
            var table = $('#example').DataTable({
                "columnDefs": [{
                    "visible": false,
                    "targets": 2
                }],
                "order": [
                    [2, 'asc']
                ],
                "displayLength": 25,
                "drawCallback": function(settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;
                    api.column(2, {
                        page: 'current'
                    }).data().each(function(group, i) {
                        if (last !== group) {
                            $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                            last = group;
                        }
                    });
                }
            });
            // Order by the grouping
            $('#example tbody').on('click', 'tr.group', function() {
                var currentOrder = table.order()[0];
                if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                    table.order([2, 'desc']).draw();
                } else {
                    table.order([2, 'asc']).draw();
                }
            });
        });
    });
    
    $('#tbl_rpt_stock_below').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
    </script>


	<br>
	
	<nav class="breadcrumb" style="background-color:#c0d8f1;">
                                    <a class="breadcrumb-item" href="index.htm">Home</a>
                                    <a class="breadcrumb-item" href="#" onclick="loadPage('reports.htm', 'container');">Reports</a>
                                    <a class="breadcrumb-item" href="#" onclick="loadPage('stock_reports.htm', 'container');">Stock Reports</a>
                                    <span class="breadcrumb-item active">Below Reorder Level Report</span>
      </nav>
  <br>
	
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-body">
				
				<div class="card card-outline-info">
				<div class="card-header"><center><h4 class="m-b-0 text-white">Below Reorder level Report</h4></center></div>
				</div>


					<div class="table-responsive">
						<table id="tbl_rpt_stock_below"
							class="display nowrap table table-hover table-striped table-bordered"
							cellspacing="0" width="100%">
							<thead>
								<tr>
									<th>Item ID</th>
									<th>Item Name</th>
									<th>Category</th>
									<th>Unit Price</th>
									<th>Sales Price</th>
									<th>Qty</th>
									<th>Reorder Level</th>
								</tr>
							</thead>

							<tbody>
								<c:forEach items="${itemlist}" var="items" varStatus="status">
									<c:set value="${items.quantity}" var="qty"/>
								<c:set value="${items.reorderlevel}" var="reorder"/>
								<c:choose>
									<c:when test="${qty <= reorder}">
									<tr>
										<td><c:out value="${items.itemid}" /></td>
										<td><c:out value="${items.itemname}" /></td>
										<td><c:out value="${items.category}" /></td>
										<td><c:out value="${items.unitprice}" /></td>
										<td><c:out value="${items.salesprice}" /></td>
										<td><c:out value="${items.quantity}" /></td>
										<td><c:out value="${items.reorderlevel}" /></td>

									</tr>
									</c:when>
									</c:choose>
								</c:forEach>
							</tbody>
						</table>
					</div>


				</div>
			</div>
		</div>
	</div>

