<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<script>
    $(document).ready(function() {
        $('#myTable').DataTable();
        $(document).ready(function() {
            var table = $('#example').DataTable({
                "columnDefs": [{
                    "visible": false,
                    "targets": 2
                }],
                "order": [
                    [2, 'asc']
                ],
                "displayLength": 25,
                "drawCallback": function(settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;
                    api.column(2, {
                        page: 'current'
                    }).data().each(function(group, i) {
                        if (last !== group) {
                            $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                            last = group;
                        }
                    });
                }
            });
            // Order by the grouping
            $('#example tbody').on('click', 'tr.group', function() {
                var currentOrder = table.order()[0];
                if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                    table.order([2, 'desc']).draw();
                } else {
                    table.order([2, 'asc']).draw();
                }
            });
        });
    });
    
    $('#tbl_rpt_stock_by_date').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
    </script>
    <script>   
    $('#mdate').bootstrapMaterialDatePicker({
        weekStart: 0,
        time: false
    });
    </script>
    <script> 
    function getDetails(){
     var mdate = document.getElementById("mdate").value;
     loadPage('rpt_stock_update_by_date.htm?mdate='+mdate, 'container');
    }
    </script>


	<br>

<nav class="breadcrumb" style="background-color:#c0d8f1;">
                                    <a class="breadcrumb-item" href="index.htm">Home</a>
                                    <a class="breadcrumb-item" href="#" onclick="loadPage('reports.htm', 'container');">Reports</a>
                                    <a class="breadcrumb-item" href="#" onclick="loadPage('stock_reports.htm', 'container');">Stock Reports</a>
                                    <span class="breadcrumb-item active">Stock Update by Date Report</span>
      </nav>
  <br>

	<div class="row">
		<div class="col-12">

			


			<div class="card">
				<div class="card-body">
				
				<div class="card card-outline-info">
				<div class="card-header"><center><h4 class="m-b-0 text-white">Stock Updates by Date Report</h4></center></div>
				</div>
					
					
					
					<div class="alert alert-warning">
				<div class="row">
					
							<div class="col-lg-4 col-md-4">Select Date :</div>
							<div class="col-lg-4 col-md-4">

								<input type="text" class="form-control"
									placeholder="select date" id="mdate" />

							</div>
							<div class="col-lg-4 col-md-4">
								<button type="button" onclick="getDetails();" class="btn btn-info">Check</button>

							</div>
						
				</div>
			</div>
					

					<div class="table-responsive">
						<table id="tbl_rpt_stock_by_date"
							class="display nowrap table table-hover table-striped table-bordered"
							cellspacing="0" width="100%">
							<thead>
								<tr>
									<th>Update date</th>
									<th>Update Time</th>
									<th>Item ID</th>
									<th>Item Name</th>
									<th>Category</th>
									<th>Unit Price</th>
									<th>Sales Price</th>
									<th>Qty</th>
									<th>Reorder Level</th>
								</tr>
							</thead>

							<tbody>
								<c:forEach items="${stockupdatelist}" var="stock"
									varStatus="status">

									<tr>
										<td><c:out value="${stock.update_date}" /></td>
										<td><c:out value="${stock.update_time}" /></td>
										<td><c:out value="${stock.itemid}" /></td>
										<td><c:out value="${stock.itemname}" /></td>
										<td><c:out value="${stock.category}" /></td>
										<td><c:out value="${stock.unitprice}" /></td>
										<td><c:out value="${stock.salesprice}" /></td>
										<td><c:out value="${stock.quantity}" /></td>
										<td><c:out value="${stock.reorderlevel}" /></td>

									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>


				</div>
			</div>
		</div>
	</div>

