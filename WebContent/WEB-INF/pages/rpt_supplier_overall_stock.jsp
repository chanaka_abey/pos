<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<script>
    $(document).ready(function() {
        $('#myTable').DataTable();
        $(document).ready(function() {
            var table = $('#example').DataTable({
                "columnDefs": [{
                    "visible": false,
                    "targets": 2
                }],
                "order": [
                    [2, 'asc']
                ],
                "displayLength": 25,
                "drawCallback": function(settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;
                    api.column(2, {
                        page: 'current'
                    }).data().each(function(group, i) {
                        if (last !== group) {
                            $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                            last = group;
                        }
                    });
                }
            });
            // Order by the grouping
            $('#example tbody').on('click', 'tr.group', function() {
                var currentOrder = table.order()[0];
                if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                    table.order([2, 'desc']).draw();
                } else {
                    table.order([2, 'asc']).draw();
                }
            });
        });
    });
    
    $('#tbl_item').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
    </script>
    
   


	<br>
	
	<nav class="breadcrumb" style="background-color:#c0d8f1;">
                                    <a class="breadcrumb-item" href="index.htm">Home</a>
                                     <a class="breadcrumb-item" href="#" onclick="loadPage('reports.htm', 'container');">Reports</a>
                                    <a class="breadcrumb-item" href="#" onclick="loadPage('supplier_reports.htm', 'container');">Supplier Reports</a>
                                    <span class="breadcrumb-item active">Supplier Stock Report</span>
      </nav>
  <br>


	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-body">
				
				<div class="card card-outline-info">
				<div class="card-header"><center><h4 class="m-b-0 text-white">Supplier Stocks</h4></center></div>
				</div>

					<div class="table-responsive">
						<table id="tbl_item"
							class="display nowrap table table-hover table-striped table-bordered"
							cellspacing="0" width="100%">
							<thead>
								<tr>
								<!--  <th>ID</th> -->
									<th>Date</th>
									<th>Supplier</th>
									<th>Itemname</th>
									<th>Quantity</th>
									<!-- <th>Item_id</th> -->
								</tr>
							</thead>

							<tbody>
								<c:forEach items="${supplierStocks}" var="supplier" varStatus="status">
              						 
              						  <tr>
              						  
										<td><c:out value="${supplier.date}" /></td>
										<td><c:out value="${supplier.supplier_name}" /></td>
										<td><c:out value="${supplier.itemname}" /></td>
										<td><c:out value="${supplier.quantity}" /></td>
										<%-- <td><c:out value="${supplier.item_id}" /></td> --%>
									</tr>
              						  
              						
								</c:forEach>
							</tbody>
						</table>
					</div>


				</div>
			</div>
		</div>
	</div>

