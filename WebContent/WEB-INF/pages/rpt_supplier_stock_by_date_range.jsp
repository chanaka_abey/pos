<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<script>
    $(document).ready(function() {
        $('#myTable').DataTable();
        $(document).ready(function() {
            var table = $('#example').DataTable({
                "columnDefs": [{
                    "visible": false,
                    "targets": 2
                }],
                "order": [
                    [2, 'asc']
                ],
                "displayLength": 25,
                "drawCallback": function(settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;
                    api.column(2, {
                        page: 'current'
                    }).data().each(function(group, i) {
                        if (last !== group) {
                            $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                            last = group;
                        }
                    });
                }
            });
            // Order by the grouping
            $('#example tbody').on('click', 'tr.group', function() {
                var currentOrder = table.order()[0];
                if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                    table.order([2, 'desc']).draw();
                } else {
                    table.order([2, 'asc']).draw();
                }
            });
        });
    });
    
    $('#tbl_rpt_activity_by_date_range').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
    </script>
    <script>   
    $('#mdate').bootstrapMaterialDatePicker({
        weekStart: 0,
        time: false
    });
    $('#mdate2').bootstrapMaterialDatePicker({
        weekStart: 0,
        time: false
    });
    </script>
    <script> 
    function getDetails(){
     var mdate = document.getElementById("mdate").value;
     var mdate2 = document.getElementById("mdate2").value;
     loadPage('rpt_supplier_stock_by_date_range.htm?mdate='+mdate+"&mdate2="+mdate2, 'container');
    }
    </script>




	<br>
	
	<nav class="breadcrumb" style="background-color:#c0d8f1;">
                                    <a class="breadcrumb-item" href="index.htm">Home</a>
                                     <a class="breadcrumb-item" href="#" onclick="loadPage('reports.htm', 'container');">Reports</a>
                                    <a class="breadcrumb-item" href="#" onclick="loadPage('supplier_reports.htm', 'container');">Supplier Reports</a>
                                    <span class="breadcrumb-item active">Supplier Stock by Date Range Report</span>
      </nav>
  <br>


	<div class="row">
		<div class="col-12">

			<div class="card">
				<div class="card-body">
					<h3>Supplier Stocks by Date Range Report</h3>


					<div class="alert alert-warning">
						<div class="row">
							
									<div class="col-lg-3 col-md-4">Select Date
										&nbsp;&nbsp;&nbsp; From:</div>
									<div class="col-lg-3 col-md-4">

										<input type="text"
											class="form-control" placeholder="select date" id="mdate" />

									</div>
									To :
									<div class="col-lg-3 col-md-4">

										<input  type="text"
											class="form-control" placeholder="select date" id="mdate2" />

									</div>
									<div class="col-lg-2 col-md-4">
										<button type="button" onclick="getDetails();" class="btn btn-info">Check</button>

									</div>
								
						</div>
					</div>


					<div class="table-responsive">
						<table id="tbl_rpt_activity_by_date_range"
							class="display nowrap table table-hover table-striped table-bordered"
							cellspacing="0" width="100%">
							<thead>
								<tr>
									<th>Date</th>
									<th>Supplier Name</th>
									<th>Item Name</th>
									<th>Quantity</th>
								</tr>
							</thead>

							<tbody>
								<c:forEach items="${supplierStocks}" var="supplier" varStatus="status">

									<tr>
										<td><c:out value="${supplier.date}" /></td>
										<td><c:out value="${supplier.supplier_name}" /></td>
										<td><c:out value="${supplier.itemname}" /></td>
										<td><c:out value="${supplier.quantity}" /></td>
									

									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>


				</div>
			</div>
		</div>
	</div>

