<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<script>
    $(document).ready(function() {
        $('#myTable').DataTable();
        $(document).ready(function() {
            var table = $('#example').DataTable({
                "columnDefs": [{
                    "visible": false,
                    "targets": 2
                }],
                "order": [
                    [2, 'asc']
                ],
                "displayLength": 25,
                "drawCallback": function(settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;
                    api.column(2, {
                        page: 'current'
                    }).data().each(function(group, i) {
                        if (last !== group) {
                            $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                            last = group;
                        }
                    });
                }
            });
            // Order by the grouping
            $('#example tbody').on('click', 'tr.group', function() {
                var currentOrder = table.order()[0];
                if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                    table.order([2, 'desc']).draw();
                } else {
                    table.order([2, 'asc']).draw();
                }
            });
        });
    });
    
    $('#tbl_rpt_stock_itemwise').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
    </script>
    
    
    <script>
    jQuery(document).ready(function() {
        // Switchery
       
        // For select 2
        $(".itemselect2").select2();
        $('.selectpicker').selectpicker();
        //Bootstrap-TouchSpin
        
        // For multiselect
       
        $(".ajax").select2({
            ajax: {
                url: "https://api.github.com/search/repositories",
                dataType: 'json',
                delay: 250,
                data: function(params) {
                    return {
                        q: params.term, // search term
                        page: params.page
                    };
                },
                processResults: function(data, params) {
                    // parse the results into the format expected by Select2
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data, except to indicate that infinite
                    // scrolling can be used
                    params.page = params.page || 1;
                    return {
                        results: data.items,
                        pagination: {
                            more: (params.page * 30) < data.total_count
                        }
                    };
                },
                cache: true
            },
            escapeMarkup: function(markup) {
                return markup;
            }, // let our custom formatter work
            minimumInputLength: 1,
            templateResult: formatRepo, // omitted for brevity, see the source of this page
            templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
        });
    });
    </script>
    
    
<script> 
    function getitemDetails(){
     var suppliername = document.getElementById("suppliername").value;
     loadPage('rpt_supplier_stock_by_supplier_name.htm?suppliername='+encodeURIComponent(suppliername), 'container');
    }
    </script>

	<br>
	
	<nav class="breadcrumb" style="background-color:#c0d8f1;">
                                    <a class="breadcrumb-item" href="index.htm">Home</a>
                                     <a class="breadcrumb-item" href="#" onclick="loadPage('reports.htm', 'container');">Reports</a>
                                    <a class="breadcrumb-item" href="#" onclick="loadPage('supplier_reports.htm', 'container');">Supplier Reports</a>
                                    <span class="breadcrumb-item active">Supplier Stock by Supplier Name Report</span>
      </nav>
  <br>


	<div class="row">
		<div class="col-12">

			


			<div class="card">
				<div class="card-body">
					<h3>Supplier Stocks by Supplier Name Report</h3>
					
					
					<div class="alert alert-warning">
				<div class="row">
					
						
							<div class="col-lg-4 col-md-4">Select Supplier :</div>
							<div class="col-lg-4 col-md-4">

								<select class="itemselect2" style="width: 100%" id="suppliername">
                                    <option>Select</option>
                                    <c:forEach items="${myModel.supplier_list}" var="supplierlist"
									varStatus="status">
                                    
                                        <option value="${supplierlist.name}">${supplierlist.name}</option> 
                                    
                                    </c:forEach>
                              
                                <select>

							</div>
							<div class="col-lg-4 col-md-4">
								<button type="button" onclick="getitemDetails();" class="btn btn-info">Check</button>

							</div>
						
				</div>
			</div>
					

					<div class="table-responsive">
						<table id="tbl_rpt_stock_itemwise"
							class="display nowrap table table-hover table-striped table-bordered"
							cellspacing="0" width="100%">
							<thead>
								<tr>
									<th>Date</th>
									<th>Supplier</th>
									<th>Item Name</th>
									<th>Quantity</th>
								</tr>
							</thead>

							<tbody>
								<c:forEach items="${myModel.supplierstocks_list}" var="supplierstocks"
									varStatus="status">

									<tr>
										<td><c:out value="${supplierstocks.date}" /></td>
										<td><c:out value="${supplierstocks.supplier_name}" /></td>
										<td><c:out value="${supplierstocks.itemname}" /></td>
										<td><c:out value="${supplierstocks.quantity}" /></td>

									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>


				</div>
			</div>
		</div>
	</div>

