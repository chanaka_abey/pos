<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<script>
    $(document).ready(function() {
        $('#myTable').DataTable();
        $(document).ready(function() {
            var table = $('#example').DataTable({
                "columnDefs": [{
                    "visible": false,
                    "targets": 2
                }],
                "order": [
                    [2, 'asc']
                ],
                "displayLength": 25,
                "drawCallback": function(settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;
                    api.column(2, {
                        page: 'current'
                    }).data().each(function(group, i) {
                        if (last !== group) {
                            $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                            last = group;
                        }
                    });
                }
            });
            // Order by the grouping
            $('#example tbody').on('click', 'tr.group', function() {
                var currentOrder = table.order()[0];
                if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                    table.order([2, 'desc']).draw();
                } else {
                    table.order([2, 'asc']).draw();
                }
            });
        });
    });
    
    $('#tbl_sales').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
    </script>
    
     <script>
    function deleteSales(rowIndex){
    	//alert(rowIndex);
    	//var Rowvalue = document.getElementById(rowIndex);
    	
    	var id = document.getElementById('id'+rowIndex).value;
    	var invoicenumber = document.getElementById('invoicenumber'+rowIndex).value;
    	var itemname = document.getElementById('itemname'+rowIndex).value;
    	//alert("id : " + id);
    	
    	
    	
    //Parameter
    $('#sa-params44'+rowIndex).click(function(){
        swal({   
            title: "Are you sure? Delete "+itemname +" in " +invoicenumber ,   
            text: "You will not be able to recover "+itemname +" in " +invoicenumber +" this store!",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#DD6B55",   
            confirmButtonText: "Yes, delete it!",   
            cancelButtonText: "No, cancel !",   
            closeOnConfirm: false,   
            closeOnCancel: false 
        }, function(isConfirm){   
            if (isConfirm) {     
                swal("Deleted!",itemname +" in " +invoicenumber+" has been deleted.", "success");   
                loadPage('deleteSales?id='+id, 'container');
                updateInvoiceForSales(rowIndex);
                deteleQuantityFromItems2(rowIndex);
                
               // alert("kkkk");
            } else {     
                swal("Cancelled", itemname +" in " +invoicenumber+ " is safe ", "error");   
            } 
        });
    });
    
    
   
    
    }
    
    </script>
    
    
    <script>
    function deteleQuantityFromItems2(rowIndex){
    	//alert("deteleQuantityFromItems");
    	var quantity = document.getElementById('quantityid'+rowIndex).value;
    	var itemidid = document.getElementById('itemid'+rowIndex).value;
    	
    	//alert("quantity " + quantity + " and Itemid : " + itemidid);
    	
    	  $.ajax({
    			method : "POST",
    			url : 'deletequantityfromitems2',
    			data : {
    				itemidid : itemidid,
    				quantity : quantity
    			},
    			dataType : "json",
    			cache : false,
    			success : function() {

    				//alert("success");
    			},
    			error : function() {
    				//alert("Error..! " );
    				//location.reload();
    			}
    		}); 
    	
    }
    </script>
    
    
     <script>
    function updateInvoiceForSales(rowIndex){
    	//alert("deteleQuantityFromItems");
    	var quantity = document.getElementById('quantityid'+rowIndex).value;
    	var itemidid = document.getElementById('itemid'+rowIndex).value;
    	var invoicenumber = document.getElementById('invoicenumber'+rowIndex).value;
    	
    	
    	var salesprice = document.getElementById('salesprice'+rowIndex).value;
    	var discount = document.getElementById('discount'+rowIndex).value;
    	var totalsalesprice = document.getElementById('totalsalesprice'+rowIndex).value;
    	
    	//alert("invoicenumber " + invoicenumber + " Itemid : " + itemidid +" salesprice : " + salesprice +" totalsalesprice : " + totalsalesprice);
    	
    	   $.ajax({
    			method : "POST",
    			url : 'updateinvoiceforsales',
    			data : {
    				invoicenumber : invoicenumber,
    				salesprice : salesprice,
    				totalsalesprice : totalsalesprice,
    				discount : discount,
    				itemidid : itemidid,
    				quantity : quantity
    			},
    			dataType : "json",
    			cache : false,
    			success : function() {

    				//alert("success");
    			},
    			error : function() {
    				//alert("Error..! " );
    				//location.reload();
    			}
    		});  
    	
    }
    </script>

	<br>
	
	<nav class="breadcrumb" style="background-color:#c0d8f1;">
                                    <a class="breadcrumb-item" href="index.htm">Home</a>
                                    <span class="breadcrumb-item active" href="#" >Sales</span>
                                    
      </nav>
  <br>
	
	<div class="row" id="indexstat">
		<!-- Column -->
		<div class="col-lg-6 col-md-8">
			<div class="card">
				<a href="#" onclick="loadPage('sales.htm', 'container');">
					<div class="card-body">
						<div class="d-flex flex-row">
							<div class="round le round-lg align-self-center round-info">
								<i class="mdi mdi-cash-multiple"></i>
							</div>
							<div class="m-l-30 align-self-center">

								<h5 class="text-muted m-b-0">Sales List</h5>
							</div>
						</div>
					</div>
				</a>
			</div>
		</div>
		<!-- Column -->
		<!-- Column -->
		
		<div class="col-lg-6 col-md-8">
			<div class="card">
			<a href="#" onclick="loadPage('invoice.htm', 'container');">
				<div class="card-body">
					<div class="d-flex flex-row">
						<div class="round  le round-lg align-self-center round-warning">
							<i class="mdi mdi-newspaper"></i>
						</div>
						<div class="m-l-30 align-self-center">

							<h5 class="text-muted m-b-0">Invoice List</h5>
						</div>
					</div>
				</div>
				</a>
			</div>
		</div> 
		<!-- Column -->
		<!-- Column -->
		<!-- chanaka impementation -->
	<!-- 	<div class="col-lg-3 col-md-6">
			<div class="card">
			<a href="#" onclick="loadPage('sales_reports.htm', 'container');">
				<div class="card-body">
					<div class="d-flex flex-row">
						<div class="round round-lg align-self-center round-primary">
							<i class="mdi mdi-chart-line"></i>
						</div>
						<div class="m-l-10 align-self-center">

							<h5 class="text-muted m-b-0">Sales Reports</h5>
						</div>
					</div>
				</div>
				</a>
			</div>
		</div> -->
		<!-- Column -->
		<!-- Column -->
		<!-- chanaka impementation -->
<!-- 		<div class="col-lg-6 col-md-8">
			<div class="card">
			<a href="#" onclick="loadPage('invoice_reports.htm', 'container');">
				<div class="card-body">
					<div class="d-flex flex-row">
						<div class="round round-lg align-self-center round-danger ">
							<i class="mdi mdi-chart-areaspline"></i>
						</div>
						<div class="m-l-30 align-self-center">

							<h5 class="text-muted m-b-0">Invoice Reports</h5>
						</div>
					</div>
				</div>
				</a>
			</div>
		</div> -->
		<!-- Column -->
	</div>



	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-body">
				<div class="card card-outline-info">
				<div class="card-header"><center><h4 class="m-b-0 text-white">Sales List</h4></center></div>
				</div>
	
					<div class="table-responsive">
						<table id="tbl_sales"
							class="display nowrap table table-hover table-striped table-bordered"
							cellspacing="0" width="100%">
							<thead>
								<tr>
									<th style="display: none;">ID</th>
									<th>Invoice No</th>
									<th>Item ID</th>
									<th>Item Name</th>
									<th>Purchase Date</th>
									<th>Purchase Time</th>
									<th>Sales Price</th>
									<th>Qty</th>
									<th>Disc</th>
									<th>Total Sale</th>
									<th style="display: none;">Item_ID</th>
									<th style="display: none;"><button type="button" class="btn btn-secondary"><i class="fa fa-edit"></i></button></th>
									<th><button type="button" class="btn btn-secondary"><i class="fa fa-trash-o"></i></button></th> 
								</tr>
							</thead>

							<tbody>
								<c:forEach items="${saleslist}" var="sales" varStatus="status">
								
              						  <tr>
              						  <td style="display: none;"><input type="hidden" id="id${status.index}" value="${sales.id}" /><c:out value="${sales.id}" /></td>
										<td><input type="hidden" id="invoicenumber${status.index}" value="${sales.invoicenumber}" /><c:out value="${sales.invoicenumber}" /></td>
										<td><c:out value="${sales.itemid}" /></td>
										<td><input type="hidden" id="itemname${status.index}" value="${sales.itemname}" /><c:out value="${sales.itemname}" /></td>
										<td><c:out value="${sales.purchasedate}" /></td>
										<td><c:out value="${sales.purchasetime}" /></td>
										<td><input type="hidden" id="salesprice${status.index}" value="${sales.salesprice}" /><c:out value="${sales.salesprice}" /></td>
										<td><input type="hidden" id="quantityid${status.index}" value="${sales.quantity}" /><c:out value="${sales.quantity}" /></td>
										<td><input type="hidden" id="discount${status.index}" value="${sales.discount}" /><c:out value="${sales.discount}" /></td>
										<td><input type="hidden" id="totalsalesprice${status.index}" value="${sales.totalsalesprice}" /><c:out value="${sales.totalsalesprice}" /></td>
										<td style="display: none;"><input type="hidden" id="itemid${status.index}" value="${sales.item_id}" /><c:out value="${sales.item_id}" /></td>
										<td style="display: none;"><button type="button" class="btn btn-success" onclick="loadPage('editSales?id=<c:out value='${sales.id}'/>', 'container');"><i class="fa fa-edit"></i></button></td>
										<td id="sa-params44${status.index}"><button type="button" class="btn btn-danger" onclick="deleteSales(${status.index});"><i class="fa fa-trash-o"></i></button></td>
									</tr>
              						  
								</c:forEach>
							</tbody>
						</table>
					</div>


				</div>
			</div>
		</div>
	</div>

