<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


	<br>
	<nav class="breadcrumb" style="background-color:#c0d8f1;">
                                    <a class="breadcrumb-item" href="index.htm">Home</a>
                                    <a class="breadcrumb-item" href="#" onclick="loadPage('reports.htm', 'container');">Reports</a>
                                    <span class="breadcrumb-item active">Sales Reports</span>
      </nav>
  <br>
	
	<div class="row" id="indexstat">
		<!-- Column -->
		<div class="col-lg-4 col-md-6">
			<div class="card">
				<a href="#" onclick="loadPage('rpt_sales_overall.htm', 'container');">
					<div class="card-body">
						<div class="d-flex flex-row">
							<div class="round round-lg align-self-center round-info">
								<i class="ti-wallet"></i>
							</div>
							<div class="m-l-10 align-self-center">

								<h5 class="text-muted m-b-0">Overall Sales Report</h5>
							</div>
						</div>
					</div>
				</a>
			</div>
		</div>
		<!-- Column -->
		<!-- Column -->
		<div class="col-lg-4 col-md-6">
			<div class="card">
			<a href="#" onclick="loadPage('rpt_sales_by_date.htm', 'container');">
				<div class="card-body">
					<div class="d-flex flex-row">
						<div class="round round-lg align-self-center round-warning">
							<i class="mdi mdi-cellphone-link"></i>
						</div>
						<div class="m-l-10 align-self-center">

							<h5 class="text-muted m-b-0">Sales by date Report</h5>
						</div>
					</div>
				</div>
				</a>
			</div>
		</div>
		<!-- Column -->
		<!-- Column -->
		<!-- <div class="col-lg-4 col-md-6">
			<div class="card">
			<a href="#" onclick="loadPage('rpt_sales_by_date_range.htm', 'container');">
				<div class="card-body">
					<div class="d-flex flex-row">
						<div class="round round-lg align-self-center round-primary">
							<i class="mdi mdi-cart-outline"></i>
						</div>
						<div class="m-l-10 align-self-center">

							<h5 class="text-muted m-b-0">Sales by date range Report</h5>
						</div>
					</div>
				</div>
				</a>
			</div>
		</div> -->
		<!-- Column -->
		
	</div>


	<div class="row" id="indexstat">
		<!-- Column -->
		<div class="col-lg-4 col-md-6">
			<div class="card">
				<a href="#" onclick="loadPage('rpt_sales_by_itemname.htm', 'container');">
					<div class="card-body">
						<div class="d-flex flex-row">
							<div class="round round-lg align-self-center round-info">
								<i class="ti-wallet"></i>
							</div>
							<div class="m-l-10 align-self-center">

								<h5 class="text-muted m-b-0">Item wise Sales Report</h5>
							</div>
						</div>
					</div>
				</a>
			</div>
		</div>
		<!-- Column -->
		<!-- Column -->
		<div class="col-lg-4 col-md-6">
			<div class="card">
				<a href="#" onclick="loadPage('rpt_sales_by_invoicenumber.htm', 'container');">
				<div class="card-body">
					<div class="d-flex flex-row">
						<div class="round round-lg align-self-center round-warning">
							<i class="mdi mdi-cellphone-link"></i>
						</div>
						<div class="m-l-10 align-self-center">

							<h5 class="text-muted m-b-0">Sales By Invoicenumber</h5>
						</div>
					</div>
				</div>
				</a>
			</div>
		</div>
		<!-- Column -->
		<!-- Column -->
		<!-- <div class="col-lg-4 col-md-6">
			<div class="card">
			<a href="">
				<div class="card-body">
					<div class="d-flex flex-row">
						<div class="round round-lg align-self-center round-primary">
							<i class="mdi mdi-cart-outline"></i>
						</div>
						<div class="m-l-10 align-self-center">

							<h5 class="text-muted m-b-0">Not mention yet</h5>
						</div>
					</div>
				</div>
				</a>
			</div>
		</div> -->
		<!-- Column -->
		
	</div>



