<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

	    <script>
    $(document).ready(function() {
        $('#myTable').DataTable();
        $(document).ready(function() {
            var table = $('#example').DataTable({
                "columnDefs": [{
                    "visible": false,
                    "targets": 2
                }],
                "order": [
                    [2, 'asc']
                ],
                "displayLength": 25,
                "drawCallback": function(settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;
                    api.column(2, {
                        page: 'current'
                    }).data().each(function(group, i) {
                        if (last !== group) {
                            $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                            last = group;
                        }
                    });
                }
            });
            // Order by the grouping
            $('#example tbody').on('click', 'tr.group', function() {
                var currentOrder = table.order()[0];
                if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                    table.order([2, 'desc']).draw();
                } else {
                    table.order([2, 'asc']).draw();
                }
            });
        });
    });
    
    $('#tbl_user').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
    </script>
    
    
     <script>
    function getdeleteuser(rowIndex){
    	//alert(rowIndex);
    	//var Rowvalue = document.getElementById(rowIndex);
    	var username = document.getElementById('username'+rowIndex).value;
    	//alert("itemname : " + itemname);
    	
    	
    	
    //Parameter
    $('#sa-params44'+rowIndex).click(function(){
        swal({   
            title: "Are you sure? Delete "+username,   
            text: "You will not be able to recover "+username+" this store!",   
            type: "warning",   
            showCancelButton: true,   
            confirmButtonColor: "#DD6B55",   
            confirmButtonText: "Yes, delete it!",   
            cancelButtonText: "No, cancel !",   
            closeOnConfirm: false,   
            closeOnCancel: false 
        }, function(isConfirm){   
            if (isConfirm) {     
                swal("Deleted!", username +" has been deleted.", "success");   
                loadPage('deleteUser?username='+username, 'container');
            } else {     
                swal("Cancelled", username+ " is safe ", "error");   
            } 
        });
    });
    
    
   
    
    }
    
    </script>
	
	
	<br>
	
	<nav class="breadcrumb" style="background-color:#c0d8f1;">
                                    <a class="breadcrumb-item" href="index.htm">Home</a>
                                    <span class="breadcrumb-item active">Users</span>
      </nav>
  <br>
	
	<div class="row">
		<!-- Column -->
		<div class="col-lg-6 col-md-8">
			<div class="card">
				<a href="#" onclick="loadPage('add_new_user.htm', 'container');">
					<div class="card-body">
						<div class="d-flex flex-row">
							<div class="round le round-lg align-self-center round-info">
								<i class="mdi mdi-account-plus"></i>
							</div>
							<div class="m-l-30 align-self-center">

								<h5 class="text-muted m-b-0">Add New User</h5>
							</div>
						</div>
					</div>
				</a>
			</div>
		</div>
		<!-- Column -->
		<!-- Column -->
		<div class="col-lg-6 col-md-8">
			<div class="card">
			<a href="#" onclick="loadPage('user.htm', 'container');">
				<div class="card-body">
					<div class="d-flex flex-row">
						<div class="round le round-lg align-self-center round-warning">
							<i class="mdi mdi-account-multiple"></i>
						</div>
						<div class="m-l-30 align-self-center">

							<h5 class="text-muted m-b-0">User List</h5>
						</div>
					</div>
				</div>
				</a>
			</div>
		</div>
		<!-- Column -->
		<!-- Column -->
		<!-- chanaka implementation -->
	<!-- 	<div class="col-lg-4 col-md-6">
			<div class="card">
			<a href="#" onclick="loadPage('rpt_activity_by_user.htm', 'container');">
				<div class="card-body">
					<div class="d-flex flex-row">
						<div class="round round-lg align-self-center round-primary">
							<i class="mdi mdi-account-card-details"></i>
						</div>
						<div class="m-l-10 align-self-center">

							<h5 class="text-muted m-b-0">User Reports</h5>
						</div>
					</div>
				</div>
				</a>
			</div>
		</div> -->
		<!-- Column -->
		<!-- Column -->
		<!-- <div class="col-lg-3 col-md-6">
			<div class="card">
				<div class="card-body" href="#">
					<div class="d-flex flex-row">
						<div class="round round-lg align-self-center round-danger">
							<i class="mdi mdi-cash-multiple"></i>
						</div>
						<div class="m-l-10 align-self-center">

							<h5 class="text-muted m-b-0">Salary</h5>
						</div>
					</div>
				</div>
			</div>
		</div> -->
		<!-- Column -->
	</div>



	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-body">
					<div class="card card-outline-info">
				<div class="card-header"><center><h4 class="m-b-0 text-white">User List</h4></center></div>
				</div>
		
					<div class="table-responsive">
						<table id="tbl_user"
							class="display nowrap table table-hover table-striped table-bordered"
							cellspacing="0" width="100%">
							<thead>
								<tr>
									<th>User Name</th>
									<th>Password</th>
									<th>Role</th>
									<th><button type="button" class="btn btn-secondary"><i class="fa fa-edit"></i></button></th>
									<th><button type="button" class="btn btn-secondary"><i class="fa fa-trash-o"></i></button></th>
								</tr>
							</thead>

							<tbody>
								<c:forEach items="${userlist}" var="user" varStatus="status">
								
              						  <tr>
										<td><input type="hidden" id="username${status.index}" value="${user.username}" /><c:out value="${user.username}" /></td>
										<td><c:out value="${user.password}" /></td>
										<td><c:out value="${user.role}" /></td>
										<td><button type="button" class="btn btn-success" onclick="loadPage('editUser?username=<c:out value='${user.username}'/>', 'container');"><i class="fa fa-edit"></i></button></td>
										<td id="sa-params44${status.index}"><button type="button" class="btn btn-danger" onclick="getdeleteuser(${status.index});"><i class="fa fa-trash-o"></i></button></td>
									</tr>
              						  
								</c:forEach>
							</tbody>
						</table>
					</div>


				</div>
			</div>
		</div>
	</div>

