package com.saminda.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.saminda.entity.Activity;
import com.saminda.entity.Item;
import com.saminda.entity.Stock;
import com.saminda.entity.User;
import com.saminda.service.ActivityService;
import com.saminda.service.UserService;

@Controller
public class ActivityController {
	
	private static final Logger logger = Logger.getLogger(ActivityController.class);
	
	public ActivityController(){
		System.out.println("ActivityController()");
	}
	
	@Autowired
	private ActivityService activityService;
	
	@Autowired
	private UserService userService;
	
	@RequestMapping(value = {"activity_reports.htm"})
	public ModelAndView getActivityReports(){
		
		return new ModelAndView("activity_reports");
	}
	
	@RequestMapping(value = {"rpt_activity_overall.htm"})
	public ModelAndView getOverallActivityReport(){
		
		List<Activity> activitylist = activityService.getAllActivies();
		
		return new ModelAndView("rpt_activity_overall","activitylist",activitylist);
		
	}
	
	
	@RequestMapping(value = {"rpt_activity_by_date.htm"})
	public ModelAndView getActivityByDate(HttpServletRequest hsr){
		String update_date = hsr.getParameter("mdate");
		//System.out.println("@ModelAttribute Stock stock in rpt_stock_update_by_date.htm : "+ stock.getUpdate_date());
		System.out.println("mdate update_date update_date update_date ---> " + update_date);
		
		//List<Stock> stockupdatelist2 = stockService.getStockByDate(stock.getUpdate_date());
		List<Activity> activitylist = activityService.getAllActiviesByDate(update_date);

		return new ModelAndView("rpt_activity_by_date","activitylist",activitylist);
	}
	
	@RequestMapping(value = {"rpt_activity_by_date_range.htm"})
	public ModelAndView getActivityByDateRange(HttpServletRequest hsr){
		String update_date = hsr.getParameter("mdate");
		String update_date2 = hsr.getParameter("mdate2");
		//System.out.println("rpt_stock_update_by_date_range update_date : "+ stock.getUpdate_date());
		//System.out.println("rpt_stock_update_by_date_range update_time: "+ stock.getUpdate_time());
		System.out.println("mdate update_date update_date update_date 1  ---> " + update_date);
		System.out.println("mdate update_date update_date update_date 2---> " + update_date2);
		
		//List<Stock> stockupdatelist = stockService.getStockByDateRange(stock.getUpdate_date(),stock.getUpdate_time());
		List<Activity> activitylist = activityService.getAllActiviesByDateRange(update_date, update_date2);
		return new ModelAndView("rpt_activity_by_date_range","activitylist",activitylist);

	}
	
	@RequestMapping(value = {"rpt_activity_by_user.htm"})
	public ModelAndView getActivityByUser(HttpServletRequest hsr,@ModelAttribute User user){
		System.out.println("@ModelAttribute rpt_activity_by_user : "+ user.getUsername());
		String username = hsr.getParameter("username");
		System.out.println("Itemname : -----> " + username);
		Map<String, Object> myModel = new HashMap<String, Object>();
		List<User> userlist  = userService.getAllUsers();
		
		//List<Stock> stockupdatelist = stockService.getStockByItemName(stock.getItemname());
		List<Activity> activitybyuserlist = activityService.getAllActiviesByUser(username);
		myModel.put("userlist", userlist);
		myModel.put("activitybyuserlist", activitybyuserlist);
		return new ModelAndView("rpt_activity_by_user","myModel",myModel);

	}
	
	@RequestMapping(value = {"rpt_activity_by_action.htm"})
	public ModelAndView getActivityByAction(HttpServletRequest hsr,@ModelAttribute Activity activity){
		System.out.println("@ModelAttribute Action : "+ activity.getAction());
		String activityname = hsr.getParameter("action");
		System.out.println("activityname : -----> " + activityname);
		Map<String, Object> myModel = new HashMap<String, Object>();
		List<Activity> actionlist  = activityService.getAllActions();
		
		System.out.println("actionlist actionlist : -----> " + actionlist);
		//List<Stock> stockupdatelist = stockService.getStockByItemName(stock.getItemname());
		List<Activity> activitybyactionlist = activityService.getAllActiviesByAction(activityname);
		myModel.put("actionlist", actionlist);
		myModel.put("activitybyactionlist", activitybyactionlist);
		return new ModelAndView("rpt_activity_by_action","myModel",myModel);

	}

}
