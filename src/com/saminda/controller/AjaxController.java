package com.saminda.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.jboss.logging.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.saminda.entity.Invoice;
import com.saminda.entity.Sales;
import com.saminda.service.InvoiceService;
import com.saminda.service.SalesService;

@RestController
@RequestMapping("/service")
public class AjaxController {

	private static final Logger logger = Logger.getLogger(AjaxController.class);

	public AjaxController() {
		System.out.println("AjaxController()");
	}

	@Autowired
	private SalesService salesService;

	@Autowired
	private InvoiceService invoiceService;

	@RequestMapping(value = "/load_sales_by_invoice", method = RequestMethod.GET, produces = { MediaType.APPLICATION_JSON_VALUE })
	public synchronized @ResponseBody JSONObject getallinvoicess(
			HttpServletRequest hsr) {

		String invoicenumber = hsr.getParameter("invoicenumber");

		ArrayList<Invoice> invoicelist = new ArrayList<Invoice>();
		invoicelist = (ArrayList<Invoice>) invoiceService.getAllInvoice();
		// List<Sales> saleslist2 = salesService
		// .getAllSalesByInvoicenumber(invoicenumber);

		JSONObject returnList = new JSONObject();

		returnList.put("status", HttpStatus.OK);
		returnList.put("data", invoicelist);
		returnList.put("message", "Sucessfull");

		return returnList;
	}
}
