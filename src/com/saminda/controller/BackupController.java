package com.saminda.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.jboss.logging.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.saminda.entity.Employee;

@Controller
public class BackupController {
	
	private int STREAM_BUFFER = 512000;
    private boolean status = false;
	private static final Logger logger = Logger.getLogger(BackupController.class);
	
	public BackupController(){
		System.out.println("BackupController()");
	}
	
	/**
     * This method is used for backup the mysql database
     *
     * @param host - hostname - localhost/127.0.0.1
     * @param port - 3306
     * @param user - root
     * @param password - root
     * @param db - database name
     * @param backupfile - file path to backup from the current location ex/
     * "/backup/"
     * @param mysqlDumpExePath - file path to mysqldump.exe from the current
     * location ex/ copy the mysqldump.exe from mysql bin in to backup folder
     * and set the path as backup ex/ "/backup/mysqldump.exe"
     * @return the status of the backup true/false
     */
	
	
	
	
	@RequestMapping(value = {"backup.htm"})
	public ModelAndView loadpage(){
		
		String backupPath =System.getProperty("user.dir");
		return new ModelAndView("backup","backupPath",backupPath);
	}
	
	
//	 public boolean backupDatabase(String host, String port, String user, String password, String db, String backupfile, String mysqlDumpExePath) {
//	        try {
//	            // Get MySQL DUMP data
//	            String dump = getServerDumpData(host, port, user, password, db, mysqlDumpExePath);
//	            //check the backup dump
//	            if (status) {
//	                byte[] data = dump.getBytes();
//	                // Set backup folder
//	                //String rootpath = System.getProperty("user.dir") + backupfile;
//	                String rootpath = "F:\\opt" + backupfile;
//
//	                // See if backup folder exists
//	                File file = new File(rootpath);
//	                if (!file.isDirectory()) {
//	                    // Create backup folder when missing. Write access is needed.
//	                    file.mkdir();
//	                }
//	                // Compose full path, create a filename as you wish
//	                DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
//	                Date date = new Date();
//	                String filepath = rootpath + "backup(With_DB)-" + db + "-" + host + "-(" + dateFormat.format(date) + ").sql";
//	                // Write SQL DUMP file
//	                File filedst = new File(filepath);
//	                FileOutputStream dest = new FileOutputStream(filedst);
//	                dest.write(data);
//	                dest.close();
//	                logger.info("Backup created successfully for - " + db + " " + host);
//	            } else {
//	                //when status false  
//	            	logger.error("Could not create the backup for - " + db + " " + host);
//	            }
//
//	        } catch (Exception ex) {
//	        	logger.error(ex, ex.getCause());
//	        }
//
//	        return status;
//	    }

//	    private String getServerDumpData(String host, String port, String user, String password, String db, String mysqlDumpExePath) {
//	        StringBuilder dumpdata = new StringBuilder();
//	        String execlient = "";
//	        try {
//	            if (host != null && user != null && password != null && db != null) {
//	                // Set path. Set location of mysqldump 
//	                //  For example: current user folder and lib subfolder          
//
//	                execlient = System.getProperty("user.dir") + mysqlDumpExePath;
//
//	                // Usage: mysqldump [OPTIONS] database [tables]
//	                // OR     mysqldump [OPTIONS] --databases [OPTIONS] DB1 [DB2 DB3...]
//	                // OR     mysqldump [OPTIONS] --all-databases [OPTIONS]
//	                String command[] = new String[]{execlient,
//	                    "--host=" + host,
//	                    "--port=" + port,
//	                    "--user=" + user,
//	                    "--password=" + password,
//	                    "--skip-comments",
//	                    "--databases",
//	                    db};
//
//	                // Run mysqldump
//	                ProcessBuilder pb = new ProcessBuilder(command);
//	                Process process = pb.start();
//
//	                InputStream in = process.getInputStream();
//	                BufferedReader br = new BufferedReader(new InputStreamReader(in));
//
//	                int count;
//	                char[] cbuf = new char[STREAM_BUFFER];
//
//	                // Read datastream
//	                while ((count = br.read(cbuf, 0, STREAM_BUFFER)) != -1) {
//	                    dumpdata.append(cbuf, 0, count);
//	                }
//
//	                //set the status
//	                int processComplete = process.waitFor();
//	                if (processComplete == 0) {                   
//	                    status = true;
//	                } else {
//	                    status = false;
//	                }
//	                // Close
//	                br.close();
//	                in.close();
//	            }
//
//	        } catch (Exception ex) {
//	        	logger.error(ex, ex.getCause());
//	            return "";
//	        }
//	        return dumpdata.toString();
//	    }

	    
/*	    public static void main(String[] args) throws SQLException {
	        MySqlBackup b = new MySqlBackup();
	        // b.backupDatabase("localhost", "3306", "root", "root", "usersdb", "opt", "C:\\Program Files\\MySQL\\MySQL Server 5.6\\bin\\mysqldump.exe");
	        //b.backupDatabase("localhost", "3306", "root", "", "payroll", "/backup/", "/backup/mysqldump.exe");
	        //  b.restoreDatabase("root", "", "backup/backup-payroll-10.100.3.108-27-09-2012.sql");
	       //  b.test();
	          b.backupDataWithDatabase("C:\\Program Files\\MySQL\\MySQL Server 5.6\\bin\\mysqldump.exe", "localhost", "3306", "root", "root", "usersdb", "C:/Users/Saminda Alahakoon/Desktop/test/");
	        // b.backupAllDatabases("C:\\wamp\\bin\\mysql\\mysql5.5.16\\bin\\mysqldump.exe", "localhost", "3306", "root", "", "C:/Users/dinuka/Desktop/test/");
	    }*/

//	    public boolean backupDataWithOutDatabase(String dumpExePath, String host, String port, String user, String password, String database, String backupPath) {
//	        boolean status = false;
//	        try {
//	            Process p = null;
//
//	            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
//	            Date date = new Date();
//	            //String filepath = "backup(without_DB)-" + database + "-" + host + "-(" + dateFormat.format(date) + ").sql";
//	            String filepath = "POS_"+ dateFormat.format(date) + ".sql";
//	            String batchCommand = "";
//	            if (password != "") {
//	                //only backup the data not included create database
//	                batchCommand = dumpExePath + " -h " + host + " --port " + port + " -u " + user + " --password=" + password + " " + database + " -r \"" + backupPath + "" + filepath + "\"";
//	            } else {
//	                batchCommand = dumpExePath + " -h " + host + " --port " + port + " -u " + user + " " + database + " -r \"" + backupPath + "" + filepath + "\"";
//	            }
//
//	            Runtime runtime = Runtime.getRuntime();
//	            p = runtime.exec(batchCommand);
//	            int processComplete = p.waitFor();
//
//	            if (processComplete == 0) {
//	                status = true;
//	                logger.info("Backup created successfully for without DB " + database + " in " + host + ":" + port);
//	            } else {
//	                status = false;
//	                logger.info("Could not create the backup for without DB " + database + " in " + host + ":" + port);
//	            }
//
//	        } catch (IOException ioe) {
//	        	logger.error(ioe, ioe.getCause());
//	        } catch (Exception e) {
//	        	logger.error(e, e.getCause());
//	        }
//	        return status;
//	    }

	    @RequestMapping(value = {"getselectdata"})
	    public ModelAndView loadbackuppage(String dumpExePath, String host, String port, String user, String password, String database, String backupPath) {
	    	
	    	System.out.println("Backup Started...."+System.getProperty("user.dir"));
	    	
	    	dumpExePath = "C:\\mysql-8.0.18-winx64\\bin\\mysqldump.exe";
	        host = "localhost";
	        port = "3306";
	        user = "root";
	        password = "12345";
	        database = "possystem";
	        backupPath = System.getProperty("user.dir");
	        
	        String backpath = backupPath;
	        System.out.println("Databse backup path -- > " + backpath);
	        
	        try {
	            Process p = null;
	            Date date = new Date();
	            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
	            SimpleDateFormat ft2 =new SimpleDateFormat ("hh-mm-ss");
	            System.out.println("ft2.format(date) " + ft2.format(date));
	            
	            //String filepath = "backup(with_DB)-" + database + "-" + host + "-(" + dateFormat.format(date) + ").sql";
	            String filepath = "POS_SYSTEM-" + database + "-" + host + "-(" + dateFormat.format(date) + ").sql";
	            
	            String batchCommand = "";
	            if (password != "") {
	                //Backup with database
	            	batchCommand = "mysqldump -u" + user + " -p" + password +" "+ database +" > " + backupPath + "\\" + filepath;
	            	/*batchCommand = dumpExePath + " -h " + host + " --port " + port + " -u " + user + " -p " + password + " --add-drop-database -B " + database + " -r \"" + backupPath + "" + filepath + "\"";*/
	            	System.out.println("backup:command "+batchCommand);
	            } else {
	            	batchCommand = dumpExePath + " -h " + host + " --port " + port + " -u " + user + " --add-drop-database -B " + database + " -r \"" + backupPath + "" + filepath + "\"";
	            }

	     /*       Runtime runtime = Runtime.getRuntime();
	            p = runtime.exec(batchCommand);
	            int processComplete = p.exitValue();*/

	           Process runtimeProcess = Runtime.getRuntime().exec(new String[] { "cmd.exe", "/c", batchCommand });
	           int processComplete = runtimeProcess.exitValue();
	            System.out.println("processComplete"+processComplete);
	            if (processComplete == 0) {
	                status = true;
	                logger.info("Backup created successfully for with DB " + database + " in " + host + ":" + port);
	            } else {
	                status = false;
	                logger.info("Could not create the backup for with DB " + database + " in " + host + ":" + port);
	            }

	        } catch (IOException ioe) {
	        	logger.error(ioe, ioe.getCause());
	        } catch (Exception e) {
	        	logger.error(e, e.getCause());
	        }
	    	
	        //return new ModelAndView("backup","backupPath",backupPath);
	        return new ModelAndView("redirect:login.htm","backupPath",backupPath);
	       // "redirect:login.htm","myModel",myModel
	    }
	    
	    
	    
//	    public boolean backupDataWithDatabase(String dumpExePath, String host, String port, String user, String password, String database, String backupPath) {
//	        boolean status = false;
//	        dumpExePath = "C:\\Program Files\\MySQL\\MySQL Server 5.6\\bin\\mysqldump.exe";
//	        host = "localhost";
//	        port = "3306";
//	        user = "root";
//	        password = "root";
//	        database = "usersdb";
//	        backupPath ="C:/Users/Saminda Alahakoon/Desktop/test/";
//	        
//	        try {
//	            Process p = null;
//
//	            Date date = new Date();
//	            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
//	            SimpleDateFormat ft2 =new SimpleDateFormat ("hh-mm-ss");
//	            System.out.println("ft2.format(date) " + ft2.format(date));
//	            
//	            //String filepath = "backup(with_DB)-" + database + "-" + host + "-(" + dateFormat.format(date) + ").sql";
//	            String filepath = "POS "+ dateFormat.format(date)+" " +ft2.format(date)+ ".sql";
//	            
//	            String batchCommand = "";
//	            if (password != "") {
//	                //Backup with database
//	                batchCommand = dumpExePath + " -h " + host + " --port " + port + " -u " + user + " --password=" + password + " --add-drop-database -B " + database + " -r \"" + backupPath + "" + filepath + "\"";
//	            } else {
//	                batchCommand = dumpExePath + " -h " + host + " --port " + port + " -u " + user + " --add-drop-database -B " + database + " -r \"" + backupPath + "" + filepath + "\"";
//	            }
//
//	            Runtime runtime = Runtime.getRuntime();
//	            p = runtime.exec(batchCommand);
//	            int processComplete = p.waitFor();
//
//
//	            if (processComplete == 0) {
//	                status = true;
//	                logger.info("Backup created successfully for with DB " + database + " in " + host + ":" + port);
//	            } else {
//	                status = false;
//	                logger.info("Could not create the backup for with DB " + database + " in " + host + ":" + port);
//	            }
//
//	        } catch (IOException ioe) {
//	        	logger.error(ioe, ioe.getCause());
//	        } catch (Exception e) {
//	        	logger.error(e, e.getCause());
//	        }
//	        return status;
//	    }

//	    public boolean backupAllDatabases(String dumpExePath, String host, String port, String user, String password, String backupPath) {
//	        boolean status = false;
//	        try {
//	            Process p = null;
//
//	            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
//	            Date date = new Date();
//	            String filepath = "backup(with_DB)-All-" + host + "-(" + dateFormat.format(date) + ").sql";
//
//	            String batchCommand = "";
//	            if (password != "") {
//	                //Backup with database
//	                batchCommand = dumpExePath + " -h " + host + " --port " + port + " -u " + user + " --password=" + password + " --add-drop-database -A  -r \"" + backupPath + "" + filepath + "\"";
//	            } else {
//	                batchCommand = dumpExePath + " -h " + host + " --port " + port + " -u " + user + " --add-drop-database -A  -r \"" + backupPath + "" + filepath + "\"";
//	            }
//
//	            Runtime runtime = Runtime.getRuntime();
//	            p = runtime.exec(batchCommand);
//	            int processComplete = p.waitFor();
//
//
//	            if (processComplete == 0) {
//	                status = true;
//	                logger.info("Backup created successfully with All DBs in " + host + ":" + port);
//	            } else {
//	                status = false;
//	                logger.info("Could not create the backup for All DBs in " + host + ":" + port);
//	            }
//
//	        } catch (IOException ioe) {
//	        	logger.error(ioe, ioe.getCause());
//	        } catch (Exception e) {
//	        	logger.error(e, e.getCause());
//	        }
//	        return status;
//	    }

	    /**
	     * Restore the backup into a local database
	     *
	     * @param dbUserName - user name
	     * @param dbPassword - password
	     * @param source - backup file
	     * @return the status true/false
	     */
//	    public boolean restoreDatabase(String dbUserName, String dbPassword, String source) {
//
//	        String[] executeCmd = new String[]{"mysql", "--user=" + dbUserName, "--password=" + dbPassword, "-e", "source " + source};
//
//	        Process runtimeProcess;
//	        try {
//	            runtimeProcess = Runtime.getRuntime().exec(executeCmd);
//	            int processComplete = runtimeProcess.waitFor();
//
//	            if (processComplete == 0) {
//	            	logger.info("Backup restored successfully with " + source);
//	                return true;
//	            } else {
//	            	logger.info("Could not restore the backup " + source);
//	            }
//	        } catch (Exception ex) {
//	        	logger.error(ex, ex.getCause());
//	        }
//
//	        return false;
//
//	    }
//	    
//	    
//	    @RequestMapping(value = {"selectfolderpath"})
//	    public ModelAndView getSelectedfolder(){
//	    	System.out.println("Select folder path..............");
//	    	
//	    	return new ModelAndView("backup");
//	    	
//
//	    }

}
