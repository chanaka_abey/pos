package com.saminda.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jboss.logging.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.saminda.entity.Invoice;
import com.saminda.service.InvoiceService;
import com.saminda.service.SalesService;

@Controller
public class ChartsController {

	private static final Logger logger = Logger.getLogger(ChartsController.class);
	
	public ChartsController() {
		System.out.println("ChartsController()");
	}
	
	@Autowired
	private SalesService salesService;
	
	@Autowired
	private InvoiceService invoiceService;
	

	@RequestMapping(value = {"rpt_charts_daily_profit.htm"})
	public ModelAndView getdailysalescharts(){
		System.out.println("daily profit ;;;;;;;;;;;;;;;;;;;");
		return new ModelAndView("rpt_charts_daily_profit");
	}
	
	@RequestMapping(value = {"loaddailyprofitdata"})
	public ModelAndView getdailyprofitchartsdata(HttpServletRequest hsr,HttpServletResponse res) throws IOException{
		
		System.out.println("loadchartdata loadchartdata loadchartdata;;;;;;;;;;;;;;;;;;;");
		
		List<Invoice> invoicelist = invoiceService.getOverallProfitByDate();
		
		System.out.println("rpt_profit_by_date : ---> " + invoicelist);
		
		
		 ArrayList<Object> pieDataSet = new ArrayList<Object>();
         JSONObject obj1  = new JSONObject();
         for(Object o : invoicelist) {
             Object[] obj = (Object[])o;
             System.out.println(" Date  ------- > : "+ obj[1].toString());
             System.out.println(" Profit  ------- > : "+ obj[2].toString());
             System.out.println(" Unitprice  ------- > : "+ obj[3].toString());
             System.out.println(" Sales  ------- > : "+ obj[0].toString());
             obj1.put("profit" , Double.parseDouble(obj[2].toString()));
             obj1.put("date" , obj[3].toString());
             pieDataSet.add(obj1);
             obj1 = new JSONObject();
         }
         PrintWriter writer = res.getWriter();
         writer.write(pieDataSet.toString());
         writer.close();

		return new ModelAndView("rpt_charts_daily_profit");

	}
	
	
	
	@RequestMapping(value = {"rpt_charts_date_range_sales.htm"})
	public ModelAndView getdaterangesalescharts(){
		System.out.println("date range sales ;;;;;;;;;;;;;;;;;;;");
		return new ModelAndView("rpt_charts_date_range_sales");
	}
	
	

	@RequestMapping(value = {"loaddaterangesalesdata"})
	public ModelAndView getchartsdata(HttpServletRequest hsr,HttpServletResponse res) throws IOException{
		
		System.out.println("loadchartdata loadchartdata loadchartdata;;;;;;;;;;;;;;;;;;;");
		String mdate = hsr.getParameter("mdate");
		String mdate2 = hsr.getParameter("mdate2");
		System.out.println("from date ------------ > "  + mdate);
		System.out.println("to date ------------ > "  + mdate2);
		
		List<Invoice> invoicelist = invoiceService.getAllProfitByDateRange(mdate, mdate2);
		
		System.out.println("rpt_profit_by_date : ---> " + invoicelist);
		
		
		 ArrayList<Object> pieDataSet = new ArrayList<Object>();
         JSONObject obj1  = new JSONObject();
         for(Object o : invoicelist) {
             Object[] obj = (Object[])o;
             obj1.put("sales" , Double.parseDouble(obj[0].toString()));
             obj1.put("date" , obj[3].toString());
             pieDataSet.add(obj1);
             obj1 = new JSONObject();
         }
         PrintWriter writer = res.getWriter();
         writer.write(pieDataSet.toString());
         writer.close();

		return new ModelAndView("rpt_charts_date_range_sales");

	}
	
	
	@RequestMapping(value = {"rpt_charts_date_range_profit.htm"})
	public ModelAndView getdaterangeprofit(){
		System.out.println("date range sales ;;;;;;;;;;;;;;;;;;;");
		return new ModelAndView("rpt_charts_date_range_profit");
	}
	
	

	@RequestMapping(value = {"loaddaterangeprofitdata"})
	public ModelAndView getdaterangeprofit(HttpServletRequest hsr,HttpServletResponse res) throws IOException{
		
		System.out.println("loadchartdata loadchartdata loadchartdata;;;;;;;;;;;;;;;;;;;");
		String mdate = hsr.getParameter("mdate");
		String mdate2 = hsr.getParameter("mdate2");
		System.out.println("from date ------------ > "  + mdate);
		System.out.println("to date ------------ > "  + mdate2);
		
		List<Invoice> invoicelist = invoiceService.getAllProfitByDateRange(mdate, mdate2);
		
		System.out.println("rpt_profit_by_date : ---> " + invoicelist);
		
		
		 ArrayList<Object> pieDataSet = new ArrayList<Object>();
         JSONObject obj1  = new JSONObject();
         for(Object o : invoicelist) {
             Object[] obj = (Object[])o;
             obj1.put("profit" , Double.parseDouble(obj[2].toString()));
             obj1.put("date" , obj[3].toString());
             pieDataSet.add(obj1);
             obj1 = new JSONObject();
         }
         PrintWriter writer = res.getWriter();
         writer.write(pieDataSet.toString());
         writer.close();

		return new ModelAndView("rpt_charts_date_range_profit");

	}
	
}
