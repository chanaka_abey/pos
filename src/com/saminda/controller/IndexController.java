package com.saminda.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;








import com.saminda.entity.Invoice;
import com.saminda.entity.Item;
import com.saminda.entity.Sales;
import com.saminda.service.InvoiceService;
import com.saminda.service.ItemService;
import com.saminda.service.SalesService;

@Controller
public class IndexController {

private static final Logger logger = Logger.getLogger(IndexController.class);
	
	public IndexController() {
		System.out.println("IndexController()");
	}
	
	@Autowired
	private ItemService itemService;
	
	@Autowired
	private InvoiceService invoiceService; 
	
	@Autowired
	private SalesService salesService;

	
	@RequestMapping(value = {"index.htm"})
    public ModelAndView getindexpage(HttpServletRequest hsr) {
		Map<String, Object> myModel = new HashMap<String, Object>();
    	logger.info("Getting index page");
    	logger.info("Getting the all Items in Index Page.");
    	System.out.println("Getting the all Items in Index Page............");
    	
    	HttpSession session = hsr.getSession();
    	String username = hsr.getParameter("username");
    	session.getAttribute("username");
    	
		List<Item> itemlist = itemService.getAllItem();
		List<Item> itemlist2 = itemService.getAllItemBelowReorderlevel();
		List<Invoice> invoice = invoiceService.getSalesSum();
		List<Invoice> profitsum = invoiceService.getProfitSum();
		List<Invoice> todayProfitsum = invoiceService.getTodayProfitSum();
		List<Invoice> todaySalessum = invoiceService.getTodaySalesSum();
		for (Item item : itemlist2) {
			System.out.println("Item ::: " + item.getId());
		}
		List<Invoice> invoiceid = invoiceService.getLastInvoiceNo();
		System.out.println("itemService.getAllItemBelowReorderlevel() "+ itemlist2);
		System.out.println("invoiceService.getLastInvoiceNo() "+ invoiceid.get(0));
		String invoice_no1 = invoiceid.toString();
		String[] invoice_no2 = invoice_no1.split("_");
		String newInvoiceNo = invoice_no2[1];
		String[] newInvoiceno2 = newInvoiceNo.split("]");
		String newNumber = newInvoiceno2[0];
		
		Long num23 = Long.parseLong(newNumber) + 1;
		//String actual_invoice_no = "IV_"
		
		System.out.println("newInvoiceNo newInvoiceNo "+ num23);
		
		//System.out.println("invoiceService.getLastInvoiceNo  "+ invoiceid.get(1));
		System.out.println("invoiceService.getSalesSum() "+ invoice.get(0));
		System.out.println("username username username username ---->  "+ session.getAttribute("username")); 
		//session.setAttribute("username", username);
		
		List<Sales> saleslist = salesService.getTodaySales();
		
		System.out.println("Today Sales ------------> " + saleslist);
		
		
		myModel.put("username", username);
		myModel.put("itemlist", itemlist);
		myModel.put("itemlist2", itemlist2);
		myModel.put("num23", num23);
		myModel.put("invoice", invoice.get(0));
		myModel.put("profitsum", profitsum.get(0));
		myModel.put("todayProfitsum", todayProfitsum.get(0));
		myModel.put("todaySalessum", todaySalessum.get(0));
		myModel.put("saleslist", saleslist);
		if(session.getAttribute("username") == null){
			return new ModelAndView("redirect:login.htm");
		}
		else{
			return new ModelAndView("index","myModel",myModel);
		}
		
    }
	
}
