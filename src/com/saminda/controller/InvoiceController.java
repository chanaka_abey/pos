package com.saminda.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.jboss.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.saminda.entity.Activity;
import com.saminda.entity.Invoice;
import com.saminda.entity.Item;
import com.saminda.entity.Sales;
import com.saminda.entity.Stock;
import com.saminda.service.ActivityService;
import com.saminda.service.InvoiceService;
import com.saminda.service.ItemService;
import com.saminda.service.SalesService;

@Controller
public class InvoiceController {

	private static final Logger logger = Logger.getLogger(InvoiceController.class);
	
	public InvoiceController(){
		System.out.println("InvoiceController()");
	}
	
	@Autowired
	private InvoiceService invoiceService;
	
	@Autowired
	private ActivityService activityService;
	
	@Autowired
	private SalesService salesService;
	
	@Autowired
	private ItemService itemService;
	
	@RequestMapping(value={"invoicepage.htm"})
	public ModelAndView getInvoicePage(HttpServletRequest hsr){
		logger.info("invoicepage controller load ");
		System.out.println("invoicepage controller load..");
		
		String invoiceid = hsr.getParameter("invoiceid");
		String total_amount = hsr.getParameter("total_amount");
		String unit_total = hsr.getParameter("unit_total");
		String profit_total = hsr.getParameter("profit");
		String total_discount = hsr.getParameter("total_discount");
		
		Double totalsale = Double.parseDouble(total_amount);
		Double unitprice = Double.parseDouble(unit_total);
		Double profit = Double.parseDouble(profit_total);
		int discount = Integer.parseInt(total_discount);
		
		System.out.println("invoiceid invoiceid invoiceid    " + invoiceid); 
		 Date time1 = new Date( );
	      SimpleDateFormat ft1 =new SimpleDateFormat ("yyyy-MM-dd");
	      SimpleDateFormat ft2 =new SimpleDateFormat ("hh:mm:ss");
		
		Invoice invoice = new Invoice();
		invoice.setInvoicenumber("premasiri"+invoiceid);
		System.out.println("Invoice Number"+invoice.getInvoicenumber());
		invoice.setPurchasedate(ft1.format(time1));
		invoice.setPurchasetime(ft2.format(time1));
		invoice.setTotalsalesprice(totalsale);
		invoice.setTotalunitprice(unitprice);
		invoice.setTotaldiscount(discount);
		invoice.setProfit(profit);
		
		invoiceService.createInvoice(invoice);
		
		return new ModelAndView("redirect:index.htm");
	}
	
	@RequestMapping(value={"invoicepage22.htm"})
	public ModelAndView getLastInvoiceID(HttpServletRequest hsr){
		invoiceService.getInvoice("ccc");
		return new ModelAndView("redirect:index.htm");
	}
	
	@RequestMapping(value={"invoice.htm"})
	public ModelAndView getinvoices(HttpServletRequest hsr){
		
		HttpSession session = hsr.getSession();
		if(session.getAttribute("role").equals("Admin")){
			Map<String, Object> myModel = new HashMap<String, Object>();
			List<Invoice> invoicelist = invoiceService.getAllInvoice();
			
			myModel.put("invoicelist", invoicelist);
			
			return new ModelAndView("invoice","myModel",myModel);
		}
		else{
			return new ModelAndView("redirect:sales.htm");
		}
	}
	
	@RequestMapping(value={"deleteInvoice"})
	public ModelAndView deleteInvoices(@RequestParam long id,HttpServletRequest hsr){
		logger.info("Deleting the Invoice. Id : "+id);
		HttpSession session = hsr.getSession();
		if(session.getAttribute("role").equals("Admin")){
			
			Invoice invoice = invoiceService.getInvoice(id);
			
			String username = session.getAttribute("username").toString();
		    System.out.println("Sales Delete: " + username);
		    Activity activity = new Activity();
		    Date time1 = new Date( );
		    SimpleDateFormat ft1 =new SimpleDateFormat ("yyyy-MM-dd");
		    SimpleDateFormat ft2 =new SimpleDateFormat ("hh:mm:ss");
		    activity.setLogdate(ft1.format(time1));
		    activity.setLogtime(ft2.format(time1));
		    activity.setActivity("DELETE invoice no :  "+invoice.getInvoicenumber() +" in invoice table");
		    activity.setAction("DELETE");
		    activity.setUser(username); 
		    activityService.createActivity(activity);
			
			
			invoiceService.deleteInvoice(id);
			System.out.println("Delete Invoice ID : " + id);
		return new ModelAndView("redirect:invoice.htm");
		}
		else{
			return new ModelAndView("redirect:sales.htm");
		}
	}
	
	
	@RequestMapping(value = { "getSalesFromInvoicenumber" })
	public ModelAndView getAllSalesPreview(HttpServletRequest hsr,
			HttpServletResponse response, @ModelAttribute Stock stock) {
		PrintWriter writer = null;
		try {
			writer = response.getWriter();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String invoicenumber = hsr.getParameter("invoicenumber");
		Map<String, Object> myModel = new HashMap<String, Object>();
		List<Sales> saleslist2 = salesService.getAllSalesByInvoicenumber(invoicenumber);
		//myModel.put("saleslist2", saleslist2);

		JSONObject questionJsionObj = new JSONObject();
		JSONObject questionJsionObj1 = new JSONObject();
JSONArray jArray=new JSONArray();
int count=0;
		
		for (Sales sales : saleslist2) {
			//questionJsionObj.put("invoicenumber", sales.getInvoicenumber().toString());
			questionJsionObj.put("Itemname", sales.getItemname().toString());
			//questionJsionObj.put("Itemid", sales.getItemid().toString()); 
			questionJsionObj.put("Quantity", sales.getQuantity()); 
			//questionJsionObj.put("Discount", sales.getDiscount()); 
			//questionJsionObj.put("Price", sales.getSalesprice()); 
			questionJsionObj.put("salesid", sales.getId());
			questionJsionObj.put("item_id", sales.getItem_id());
			jArray.add(count, questionJsionObj.toJSONString()); 
			count ++;
		}
		
		
		//questionJsionObj.put("saleslist2",saleslist2);
	//	questionJsionObj1.put("values", jArray);
		writer.write(jArray.toString());
		writer.close();
		return new ModelAndView("invoice");

	}
	
	
	
	@RequestMapping(value = { "deletesalefrominvoice" })
	public ModelAndView deletesalesfrominvoicenumber(HttpServletRequest hsr){
		HttpSession session = hsr.getSession();
		String id = hsr.getParameter("id");
		String qty = hsr.getParameter("qty");
		String itemid = hsr.getParameter("item_id");
		long item_id = Long.parseLong(itemid);
		long salesid = Long.parseLong(id);
		int quantity = Integer.parseInt(qty);
		
	
		
		Item item = itemService.getItem(item_id);
		int ori_qty =  item.getQuantity();
		int total_qty = ori_qty + quantity;
		
		System.out.println("Delete Sales ID From Invoice Table : " + salesid);
		System.out.println("Delete Item ID From Invoice Table : " + itemid);
		System.out.println("Delete Quantity From Invoice Table : " + total_qty);

		item.setQuantity(total_qty);
		itemService.updateItem(item);
		
		salesService.deleteSales(salesid);
		
		return new ModelAndView("redirect:invoice.htm");
	}
	
	
	
	
	
}


