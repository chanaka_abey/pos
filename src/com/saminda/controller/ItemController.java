package com.saminda.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.saminda.entity.Activity;
import com.saminda.entity.Item;
import com.saminda.service.ActivityService;
import com.saminda.service.ItemService;

@Controller
public class ItemController {

	private static final Logger logger = Logger.getLogger(ItemController.class);

	public ItemController() {
		System.out.println("ItemController()");
	}

	@Autowired
	private ItemService itemService;

	@Autowired
	private ActivityService activityService;

	@RequestMapping(value = { "add_new_item.htm" })
	public ModelAndView createItem(@ModelAttribute Item item,
			HttpServletRequest hsr) {
		logger.info("Creating Item. Data: " + item);

		HttpSession session = hsr.getSession();
		if (session.getAttribute("role").equals("Admin")) {

			Map<String, Object> myModel = new HashMap<String, Object>();
			List<Item> itemlist2 = itemService.getAllItemBelowReorderlevel();
			myModel.put("itemlist2", itemlist2);
			return new ModelAndView("add_new_item", "myModel", myModel);
		} else {
			return new ModelAndView("redirect:items.htm");
		}
	}

	@RequestMapping(value = { "saveItem" })
	public String saveItem(HttpServletRequest hsr) {
		// logger.info("Saving the Item. Data :  "+ item);
		System.out.println("saveItem function open");
		HttpSession session = hsr.getSession();

		try {

			String id = hsr.getParameter("id");
			// Long myid = Long.parseLong(id);
			String itemid = hsr.getParameter("itemid");
			String itemname = hsr.getParameter("itemname");
			String category = hsr.getParameter("category");
			String position = hsr.getParameter("position");
			String unitprice = hsr.getParameter("unitprice");
			String salesprice = hsr.getParameter("salesprice");
			String quantity = hsr.getParameter("quantity");
			String reorderlevel = hsr.getParameter("reorderlevel");

			float unitp = Float.parseFloat(unitprice);
			float salesp = Float.parseFloat(salesprice);
			int qty = Integer.parseInt(quantity);
			int reorder = Integer.parseInt(reorderlevel);

			System.out.println("myid ---> " + id);
			System.out.println("itemid ---> " + itemid);
			System.out.println("itemname ---> " + itemname);
			System.out.println("category ---> " + category);
			System.out.println("position ---> " + position);
			System.out.println("unitprice ---> " + unitprice);
			System.out.println("salesprice ---> " + salesprice);
			System.out.println("quantity ---> " + quantity);
			System.out.println("reorderlevel ---> " + reorderlevel);
			

			if (id == "" || id == "null" || id == null) {
				System.out.println("Item Created ");

				Item item = new Item();
				item.setItemid(itemid);
				item.setItemname(itemname);
				item.setUnitprice(unitp);
				item.setSalesprice(salesp);
				
				
				if(category == "" || category == "null" || category == null){
					item.setCategory("none");
				}else{
					item.setCategory(category);
				}
				
				
				if(position == "" || position == "null" || position == null){
					item.setPosition("none");
				}else{
					item.setPosition(position);
				}
				
				item.setQuantity(qty);
				item.setReorderlevel(reorder);

				String username = session.getAttribute("username").toString();
				System.out.println("Sales Create User : " + username);
				Activity activity = new Activity();
				Date time1 = new Date();
				SimpleDateFormat ft1 = new SimpleDateFormat("yyyy-MM-dd");
				SimpleDateFormat ft2 = new SimpleDateFormat("hh:mm:ss");
				activity.setLogdate(ft1.format(time1));
				activity.setLogtime(ft2.format(time1));
				activity.setActivity("Create new Itemid " + itemid
						+ " and Itemname : " + itemname);
				activity.setAction("CREATE");
				activity.setUser(username);
				activityService.createActivity(activity);

				logger.info("createItem createItem createItem :  ");
				itemService.createItem(item);

				return "redirect:index.htm";

			} else {
				System.out.println("Item Updated");

				Item item = new Item();
				Long id22 = Long.parseLong(id);
				item.setId(id22);
				item.setItemid(itemid);
				item.setItemname(itemname);
				item.setUnitprice(unitp);
				item.setSalesprice(salesp);
				item.setCategory(category);
				item.setPosition(position);
				
				// item.setQuantity(qty);
				item.setReorderlevel(reorder);

				int quantity444 = itemService.getItem(id22).getQuantity();

				System.out.println("Stock avilable quantity : " + quantity444);
				System.out.println("Stock updated quantity : " + qty);
				int updatedquantity = quantity444 + qty;
				item.setQuantity(updatedquantity);

				System.out.println("updatedquantity : " + updatedquantity);
				itemService.updateItem(item);

				// String quntity2 = hsr.getParameter("quantity");
				// int qty = Integer.parseInt(quntity2);

				String username = session.getAttribute("username").toString();
				System.out.println("Sales Create User : " + username);
				Activity activity = new Activity();
				Date time1 = new Date();
				SimpleDateFormat ft1 = new SimpleDateFormat("yyyy-MM-dd");
				SimpleDateFormat ft2 = new SimpleDateFormat("hh:mm:ss");
				activity.setLogdate(ft1.format(time1));
				activity.setLogtime(ft2.format(time1));
				activity.setActivity("Edit Itemid " + item.getItemid());
				activity.setAction("EDIT");
				activity.setUser(username);
				activityService.createActivity(activity);

				return "redirect:index.htm";

			}

		} catch (Exception e) {
			System.out.println("Error : " + e);
		}

		return "redirect:index.htm";
	}

	@RequestMapping(value = { "changeItem" })
	public String changeItem(HttpServletRequest hsr) {
		HttpSession session = hsr.getSession();
		String id = hsr.getParameter("id");
		// Long myid = Long.parseLong(id);
		String itemid = hsr.getParameter("itemid");
		String itemname = hsr.getParameter("itemname");
		String category = hsr.getParameter("category");
		String position = hsr.getParameter("position");
		String unitprice = hsr.getParameter("unitprice");
		String salesprice = hsr.getParameter("salesprice");
		String quantity = hsr.getParameter("quantity");
		String reorderlevel = hsr.getParameter("reorderlevel");

		float unitp = Float.parseFloat(unitprice);
		float salesp = Float.parseFloat(salesprice);
		int qty = Integer.parseInt(quantity);
		int reorder = Integer.parseInt(reorderlevel);

		Item item = new Item();
		Long id22 = Long.parseLong(id);
		item.setId(id22);
		item.setItemid(itemid);
		item.setItemname(itemname);
		item.setUnitprice(unitp);
		item.setSalesprice(salesp);
		item.setCategory(category);
		item.setPosition(position);
		item.setQuantity(qty);
		item.setReorderlevel(reorder);

		String username = session.getAttribute("username").toString();
		Activity activity = new Activity();
		Date time1 = new Date();
		SimpleDateFormat ft1 = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat ft2 = new SimpleDateFormat("hh:mm:ss");
		activity.setLogdate(ft1.format(time1));
		activity.setLogtime(ft2.format(time1));
		activity.setActivity("Change Itemid " + item.getItemid() +" with " + qty + " quantity");
		activity.setAction("CHANGE");
		activity.setUser(username);
		activityService.createActivity(activity);

		itemService.updateItem(item);

		return "redirect:index.htm";

	}

	@RequestMapping(value = { "editItem" })
	public ModelAndView editItem(@RequestParam long id,
			@ModelAttribute Item item, HttpServletRequest hsr) {
		Map<String, Object> myModel = new HashMap<String, Object>();

		HttpSession session = hsr.getSession();
		System.out.println("session.getAttribute role : "
				+ session.getAttribute("role"));
		// String rolename = session.getAttribute("role").toString();
		// System.out.println("session.getAttribute role ----->  : " +
		// rolename);
		if (session.getAttribute("role").equals("Admin")) {
			logger.info("Updating the Item for the Id " + id);

			item = itemService.getItem(id);
			List<Item> itemlist2 = itemService.getAllItemBelowReorderlevel();
			myModel.put("itemobject", item);
			myModel.put("itemlist2", itemlist2);
			return new ModelAndView("add_new_item", "myModel", myModel);

		} else {
			return new ModelAndView("redirect:items.htm");
		}

	}

	@RequestMapping(value = { "deleteItem" })
	public ModelAndView deleteItem(@RequestParam long id, HttpServletRequest hsr) {
		logger.info("Deleting the Item. Id : " + id);
		HttpSession session = hsr.getSession();

		if (session.getAttribute("role").equals("Admin")) {

			Item item2 = itemService.getItem(id);

			String username = session.getAttribute("username").toString();
			System.out.println("Sales Create User : " + username);
			System.out.println("Delete Item id : " + item2.getItemname());
			Activity activity = new Activity();
			Date time1 = new Date();
			SimpleDateFormat ft1 = new SimpleDateFormat("yyyy-MM-dd");
			SimpleDateFormat ft2 = new SimpleDateFormat("hh:mm:ss");
			activity.setLogdate(ft1.format(time1));
			activity.setLogtime(ft2.format(time1));
			activity.setActivity("Delete itemid : " + item2.getItemid()
					+ " and itemname : " + item2.getItemname()
					+ " from Item Table");
			activity.setAction("DELETE");
			activity.setUser(username);
			activityService.createActivity(activity);

			itemService.deleteItem(id);

			return new ModelAndView("redirect:items.htm");
		} else {
			return new ModelAndView("redirect:items.htm");
		}
	}

	@RequestMapping(value = { "items.htm" })
	public ModelAndView getAllItem(HttpServletRequest hsr) {
		Map<String, Object> myModel = new HashMap<String, Object>();
		HttpSession session = hsr.getSession();
		logger.info("Getting the all Items.");
		List<Item> itemlist = itemService.getAllItem();
		List<Item> itemlist2 = itemService.getAllItemBelowReorderlevel();

		myModel.put("itemlist", itemlist);
		myModel.put("itemlist2", itemlist2);

		if (session.getAttribute("username") == null) {
			return new ModelAndView("redirect:login.htm");
		} else {
			System.out.println("Items load...............");
			return new ModelAndView("items", "myModel", myModel);
		}

	}

	@RequestMapping(value = { "stock_reports.htm" })
	public ModelAndView getstockreports(HttpServletRequest hsr) {

		HttpSession session = hsr.getSession();
		if (session.getAttribute("role").equals("Admin")) {

			return new ModelAndView("stock_reports");

		} else {
			return new ModelAndView("redirect:items.htm");
		}
	}

	@RequestMapping(value = "reorder_list.htm")
	public ModelAndView getreorderlistpage() {
		System.out.println("reoder_list open");
		List<Item> itemlist = itemService.getAllItem();
		System.out.println("itemlist : " + itemlist);
		return new ModelAndView("reorder_list", "itemlist", itemlist);
	}
	
	
	@RequestMapping(value = {"saveSupplierItem"})
	public ModelAndView saveSupplierItem(HttpServletRequest hsr){
		
		System.out.println("saveSupplierItem open-------");
		HttpSession session = hsr.getSession();
		
		try{
		
		String itemid = hsr.getParameter("itemid");
		String itemname = hsr.getParameter("itemname");
		String category = hsr.getParameter("category");
		String position = hsr.getParameter("position");
		String quantity = hsr.getParameter("quantity");
		String unitprice = hsr.getParameter("unitprice");
		String salesprice = hsr.getParameter("salesprice");
		String reorder = hsr.getParameter("reorder");
		
		int unit_price = Integer.parseInt(unitprice);
		int sales_price = Integer.parseInt(salesprice);
		int qty = Integer.parseInt(quantity);
		int re_level = Integer.parseInt(reorder);
		
		Item item = new Item();
		item.setItemid(itemid);
		item.setItemname(itemname);
		item.setCategory(category);
		item.setPosition(position);
		item.setUnitprice(unit_price);
		item.setSalesprice(sales_price);
		item.setQuantity(qty);
		item.setReorderlevel(re_level);
		
		itemService.createItem(item);
			
	    return new ModelAndView("redirect:index.htm");
	    
		}
		catch(Exception e){
			System.out.println("Error === : " + e);
			return new ModelAndView("redirect:index.htm");
		}
			
	}

}
