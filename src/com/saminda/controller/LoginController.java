package com.saminda.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.saminda.entity.Activity;
import com.saminda.entity.Sales;
import com.saminda.entity.User;
import com.saminda.service.ActivityService;
import com.saminda.service.UserService;

@Controller
@RequestMapping(value={"/","login"})
public class LoginController {
	
private static final Logger logger = Logger.getLogger(LoginController.class);
	
	public LoginController() {
		System.out.println("LoginController()");
	}
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private ActivityService activityService;

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView showForm(Map model,HttpServletRequest hsr) {
		
		User loginForm = new User();
		model.put("loginForm", loginForm);
		HttpSession session=hsr.getSession();  
        session.invalidate();  
		return new ModelAndView("login");
	}

	@RequestMapping(method = RequestMethod.POST)
	public ModelAndView processForm(@Valid User loginForm, BindingResult result,HttpServletRequest hsr) {

		Map<String, Object> myModel = new HashMap<String, Object>();
		
		
		String username = hsr.getParameter("username");
		

		String password = loginForm.getPassword();
		//String role = loginForm.getRole();
		String err = "Invalid Username or Password!! Please try again!!!";

		System.out.println("User Name hrs: " + username);
		System.out.println("password : " + password);
		//System.out.println("role : " + role);
	
		
		boolean userExists = userService.checkLogin(loginForm.getUsername(), loginForm.getPassword());
		if(userExists){
			
			User user = userService.getUserbyUsername(username);
			String role = user.getRole();
			
			System.out.println("This is new ROlE -----------> " + role);

			
			HttpSession session = hsr.getSession();
			session.setAttribute("username", username);
			session.setAttribute("role", role);
			myModel.put("username", username);
			hsr.setAttribute("username", username);
			
			Activity activity = new Activity();
			Date time1 = new Date( );
		    SimpleDateFormat ft1 =new SimpleDateFormat ("yyyy-MM-dd");
		    SimpleDateFormat ft2 =new SimpleDateFormat ("hh:mm:ss");
		    
		    activity.setLogdate(ft1.format(time1));
		    activity.setLogtime(ft2.format(time1));
		    activity.setActivity(username +" login to system");
		    activity.setAction("LOGIN");
		    activity.setUser(username);
		    
		    activityService.createActivity(activity);

			return new ModelAndView("redirect:index.htm","myModel",myModel);
		}else{
			//result.rejectValue("username","invaliduser");
			myModel.put("err", err);
			return new ModelAndView("redirect:login.htm","myModel",myModel);
		}

	}
	
	
}
