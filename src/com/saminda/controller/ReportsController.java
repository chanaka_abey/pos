package com.saminda.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.jboss.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.saminda.entity.Invoice;
import com.saminda.entity.Item;
import com.saminda.entity.Sales;
import com.saminda.entity.Stock;
import com.saminda.service.InvoiceService;
import com.saminda.service.ItemService;
import com.saminda.service.SalesService;
import com.saminda.service.StockService;

@Controller
public class ReportsController {

	private static final Logger logger = Logger.getLogger(ItemController.class);

	public ReportsController() {
		System.out.println("ReportsController()");
	}

	@Autowired
	private ItemService itemService;

	@Autowired
	private StockService stockService;

	@Autowired
	private SalesService salesService;

	@Autowired
	private InvoiceService invoiceService;

	@RequestMapping(value = { "reports.htm" })
	public ModelAndView getreportspage(HttpServletRequest hsr) {

		HttpSession session = hsr.getSession();

		if (session.getAttribute("username") == null) {
			System.out.println("userrole is null");
			return new ModelAndView("redirect:login.htm");
		} else if ((session.getAttribute("role").equals("Admin"))) {
			System.out.println("userrole is admin");
			return new ModelAndView("reports");
		} else {
			System.out.println("userrole is user");
			return new ModelAndView("index.htm");
		}

	}

	/** .............Stock Reports Start............ **/

	@RequestMapping(value = { "rpt_stock_overall.htm" })
	public ModelAndView getstockovrallreport() {

		List<Item> itemlist = itemService.getAllItem();

		return new ModelAndView("rpt_stock_overall", "itemlist", itemlist);
	}

	@RequestMapping(value = { "rpt_stock_belowreoder.htm" })
	public ModelAndView getstockbelowreorderlevelreport() {

		List<Item> itemlist = itemService.getAllItem();

		return new ModelAndView("rpt_stock_belowreoder", "itemlist", itemlist);
	}

	@RequestMapping(value = { "rpt_stock_update_by_date.htm" })
	public ModelAndView getStockUpdatesByDate(HttpServletRequest hsr,
			@ModelAttribute Stock stock) {
		String update_date = hsr.getParameter("mdate");
		// System.out.println("@ModelAttribute Stock stock in rpt_stock_update_by_date.htm : "+
		// stock.getUpdate_date());
		System.out.println("mdate update_date update_date update_date ---> "
				+ update_date);

		// List<Stock> stockupdatelist2 =
		// stockService.getStockByDate(stock.getUpdate_date());
		List<Stock> stockupdatelist = stockService.getStockByDate(update_date);

		return new ModelAndView("rpt_stock_update_by_date", "stockupdatelist",
				stockupdatelist);
	}

	@RequestMapping(value = { "rpt_stock_update_by_date_range.htm" })
	public ModelAndView getStockUpdatesByDateRange(HttpServletRequest hsr,
			@ModelAttribute Stock stock) {
		String update_date = hsr.getParameter("mdate");
		String update_date2 = hsr.getParameter("mdate2");
		// System.out.println("rpt_stock_update_by_date_range update_date : "+
		// stock.getUpdate_date());
		// System.out.println("rpt_stock_update_by_date_range update_time: "+
		// stock.getUpdate_time());
		System.out.println("mdate update_date update_date update_date 1  ---> "
				+ update_date);
		System.out.println("mdate update_date update_date update_date 2---> "
				+ update_date2);

		// List<Stock> stockupdatelist =
		// stockService.getStockByDateRange(stock.getUpdate_date(),stock.getUpdate_time());
		List<Stock> stockupdatelist = stockService.getStockByDateRange(
				update_date, update_date2);
		return new ModelAndView("rpt_stock_update_by_date_range",
				"stockupdatelist", stockupdatelist);

	}

	@RequestMapping(value = { "rpt_stock_update_by_itemname.htm" })
	public ModelAndView getStockUpdatesByItemName(HttpServletRequest hsr,
			@ModelAttribute Stock stock) {
		System.out
				.println("@ModelAttribute rpt_stock_update_by_itemname.htm : "
						+ stock.getItemname());
		String itemname = hsr.getParameter("itemname");
		System.out.println("Itemname : -----> " + itemname);
		Map<String, Object> myModel = new HashMap<String, Object>();
		List<Item> stocklist = itemService.getAllItem();

		// List<Stock> stockupdatelist =
		// stockService.getStockByItemName(stock.getItemname());
		List<Stock> stockupdatelist = stockService.getStockByItemName(itemname);
		myModel.put("stocklist", stocklist);
		myModel.put("stockupdatelist", stockupdatelist);
		return new ModelAndView("rpt_stock_update_by_itemname", "myModel",
				myModel);

	}

	/** ....................... Stock Reports End.................... **/

	/** ........................ Sales Reports Start ................. **/

	@RequestMapping(value = { "sales_reports.htm" })
	public ModelAndView getSalesReports(HttpServletRequest hsr) {

		HttpSession session = hsr.getSession();
		if (session.getAttribute("role").equals("Admin")) {
			return new ModelAndView("sales_reports");
		} else {
			return new ModelAndView("redirect:sales.htm");
		}
	}

	@RequestMapping(value = { "rpt_sales_overall.htm" })
	public ModelAndView getOverallSalesReport() {

		List<Sales> saleslist = salesService.getAllSales();

		return new ModelAndView("rpt_sales_overall", "saleslist", saleslist);

	}

	@RequestMapping(value = { "rpt_sales_by_date.htm" })
	public ModelAndView getOverallSalesByDateReport(
			@ModelAttribute Sales sales, HttpServletRequest hsr) {

		String mdate = hsr.getParameter("mdate");

		// System.out.println("rpt_sales_by_date.htm " +
		// sales.getPurchasedate());
		System.out.println("rpt_sales_by_date.htm " + mdate);

		// List<Sales> saleslist =
		// salesService.getAllSalesByDate(sales.getPurchasedate());
		List<Sales> saleslist = salesService.getAllSalesByDate(mdate);

		return new ModelAndView("rpt_sales_by_date", "saleslist", saleslist);

	}

	@RequestMapping(value = { "rpt_sales_by_date_range.htm" })
	public ModelAndView getOverallSalesByDateRangeReport(
			@ModelAttribute Sales sales, HttpServletRequest hsr) {

		String mdate = hsr.getParameter("mdate");
		String mdate2 = hsr.getParameter("mdate2");

		// System.out.println("rpt_sales_by_date_range.htm  from date " +
		// sales.getPurchasedate());
		// System.out.println("rpt_sales_by_date_range.htm  to date " +
		// sales.getPurchasetime());
		System.out.println("rpt_sales_by_date_range.htm  from date " + mdate);
		System.out.println("rpt_sales_by_date_range.htm  to date " + mdate2);

		// List<Sales> saleslist =
		// salesService.getAllSalesByDateRange(sales.getPurchasedate(),
		// sales.getPurchasetime());
		List<Sales> saleslist = salesService.getAllSalesByDateRange(mdate,
				mdate2);

		return new ModelAndView("rpt_sales_by_date_range", "saleslist",
				saleslist);

	}

	@RequestMapping(value = { "rpt_sales_by_itemname.htm" })
	public ModelAndView getSalesByItemname(HttpServletRequest hsr,
			@ModelAttribute Sales sales) {
		String itemname = hsr.getParameter("itemname");
		// System.out.println("@ModelAttribute rpt_stock_update_by_itemname.htm : "+
		// sales.getItemname());
		System.out
				.println("@ModelAttribute rpt_stock_update_by_itemname.htm : "
						+ itemname);

		Map<String, Object> myModel = new HashMap<String, Object>();
		List<Item> saleslist1 = itemService.getAllItem();

		// List<Sales> saleslist2 =
		// salesService.getAllSalesByItemName(sales.getItemname());
		List<Sales> saleslist2 = salesService.getAllSalesByItemName(itemname);
		myModel.put("saleslist1", saleslist1);
		myModel.put("saleslist2", saleslist2);
		return new ModelAndView("rpt_sales_by_itemname", "myModel", myModel);

	}

	@RequestMapping(value = { "rpt_sales_by_invoicenumber.htm" })
	public ModelAndView getSalesByInvoicenumber(HttpServletRequest hsr,
			@ModelAttribute Stock stock) {
		System.out
				.println("rpt_sales_by_invoicenumber    rpt_sales_by_invoicenumber  ");

		String invoicenumber = hsr.getParameter("invoicenumber");
		// System.out.println("@ModelAttribute rpt_stock_update_by_itemname.htm : "+
		// sales.getItemname());
		System.out
				.println("@ModelAttribute rpt_stock_update_by_itemname.htm : "
						+ invoicenumber);

		Map<String, Object> myModel = new HashMap<String, Object>();
		List<Invoice> invoicelist = invoiceService.getAllInvoice();

		// List<Sales> saleslist2 =
		// salesService.getAllSalesByItemName(sales.getItemname());
		List<Sales> saleslist2 = salesService
				.getAllSalesByInvoicenumber(invoicenumber);
		myModel.put("invoicelist", invoicelist);
		myModel.put("saleslist2", saleslist2);
		return new ModelAndView("rpt_sales_by_invoicenumber", "myModel",
				myModel);
	}

	@RequestMapping(value = { "load_sales_preview.htm" })
	public ModelAndView getSalesPreview(HttpServletRequest hsr,
			@ModelAttribute Stock stock) {
		System.out.println("load_sales_preview    load_sales_preview  ");

		String invoicenumber = hsr.getParameter("invoicenumber");
		// System.out.println("@ModelAttribute rpt_stock_update_by_itemname.htm : "+
		// sales.getItemname());
		System.out
				.println("@ModelAttribute rpt_stock_update_by_itemname.htm : "
						+ invoicenumber);

		Map<String, Object> myModel = new HashMap<String, Object>();
		List<Invoice> invoicelist = invoiceService.getAllInvoice();

		// List<Sales> saleslist2 =
		// salesService.getAllSalesByItemName(sales.getItemname());
		List<Sales> saleslist2 = salesService
				.getAllSalesByInvoicenumber(invoicenumber);
		myModel.put("invoicelist", invoicelist);
		myModel.put("saleslist2", saleslist2);
		return new ModelAndView("invoice", "myModel", myModel);
	}

	@RequestMapping(value = { "load_sales_by_invoice_no.htm" })
	public ModelAndView getAllSalesPreview(HttpServletRequest hsr,
			HttpServletResponse response, @ModelAttribute Stock stock) {
		PrintWriter writer = null;
		try {
			writer = response.getWriter();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String invoicenumber = hsr.getParameter("invoicenumber");
		Map<String, Object> myModel = new HashMap<String, Object>();
		List<Sales> saleslist2 = salesService.getAllSalesByInvoicenumber(invoicenumber);
		//myModel.put("saleslist2", saleslist2);

		JSONObject questionJsionObj = new JSONObject();
		JSONObject questionJsionObj1 = new JSONObject();
JSONArray jArray=new JSONArray();
int count=0;
		
		for (Sales sales : saleslist2) {
			questionJsionObj.put("invoicenumber", sales.getInvoicenumber().toString());
			questionJsionObj.put("Itemname", sales.getItemname().toString());
			questionJsionObj.put("Itemid", sales.getItemid().toString()); 
			questionJsionObj.put("Quantity", sales.getQuantity()); 
			questionJsionObj.put("Discount", sales.getDiscount()); 
			questionJsionObj.put("Price", sales.getSalesprice()); 
			jArray.add(count, questionJsionObj.toJSONString()); 
			count ++;
		}
		
		
		//questionJsionObj.put("saleslist2",saleslist2);
	//	questionJsionObj1.put("values", jArray);
		writer.write(jArray.toString());
		writer.close();
		return new ModelAndView("invoice");

	}

	/** ....................... Sales Reports End.................... **/

	/** ........................ Invoice Reports Start ................. **/

	@RequestMapping(value = { "invoice_reports.htm" })
	public ModelAndView getInvoiceReports(HttpServletRequest hsr) {

		HttpSession session = hsr.getSession();
		if (session.getAttribute("role").equals("Admin")) {
			return new ModelAndView("invoice_reports");
		} else {
			return new ModelAndView("redirect:sales.htm");
		}
	}

	@RequestMapping(value = { "rpt_invoice_overall.htm" })
	public ModelAndView getOverallInvoiceReport() {

		List<Invoice> invoicelist = invoiceService.getAllInvoice();

		return new ModelAndView("rpt_invoice_overall", "invoicelist",
				invoicelist);

	}

	@RequestMapping(value = { "rpt_invoices_by_date.htm" })
	public ModelAndView getOverallSalesByDateReport(
			@ModelAttribute Invoice invoice, HttpServletRequest hsr) {
		String mdate = hsr.getParameter("mdate");
		// System.out.println("@ModelAttribute Invoice " +
		// invoice.getPurchasedate());
		System.out.println("@ModelAttribute Invoice " + mdate);
		// List<Invoice> invoicelist =
		// invoiceService.getAllInvoiceByDate(invoice.getPurchasedate());
		List<Invoice> invoicelist = invoiceService.getAllInvoiceByDate(mdate);

		return new ModelAndView("rpt_invoices_by_date", "invoicelist",
				invoicelist);

	}

	@RequestMapping(value = { "rpt_invoices_by_date_range.htm" })
	public ModelAndView getOverallInvoiceByDateRangeReport(
			@ModelAttribute Invoice invoice, HttpServletRequest hsr) {

		String mdate = hsr.getParameter("mdate");
		String mdate2 = hsr.getParameter("mdate2");
		// System.out.println("rpt_invoices_by_date_range  from date " +
		// invoice.getPurchasedate());
		// System.out.println("rpt_invoices_by_date_range  to date " +
		// invoice.getPurchasetime());
		System.out.println("rpt_invoices_by_date_range  from date " + mdate);
		System.out.println("rpt_invoices_by_date_range  to date " + mdate2);

		// List<Invoice> invoicelist = invoiceService.getAllInvoiceByDateRange(
		// invoice.getPurchasedate(), invoice.getPurchasetime());
		List<Invoice> invoicelist = invoiceService.getAllInvoiceByDateRange(
				mdate, mdate2);

		return new ModelAndView("rpt_invoices_by_date_range", "invoicelist",
				invoicelist);

	}

	@RequestMapping(value = { "rpt_profit_by_date.htm" })
	public ModelAndView getOverallProfitbyDate() {

		List<Invoice> invoicelist = invoiceService.getOverallProfitByDate();

		System.out.println("rpt_profit_by_date : ---> " + invoicelist);

		return new ModelAndView("rpt_profit_by_date", "invoicelist",
				invoicelist);

	}

	@RequestMapping(value = { "rpt_profit_by_date_range.htm" })
	public ModelAndView getprofitbydaterenge(@ModelAttribute Invoice invoice,
			HttpServletRequest hsr) {
		String mdate = hsr.getParameter("mdate");
		String mdate2 = hsr.getParameter("mdate2");

		// System.out.println("rpt_profit_by_date_range  from date " +
		// invoice.getPurchasedate());
		// System.out.println("rpt_profit_by_date_range  to date " +
		// invoice.getPurchasetime());

		System.out.println("rpt_profit_by_date_range  from date " + mdate);
		System.out.println("rpt_profit_by_date_range  to date " + mdate2);

		// List<Invoice> invoicelist =
		// invoiceService.getAllProfitByDateRange(invoice.getPurchasedate(),
		// invoice.getPurchasetime());
		List<Invoice> invoicelist = invoiceService.getAllProfitByDateRange(
				mdate, mdate2);

		return new ModelAndView("rpt_profit_by_date_range", "invoicelist",
				invoicelist);

	}

	@RequestMapping(value = { "charts.htm" })
	public ModelAndView getcharts() {
		return new ModelAndView("charts");
	}

	@RequestMapping(value = { "rpt_charts_daily_sales.htm" })
	public ModelAndView getdailysalescharts() {
		System.out.println("rpt_charts_daily_sales;;;;;;;;;;;;;;;;;;;");
		return new ModelAndView("rpt_charts_daily_sales");
	}

	@RequestMapping(value = { "loadchartdata" })
	public ModelAndView getchartsdata(HttpServletRequest hsr,
			HttpServletResponse res) throws IOException {

		System.out
				.println("loadchartdata loadchartdata loadchartdata;;;;;;;;;;;;;;;;;;;");

		List<Invoice> invoicelist = invoiceService.getOverallProfitByDate();

		System.out.println("rpt_profit_by_date : ---> " + invoicelist);

		ArrayList<Object> pieDataSet = new ArrayList<Object>();
		JSONObject obj1 = new JSONObject();
		for (Object o : invoicelist) {
			Object[] obj = (Object[]) o;
			System.out.println(" Date  ------- > : " + obj[1].toString());
			System.out.println(" Profit  ------- > : " + obj[2].toString());
			System.out.println(" Unitprice  ------- > : " + obj[3].toString());
			System.out.println(" Sales  ------- > : " + obj[0].toString());
			obj1.put("sales", Double.parseDouble(obj[0].toString()));
			obj1.put("date", obj[3].toString());
			pieDataSet.add(obj1);
			obj1 = new JSONObject();
		}
		PrintWriter writer = res.getWriter();
		writer.write(pieDataSet.toString());
		writer.close();

		return new ModelAndView("rpt_charts_daily_sales");

	}

	@RequestMapping(value = { "rpt_charts_overall.htm" })
	public ModelAndView getoverallchart() {
		return new ModelAndView("rpt_charts_overall");
	}

	@RequestMapping(value = { "loadoverallchartdata" })
	public ModelAndView getoverallchartsdata(HttpServletRequest hsr,
			HttpServletResponse res) throws IOException {

		List<Invoice> invoicelist = invoiceService.getOverallProfitByDate();

		System.out.println("rpt_profit_by_date : ---> " + invoicelist);

		ArrayList<Object> pieDataSet = new ArrayList<Object>();
		JSONObject obj1 = new JSONObject();
		for (Object o : invoicelist) {
			Object[] obj = (Object[]) o;
			// System.out.println(" Date  ------- > : "+ obj[1].toString());
			// System.out.println(" Profit  ------- > : "+ obj[2].toString());
			System.out.println(" profit  ------- > : " + obj[2].toString());
			System.out.println(" Sales  ------- > : " + obj[0].toString());
			obj1.put("sales", Double.parseDouble(obj[0].toString()));
			obj1.put("date", obj[3].toString());
			obj1.put("profit", Double.parseDouble(obj[2].toString()));
			pieDataSet.add(obj1);
			obj1 = new JSONObject();
		}
		PrintWriter writer = res.getWriter();
		writer.write(pieDataSet.toString());
		writer.close();

		return new ModelAndView("rpt_charts_overall");

	}

}
