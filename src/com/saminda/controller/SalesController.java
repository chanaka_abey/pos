package com.saminda.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;








import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.saminda.entity.Activity;
import com.saminda.entity.Invoice;
import com.saminda.entity.Item;
import com.saminda.entity.Sales;
import com.saminda.service.ActivityService;
import com.saminda.service.InvoiceService;
import com.saminda.service.ItemService;
import com.saminda.service.SalesService;



@Controller
public class SalesController {

private static final Logger logger = Logger.getLogger(InvoiceController.class);
	
	public SalesController(){
		System.out.println("SalesController()");
	}
	
	@Autowired
	private SalesService salesService;
	
	
	@Autowired
	private ItemService itemService;
	
	@Autowired
	private ActivityService activityService;
	
	@Autowired
	private InvoiceService invoiceService;
	
	
	public ItemService getItemService() {
		return itemService;
	}




	public void setItemService(ItemService itemService) {
		this.itemService = itemService;
	}




	@RequestMapping(value={"getsalespage.htm"})
	public ModelAndView getSalesPage(HttpServletRequest hsr){
		logger.info("salespage controller load ");
		System.out.println("salespage controller load..");
		
		String invoiceid = hsr.getParameter("invoiceid");
		String ids = hsr.getParameter("id");
		String itemname = hsr.getParameter("itemname");
		String quntity = hsr.getParameter("quntity");
		String salesprice = hsr.getParameter("salesprice");
		String itemid = hsr.getParameter("itemid");
		String discount = hsr.getParameter("discount");
		String salesprice2 = hsr.getParameter("salesprice2");
		String unitprice2 = hsr.getParameter("unitprice33");
		String category = hsr.getParameter("category");
		String reorder = hsr.getParameter("reorder");
		
		
		int id  = Integer.parseInt(ids);
		int qty = Integer.parseInt(quntity);
		Float unitprice = Float.parseFloat(unitprice2);
		Float salesp = Float.parseFloat(salesprice2);
		Double total_sale = Double.parseDouble(salesprice);
		Double sales_price = Double.parseDouble(salesprice2);
		int disc = Integer.parseInt(discount);
		int reorderlevel = Integer.parseInt(reorder);
		
		Date time1 = new Date( );
	    SimpleDateFormat ft1 =new SimpleDateFormat ("yyyy-MM-dd");
	    SimpleDateFormat ft2 =new SimpleDateFormat ("hh:mm:ss");
	    
	  
	    Sales sales = new Sales();
	    
	    sales.setItemid(itemid);
	    sales.setItemname(itemname);
	    sales.setQuantity(qty);
	    sales.setDiscount(disc);
	    sales.setInvoicenumber(invoiceid);
	    sales.setPurchasedate(ft1.format(time1));
	    sales.setPurchasetime(ft2.format(time1));
	    sales.setTotalsalesprice(total_sale);
	    sales.setSalesprice(sales_price);
	    sales.setItem_id(id);
	    
	    System.out.println("Create Sale Item ID --------> > > > > > " + id);

	    
	    System.out.println("Item ID : " + itemid);
	    
	    HttpSession session = hsr.getSession();
	    String username = session.getAttribute("username").toString();
	    System.out.println("Sales Create User : " + username);
	    Activity activity = new Activity();
	    activity.setLogdate(ft1.format(time1));
	    activity.setLogtime(ft2.format(time1));
	    activity.setActivity("Proceed "+invoiceid+" with "+itemname+" and "+total_sale+" total Sale" );
	    activity.setAction("CREATE");
	    activity.setUser(username);
	    
	    activityService.createActivity(activity);

	    salesService.createSales(sales);
	    
	    
	    int quantity = itemService.getItem(id).getQuantity();
		
	    int stockqty = quantity - qty;
	    
		System.out.println("Sales Controller  itemService getItem().getQuantity() ------>  " + quantity);
		System.out.println("After Reduce Stock Balance Quantity ------>  " + stockqty);

		
		Item item = new Item();
		
		item.setId(id);
		item.setItemid(itemid);
		item.setItemname(itemname);
		item.setUnitprice(unitprice);
		item.setSalesprice(salesp);
		item.setQuantity(stockqty);
		item.setCategory(category);
		item.setReorderlevel(reorderlevel);
		
		
		itemService.updateItem(item);
		
		System.out.println("After Update Item Tables  ------>  ");
		
		return new ModelAndView("redirect:index.htm");
	}
	
	
	@RequestMapping(value={"saveSales"})
	public ModelAndView saveSales(@ModelAttribute Sales sales,HttpServletRequest hsr){
		logger.info("Saving the Sales. Data :  "+ sales);
		
		if(sales.getId() == 0){
			logger.info("createSales createSales createSales :  ");
			salesService.createSales(sales);
			
		}
		else{
			logger.info("updateSales updateSales updateSales :  ");
			
			HttpSession session = hsr.getSession();
		    String username = session.getAttribute("username").toString();
		    System.out.println("Sales Create User : " + username);
		    Activity activity = new Activity();
		    Date time1 = new Date( );
		    SimpleDateFormat ft1 =new SimpleDateFormat ("yyyy-MM-dd");
		    SimpleDateFormat ft2 =new SimpleDateFormat ("hh:mm:ss");
		    activity.setLogdate(ft1.format(time1));
		    activity.setLogtime(ft2.format(time1));
		    activity.setActivity("Edit invoice no "+sales.getInvoicenumber());
		    activity.setAction("EDIT");
		    activity.setUser(username);
		    
		    activityService.createActivity(activity);
			
			salesService.updateSales(sales);
		}
		
		
		
		return new ModelAndView("redirect:index.htm");
	}
	
	@RequestMapping(value={"editSales"})
	public ModelAndView editSales(@RequestParam long id, @ModelAttribute Sales sales,HttpServletRequest hsr){
		logger.info("Updating the Sales for the Id "+id);
		HttpSession session = hsr.getSession();
		if(session.getAttribute("role").equals("Admin")){
		sales = salesService.getSales(id);
		return new ModelAndView("add_new_sales","sales",sales);
		}
		else{
			return new ModelAndView("redirect:sales.htm");
		}
	}
	
	@RequestMapping(value={"deleteSales"})
	public ModelAndView deleteSales(@RequestParam long id,HttpServletRequest hsr){
		logger.info("Deleting the Sales. Id : "+id);
		HttpSession session = hsr.getSession();
		if(session.getAttribute("role").equals("Admin")){
			
		
		
		Sales sale = salesService.getSales(id);
		
	    String username = session.getAttribute("username").toString();
	    System.out.println("Sales Delete: " + username);
	    Activity activity = new Activity();
	    Date time1 = new Date( );
	    SimpleDateFormat ft1 =new SimpleDateFormat ("yyyy-MM-dd");
	    SimpleDateFormat ft2 =new SimpleDateFormat ("hh:mm:ss");
	    activity.setLogdate(ft1.format(time1));
	    activity.setLogtime(ft2.format(time1));
	    activity.setActivity("DELETE Sale invoice no :  "+sale.getInvoicenumber() + " and itemname : " + sale.getItemname());
	    activity.setAction("DELETE");
	    activity.setUser(username); 
	    activityService.createActivity(activity);
		
	    salesService.deleteSales(id);
	    
	    System.out.println("Delete Sale--------> > > > > > > > > > > > > > ");
		
		return new ModelAndView("redirect:sales.htm");
		}
		else{
			return new ModelAndView("redirect:sales.htm");
		}
	}
	
	@RequestMapping(value={"sales.htm"})
	public ModelAndView getAllSales(HttpServletRequest hsr){
		
		HttpSession session = hsr.getSession();
		logger.info("Getting the all Sales.");
		List<Sales> saleslist = salesService.getAllSales();
		
		
		
		if(session.getAttribute("username") == null){
			return new ModelAndView("redirect:login.htm");
		}
		else{
			
			return new ModelAndView("sales","saleslist",saleslist);
		}
	}
	
	
	@RequestMapping(value = { "deletequantityfromitems2" })
	public ModelAndView delete_quantity_from_items(HttpServletRequest hsr){
		HttpSession session = hsr.getSession();
		String itemid = hsr.getParameter("itemidid");
		String quantity = hsr.getParameter("quantity");
		
		long id = Long.parseLong(itemid);
		int quantitys = Integer.parseInt(quantity);
		
		System.out.println("deletequantityfromitems2 itemid ----> " + itemid);
		System.out.println("deletequantityfromitems2 quantity -----> " + quantity);
		
		Item item = itemService.getItem(id);
		int qty = item.getQuantity();
		System.out.println("deletequantityfromitems2 qty -----> " + qty);
		int remainqty = qty + quantitys;
		System.out.println("deletequantityfromitems2 remainqty -----> " + remainqty);
		
		item.setQuantity(remainqty);
		itemService.updateItem(item);
		
		return new ModelAndView("redirect:load_supplier_stocks.htm");
	}
	
	
	@RequestMapping(value = { "updateinvoiceforsales" })
	public ModelAndView updateinvoiceforsales(HttpServletRequest hsr){
		HttpSession session = hsr.getSession();
		String itemid = hsr.getParameter("itemidid");
		String quantity = hsr.getParameter("quantity");
		String invoicenumber = hsr.getParameter("invoicenumber");
		String salesprice = hsr.getParameter("salesprice");
		String discount = hsr.getParameter("discount");
		String totalsalesprice = hsr.getParameter("totalsalesprice");
		
		long item_id = Long.parseLong(itemid);
		
		Item item = itemService.getItem(item_id);
		
		double item_unitprice = item.getUnitprice();
		double item_salesprice = item.getSalesprice();
		
		int current_discount = Integer.parseInt(discount);
		
		
		double dl_totalsalesprice = Double.parseDouble(totalsalesprice);
		int qty = Integer.parseInt(quantity);
		
		List<Invoice> invoicelist = invoiceService.getAllInvoices(invoicenumber);
		System.out.println("invoicelist ---> " + invoicelist.get(0).getId());
		long invoice_id = invoicelist.get(0).getId();
		
		Invoice invoice = invoiceService.getInvoice(invoice_id);
		
		double current_totalsalesprice = invoice.getTotalsalesprice();
		double current_totalunitsprice = invoice.getTotalunitprice();
		int current_totaldiscount = invoice.getTotaldiscount();
		double current_profit = invoice.getProfit();
		
		System.out.println("current_totalsalesprice : " + current_totalsalesprice);
		//long invoice_id = invoicelist.
		System.out.println("current_totalunitsprice : " + current_totalunitsprice);
		System.out.println("current_totaldiscount : " + current_totaldiscount);
		System.out.println("current_profit : " + current_profit);
		System.out.println("Sales quantity : " + qty);
		System.out.println("Sales current_discount : " + current_discount);
		
		System.out.println("item_unitprice  : " + item_unitprice);
		System.out.println("item_salesprice : " + item_salesprice);

		double total_unitprice = item_unitprice * qty;
		double total_salesprice = item_salesprice * qty;
		double discount_price = (total_salesprice * current_discount)/100;
		double total_sale_sprice = total_salesprice - discount_price;
		
		System.out.println("total_unitprice : " + total_unitprice);
		System.out.println("total_salesprice : " + total_salesprice);
		System.out.println("discount_price : " + discount_price);
		System.out.println("total_sale_sprice : " + total_sale_sprice);
		
		double total_discount_price = (total_sale_sprice *current_totaldiscount)/100;
		double total_reduce_salesprice = total_sale_sprice - total_discount_price;
		double reduce_totalsaleprice = current_totalsalesprice - total_reduce_salesprice;
		
		System.out.println("total_discount_price : " + total_discount_price);
		System.out.println("total_reduce_salesprice : " + total_reduce_salesprice);
		System.out.println("reduce_totalsaleprice : " + reduce_totalsaleprice);
		
		double reduce_total_unit_price = current_totalunitsprice - total_unitprice;
		
		System.out.println("reduce_total_unit_price : " + reduce_total_unit_price);
		
		double avilable_profit = (double) (reduce_totalsaleprice - reduce_total_unit_price);
		
		if(reduce_totalsaleprice == 0 && reduce_total_unit_price == 0 && avilable_profit == 0){
			
			invoiceService.deleteInvoice(invoice_id);
		}
		else{
		
		invoice.setTotalsalesprice(reduce_totalsaleprice);
		invoice.setTotalunitprice(reduce_total_unit_price);
		
		
		System.out.println("avilable_profit : " + avilable_profit);
		
		invoice.setProfit(avilable_profit);
		
		invoiceService.updateInvoice(invoice);
		}
		
		return new ModelAndView("redirect:load_supplier_stocks.htm");
	}
	
	
	
}
