package com.saminda.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.saminda.entity.Stock;
import com.saminda.service.StockService;

@Controller
public class StockController {

	
	private static final Logger logger = Logger.getLogger(StockController.class);
	
	public StockController() {
		System.out.println("StockController()");
	}
	
	@Autowired
	private StockService stockService;
	
	@RequestMapping(value={"getstockpage.htm"})
	public ModelAndView getStockInsert(HttpServletRequest hsr){
		System.out.println("Stock insert load.. 999999999999999999999999999999999");
		
		String itemid = hsr.getParameter("itemid");
		String itemname = hsr.getParameter("itemname");
		String category = hsr.getParameter("category");
		String unitprice_string = hsr.getParameter("unitprice");
		String salesprice_string = hsr.getParameter("salesprice");
		String quantity_string = hsr.getParameter("quantity");
		String reorderlevel_string = hsr.getParameter("reorderlevel");
		
		
		System.out.println("stock itemid "+ itemid);
		System.out.println("stock itemname "+ itemname);
		System.out.println("stock category "+ category);
		System.out.println("stock quantity "+ quantity_string);
		
		
		int quantity = Integer.parseInt(quantity_string);
		int reorderlevel = Integer.parseInt(reorderlevel_string);
		double unitprice = Double.parseDouble(unitprice_string);
		double salesprice = Double.parseDouble(salesprice_string);
		
		Date time1 = new Date( );
	    SimpleDateFormat ft1 =new SimpleDateFormat ("yyyy-MM-dd");
	    SimpleDateFormat ft2 =new SimpleDateFormat ("hh:mm:ss");
	    
	    Stock stock = new Stock();
	    
	    stock.setItemid(itemid);
	    stock.setItemname(itemname);
	    stock.setCategory(category);
	    stock.setSalesprice(salesprice);
	    stock.setUnitprice(unitprice);
	    stock.setQuantity(quantity);
	    stock.setReorderlevel(reorderlevel);
	    stock.setUpdate_date(ft1.format(time1));
	    stock.setUpdate_time(ft2.format(time1));
	    
	    stockService.CreateStock(stock);
		
		return new ModelAndView("redirect:items.htm");
	}
	
	@RequestMapping(value={"rpt_stock_updates.htm"})
	public ModelAndView getAllStockUpdates(HttpServletRequest hsr){
		List<Stock> stocklist = stockService.getAllStock();
		
		System.out.println("rpt_stock_updates.htm rpt_stock_updates.htm :::::::: " + stocklist);
		return new ModelAndView("rpt_stock_updates","stocklist",stocklist);
	}
	
}
