package com.saminda.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.saminda.entity.Activity;
import com.saminda.entity.Item;
import com.saminda.entity.Stock;
import com.saminda.entity.SupplierStocks;
import com.saminda.entity.Suppliers;
import com.saminda.service.ActivityService;
import com.saminda.service.ItemService;
import com.saminda.service.SupplierStocksService;
import com.saminda.service.SuppliersService;

@Controller
public class SupplierStocksController {

private static final Logger logger = Logger.getLogger(ItemController.class);
	
	public SupplierStocksController() {
		System.out.println("SupplierStocksController()");
	}
	
	@Autowired
	private SupplierStocksService supplierStocksService;
	
	@Autowired
	private ItemService itemService;
	
	@Autowired
	private SuppliersService suppliersService;
	
	@Autowired
	private ActivityService activityService;
	
	
	@RequestMapping(value = {"load_supplier_stocks.htm"})
	public ModelAndView getSupplierStocksPage(){
		
		System.out.println("load_supplier_stocks.htm controller load -----> ");
		List<SupplierStocks> supplierStocks = supplierStocksService.getAllSupplierStocks();
		
		return new ModelAndView("load_supplier_stocks","supplierStocks",supplierStocks);
		//return new ModelAndView("load_supplier_stocks");
		
	}
	
	
	@RequestMapping(value = {"add_new_supplier_stock.htm"})
	public ModelAndView addNewSupplierStocks(HttpServletRequest hsr,@ModelAttribute Stock stock){
		
		Map<String, Object> myModel = new HashMap<String, Object>();
		
		List<Item> stocklist  = itemService.getAllItem();
		List<Suppliers> supplierslist = suppliersService.getAllSuppliers();
		
		myModel.put("stocklist", stocklist);
		myModel.put("supplierslist", supplierslist);
		
		return new ModelAndView("add_new_supplier_stock","myModel",myModel);
		
	}
	
	
	
	@RequestMapping(value = {"create_supplier_item.htm"})
	public ModelAndView createSupplierItem(HttpServletRequest hsr,@ModelAttribute Stock stock){
		
		Map<String, Object> myModel = new HashMap<String, Object>();
		
		
		List<Suppliers> supplierslist = suppliersService.getAllSuppliers();
		
	
		myModel.put("supplierslist", supplierslist);
		
		return new ModelAndView("create_supplier_item","myModel",myModel);
		
	}
	
	
	@RequestMapping(value = {"saveSupplierStocks"})
	public ModelAndView saveSupplierStock(HttpServletRequest hsr){
		
		System.out.println("saveSupplierStocks open-------");
		HttpSession session = hsr.getSession();
		try{
			String id = hsr.getParameter("id");
			String itemname = hsr.getParameter("itemname");
			String supplier_name = hsr.getParameter("supplier_name");
			String date = hsr.getParameter("date");
			String quantity = hsr.getParameter("quantity");
			String itemidid = hsr.getParameter("itemidid");
			int item_id = Integer.parseInt(itemidid);
			
			
			int qty = Integer.parseInt(quantity);
			
			
			System.out.println("id ---> " + id);
			System.out.println("itemname ---> " + itemname);
			System.out.println("supplier_name ---> " + supplier_name);
			System.out.println("date ---> " + date);
			System.out.println("quantity ---> " + qty);
			
			if (id == "" || id == "null" || id == null) {
				System.out.println("Supplier Stock Created ");
				
				SupplierStocks supplierStocks = new SupplierStocks();
				supplierStocks.setSupplier_name(supplier_name);
				supplierStocks.setItemname(itemname);
				supplierStocks.setDate(date);
				supplierStocks.setQuantity(qty);
				supplierStocks.setItem_id(item_id);
				
				
				String username = session.getAttribute("username").toString();
				Activity activity = new Activity();
				Date time1 = new Date();
				SimpleDateFormat ft1 = new SimpleDateFormat("yyyy-MM-dd");
				SimpleDateFormat ft2 = new SimpleDateFormat("hh:mm:ss");
				activity.setLogdate(ft1.format(time1));
				activity.setLogtime(ft2.format(time1));
				activity.setActivity("Create Supplier Stock from " + supplier_name+ " in " + itemname +" & qty " + qty);
				activity.setAction("CREATE");
				activity.setUser(username);
				activityService.createActivity(activity);
				
				
				
				supplierStocksService.createSupplierStocks(supplierStocks);
				
				return new ModelAndView("redirect:index.htm");
			}
			else{
				System.out.println("Supplier Stock Update ");
				
				SupplierStocks supplierStocks = new SupplierStocks();
				Long id22 = Long.parseLong(id);
				supplierStocks.setId(id22);
				supplierStocks.setSupplier_name(supplier_name);
				supplierStocks.setItemname(itemname);
				supplierStocks.setDate(date);
				supplierStocks.setQuantity(qty);
				supplierStocks.setItem_id(item_id);
				
				
				String username = session.getAttribute("username").toString();
				Activity activity = new Activity();
				Date time1 = new Date();
				SimpleDateFormat ft1 = new SimpleDateFormat("yyyy-MM-dd");
				SimpleDateFormat ft2 = new SimpleDateFormat("hh:mm:ss");
				activity.setLogdate(ft1.format(time1));
				activity.setLogtime(ft2.format(time1));
				activity.setActivity("EDIT Supplier Stock from " + supplier_name+ " in " + itemname +" & qty " + qty);
				activity.setAction("EDIT");
				activity.setUser(username);
				activityService.createActivity(activity);
				
				
				supplierStocksService.updateSupplierStocks(supplierStocks);
				
				return new ModelAndView("redirect:index.htm");
				
			}
			
			
			
			
		}
		catch(Exception e){
			System.out.println("Error === : " + e);
			return new ModelAndView("redirect:index.htm");
		}
		
		
		
	}
	
	@RequestMapping(value = { "editSupplierStock" })
	public ModelAndView editSupplierStocks(@RequestParam long id,@ModelAttribute SupplierStocks supplierStocks,HttpServletRequest hsr) {
		Map<String, Object> myModel = new HashMap<String, Object>();
	
		System.out.println("editSupplierStock open .>>>>>>> ");
		
		System.out.println("Edit Supplier Stock id ----> " + id);
		HttpSession session = hsr.getSession();
		System.out.println("session.getAttribute role : "
				+ session.getAttribute("role"));
		if (session.getAttribute("role").equals("Admin") || session.getAttribute("role").equals("User")) {
			logger.info("Updating the Item for the Id " + id);
			
			supplierStocks = supplierStocksService.getSupplierStock(id);
			
			List<Item> stocklist  = itemService.getAllItem();
			List<Suppliers> supplierslist = suppliersService.getAllSuppliers();
		
			
			long idss = supplierStocks.getId();
			String sup_name = supplierStocks.getSupplier_name();
			String item_name = supplierStocks.getItemname();
			String date = supplierStocks.getDate();
			int quantity = supplierStocks.getQuantity();
			int item_id = supplierStocks.getItem_id();
			
			System.out.println("sup_name  " + sup_name +"  item_name : " + item_name +" date :  "+date+ "  quantity : "+ quantity);
			
			myModel.put("idss", idss);
			myModel.put("sup_name", sup_name);
			myModel.put("item_name", item_name);
			myModel.put("date", date);
			myModel.put("quantity", quantity);
			myModel.put("item_id", item_id);
			
			myModel.put("stocklist", stocklist);
			myModel.put("supplierslist", supplierslist);
			
			System.out.println("supplierStocks -----> > > > " + supplierStocks);

			
			return new ModelAndView("add_new_supplier_stock","myModel",myModel);
		}
		else{
			return new ModelAndView("#");
		}
	}
	
	@RequestMapping(value = { "deleteSupplierStock" })
	public ModelAndView deleteSupplierStocks(@RequestParam long id, HttpServletRequest hsr) {
		logger.info("Deleting the Supplier Stock. Id : " + id);
		HttpSession session = hsr.getSession();
	
		if (session.getAttribute("role").equals("Admin")) {
			//SupplierStocks supplierStocks = supplierStocksService.getSupplierStock(id);
			
			SupplierStocks supplystock = supplierStocksService.getSupplierStock(id);
			String supplyname = supplystock.getSupplier_name();
			String supplydate = supplystock.getDate();
			String itemname = supplystock.getItemname();
			
			
			String username = session.getAttribute("username").toString();
			Activity activity = new Activity();
			Date time1 = new Date();
			SimpleDateFormat ft1 = new SimpleDateFormat("yyyy-MM-dd");
			SimpleDateFormat ft2 = new SimpleDateFormat("hh:mm:ss");
			activity.setLogdate(ft1.format(time1));
			activity.setLogtime(ft2.format(time1));
			activity.setActivity("DELETE Supplier Stock " + supplydate+ "  " + supplyname +" & " + itemname);
			activity.setAction("DELETE");
			activity.setUser(username);
			activityService.createActivity(activity);
			
			
			supplierStocksService.deleteSupplierStocks(id);
			
			return new ModelAndView("redirect:load_supplier_stocks.htm");
		}else{
			return new ModelAndView("redirect:load_supplier_stocks.htm");
		}
	
	}
	
	@RequestMapping(value = { "updateStock" })
	public ModelAndView updateStockItem(HttpServletRequest hsr){
		
		System.out.println("updateStock ---->>> updateStock ---->> ");
		HttpSession session = hsr.getSession();
		String itemid = hsr.getParameter("itemidid");
		String quantity = hsr.getParameter("quantity");
		String updequantityid = hsr.getParameter("updequantityid");
		int update_quantity = Integer.parseInt(updequantityid) + 0;
		
		
		long id = Long.parseLong(itemid);
		int quantitys = Integer.parseInt(quantity);
		
		System.out.println("updateStock Item id : " + itemid +" and Quantity : " + quantity);
		
		Item item = itemService.getItem(id);
		int qty = item.getQuantity();
		int totalqty = quantitys + qty - update_quantity;
		String itemname = item.getItemname();
		
		System.out.println("updateStock Toatl Quantity " + itemname + " ---- > " +  totalqty);
		item.setQuantity(totalqty);
		itemService.updateItem(item);
		
		return new ModelAndView("redirect:load_supplier_stocks.htm");
	}
	
	
	@RequestMapping(value = { "deletequantityfromitems" })
	public ModelAndView delete_quantity_from_item(HttpServletRequest hsr){
		HttpSession session = hsr.getSession();
		String itemid = hsr.getParameter("itemidid");
		String quantity = hsr.getParameter("quantity");
		
		long id = Long.parseLong(itemid);
		int quantitys = Integer.parseInt(quantity);
		
		System.out.println("deletequantityfromitems itemid ----> " + itemid);
		System.out.println("deletequantityfromitems quantity -----> " + quantity);
		
		Item item = itemService.getItem(id);
		int qty = item.getQuantity();
		System.out.println("deletequantityfromitems qty -----> " + qty);
		int remainqty = qty - quantitys;
		System.out.println("deletequantityfromitems remainqty -----> " + remainqty);
		
		item.setQuantity(remainqty);
		itemService.updateItem(item);
		
		return new ModelAndView("redirect:load_supplier_stocks.htm");
	}
	
	@RequestMapping(value = { "supplier_reports.htm" })
	public ModelAndView getSupplierReports(){
		
		return new ModelAndView("supplier_reports");
		
	}
	
	@RequestMapping(value = { "rpt_supplier_suppliers.htm" })
	public ModelAndView getSupplier_supplierReports(){
		
		List<Suppliers> supplierslist = suppliersService.getAllSuppliers();
		
		return new ModelAndView("rpt_supplier_suppliers","supplierslist",supplierslist);
		
		
	}
	

	@RequestMapping(value = { "rpt_supplier_overall_stock.htm" })
	public ModelAndView getOverallSupplierStock(){
		
		List<SupplierStocks> supplierStocks = supplierStocksService.getAllSupplierStocks();
		
		return new ModelAndView("rpt_supplier_overall_stock","supplierStocks",supplierStocks);
		
		
	}
	
	@RequestMapping(value = { "rpt_supplier_stock_by_date.htm" })
	public ModelAndView getSupplierStockByDate(HttpServletRequest hsr){
		
		String date = hsr.getParameter("mdate");
		
		List<SupplierStocks> supplierStocks = supplierStocksService.getSupplierStocksByDate(date);
		
		return new ModelAndView("rpt_supplier_stock_by_date","supplierStocks",supplierStocks);
	}
	
	@RequestMapping(value = { "rpt_supplier_stock_by_date_range.htm" })
	public ModelAndView getSupplierStockByDateRange(HttpServletRequest hsr){
		
		String from_date = hsr.getParameter("mdate");
		String to_date = hsr.getParameter("mdate2");
		
		List<SupplierStocks> supplierStocks = supplierStocksService.getSupplierStocksByDateRange(from_date, to_date);
		
		return new ModelAndView("rpt_supplier_stock_by_date_range","supplierStocks",supplierStocks);
	}
	
	
	@RequestMapping(value = { "rpt_supplier_stock_by_supplier_name.htm" })
	public ModelAndView getSupplierStocksByISupplier(HttpServletRequest hsr){
		String suppliername = hsr.getParameter("suppliername");
		Map<String, Object> myModel = new HashMap<String, Object>();
		
		List<Suppliers> supplier_list = suppliersService.getAllSuppliers();
		
		List<SupplierStocks> supplierstocks_list = supplierStocksService.getSupplierStocksBySupplier(suppliername);
		myModel.put("supplier_list", supplier_list);
		myModel.put("supplierstocks_list", supplierstocks_list);
		
		return new ModelAndView("rpt_supplier_stock_by_supplier_name","myModel",myModel);
		
	}
	
	@RequestMapping(value = { "rpt_supplier_stock_by_itemname.htm" })
	public ModelAndView getSupplierStocksByItemname(HttpServletRequest hsr){
		
		String itemname = hsr.getParameter("itemname");
		Map<String, Object> myModel = new HashMap<String, Object>();
		
		List<Item> itemlist = itemService.getAllItem();
		
		List<SupplierStocks> supplierstocklist = supplierStocksService.getSupplierStocksByItemName(itemname);
		
		myModel.put("itemlist", itemlist);
		myModel.put("supplierstocklist", supplierstocklist);
		
		return new ModelAndView("rpt_supplier_stock_by_itemname","myModel",myModel);
		
	}
	
	
	@RequestMapping(value = {"createsupplieritemstock"})
	public ModelAndView CreateSupplierItemStock(HttpServletRequest hsr){
		
		System.out.println("CreateSupplierItemStock open-------");
		HttpSession session = hsr.getSession();
		
		String suplliername = hsr.getParameter("suplliername");
		String itemname = hsr.getParameter("itemname");
		String quantity = hsr.getParameter("quantity");
		String date = hsr.getParameter("date");
		
		int qty = Integer.parseInt(quantity);
		
		SupplierStocks stock = new SupplierStocks();
		stock.setItemname(itemname);
		stock.setSupplier_name(suplliername);
		stock.setDate(date);
		stock.setQuantity(qty);
		
		
		 supplierStocksService.createSupplierStocks(stock);
			
	    return new ModelAndView("redirect:index.htm");
			
	}
	
	
	
	
}
