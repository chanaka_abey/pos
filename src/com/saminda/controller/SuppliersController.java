package com.saminda.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.saminda.entity.Activity;
import com.saminda.entity.Sales;
import com.saminda.entity.Suppliers;
import com.saminda.entity.User;
import com.saminda.service.ActivityService;
import com.saminda.service.SuppliersService;


@Controller
public class SuppliersController {
	
	private static final Logger logger = Logger.getLogger(SuppliersController.class);

	public SuppliersController() {
		System.out.println("SuppliersController()");
	}
	
	@Autowired
	private SuppliersService suppliersService;
	
	@Autowired
	private ActivityService activityService;
	
	
	@RequestMapping(value = { "suppliers.htm" })
	public ModelAndView getSupplierspage(@ModelAttribute Suppliers supplier){
		
		List<Suppliers> supplierslist = suppliersService.getAllSuppliers();
		
		return new ModelAndView("suppliers","supplierslist",supplierslist);
	}
	
	@RequestMapping(value = { "add_new_supplier.htm" })
	public ModelAndView addnewSupplier(@ModelAttribute Suppliers suppliers){
		
		return new ModelAndView("add_new_supplier");
	}
	
	@RequestMapping(value={"saveSupplier"})
	public ModelAndView saveSupplier(@ModelAttribute Suppliers suppliers,HttpServletRequest hsr){
		HttpSession session = hsr.getSession();
		
		if(suppliers.getId() == 0){
			logger.info("createSales createSales createSales :  ");
			
			
			String username = session.getAttribute("username").toString();
			Activity activity = new Activity();
			Date time1 = new Date();
			SimpleDateFormat ft1 = new SimpleDateFormat("yyyy-MM-dd");
			SimpleDateFormat ft2 = new SimpleDateFormat("hh:mm:ss");
			activity.setLogdate(ft1.format(time1));
			activity.setLogtime(ft2.format(time1));
			activity.setActivity("CREATE Supplier " + suppliers.getName()+ " in  " + suppliers.getAddress());
			activity.setAction("CREATE");
			activity.setUser(username);
			activityService.createActivity(activity);
			
			
			suppliersService.createSupplier(suppliers);
			
		}
		else{
			
			
			String username = session.getAttribute("username").toString();
			Activity activity = new Activity();
			Date time1 = new Date();
			SimpleDateFormat ft1 = new SimpleDateFormat("yyyy-MM-dd");
			SimpleDateFormat ft2 = new SimpleDateFormat("hh:mm:ss");
			activity.setLogdate(ft1.format(time1));
			activity.setLogtime(ft2.format(time1));
			activity.setActivity("EDIT Supplier " + suppliers.getName()+ " in  " + suppliers.getAddress());
			activity.setAction("EDIT");
			activity.setUser(username);
			activityService.createActivity(activity);
			
			suppliersService.updateSuppliers(suppliers);
		}
		
		
		
		return new ModelAndView("redirect:index.htm");
	}
	
	@RequestMapping(value={"editSupplier"})
	public ModelAndView editSupplier(@RequestParam long id, @ModelAttribute Suppliers suppliers,HttpServletRequest hsr){
		
		HttpSession session = hsr.getSession();
		if(session.getAttribute("role").equals("Admin") || session.getAttribute("role").equals("User")){
			suppliers = suppliersService.getSupplier(id);
		return new ModelAndView("add_new_supplier","suppliers",suppliers);
		}
		else{
			return new ModelAndView("#");
		}
	}
	
	@RequestMapping(value={"deleteSupplier"})
	public ModelAndView deleteSupplier(@RequestParam long id,HttpServletRequest hsr){
		logger.info("Deleting the Sales. Id : "+id);
		HttpSession session = hsr.getSession();
		if(session.getAttribute("role").equals("Admin")){
			
			
			String username = session.getAttribute("username").toString();
			Suppliers supply = suppliersService.getSupplier(id);
			String name = supply.getName();
			
			
			Activity activity = new Activity();
			Date time1 = new Date();
			SimpleDateFormat ft1 = new SimpleDateFormat("yyyy-MM-dd");
			SimpleDateFormat ft2 = new SimpleDateFormat("hh:mm:ss");
			activity.setLogdate(ft1.format(time1));
			activity.setLogtime(ft2.format(time1));
			activity.setActivity("DELETE Supplier " + name);
			activity.setAction("DELETE");
			activity.setUser(username);
			activityService.createActivity(activity);
			
			suppliersService.deleteSupplier(id);
		return new ModelAndView("redirect:suppliers.htm");
		}
		else{
			return new ModelAndView("redirect:suppliers.htm");
		}
		
		
	}
	

}
