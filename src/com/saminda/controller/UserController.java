package com.saminda.controller;


import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;




import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;




import com.saminda.entity.Activity;
import com.saminda.entity.User;
import com.saminda.service.ActivityService;
import com.saminda.service.UserService;


@Controller
public class UserController {

	private static final Logger logger = Logger.getLogger(InvoiceController.class);
	
	public UserController(){
		System.out.println("UserController()");
	}
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private ActivityService activityService;
	
	@RequestMapping(value={"user.htm"})
	public ModelAndView getAllUser(HttpServletRequest hsr){
		
		HttpSession session = hsr.getSession();
		List<User> userlist = userService.getAllUsers();
		if(session.getAttribute("username") == null){
			System.out.println("userrole is null");
			return new ModelAndView("redirect:login.htm");
		}
		else if((session.getAttribute("role").equals("User"))){
			System.out.println("userrole is user");
			return new ModelAndView("index.htm");
		}
		else{
			System.out.println("userrole is admin");
			return new ModelAndView("user","userlist",userlist);
		}
	}
	
	@RequestMapping(value={"add_new_user.htm"})
	public ModelAndView addNewUser(@ModelAttribute User user, HttpServletRequest hsr){
	HttpSession session = hsr.getSession();
		
		if(session.getAttribute("role").equals("Admin")){
		return new ModelAndView("add_new_user");
		}else{
			return new ModelAndView("#");
		}
	}
	
	@RequestMapping(value={"saveUser"})
	public ModelAndView saveUser(@ModelAttribute User user,HttpServletRequest hsr){
		logger.info("Saving the User. Data :  "+ user);
		
		System.out.println("Saving the User -------------> " + user.getUsername());
		System.out.println("Saving the Password -------------> " + user.getPassword());
		System.out.println("Saving the Role -------------> " + user.getRole());
		
		//User user2 = new User();
		//user2.setUsername(user.getUsername());
		//user2.setPassword(user.getPassword());
		//user2.setRole(user.getRole());
			HttpSession session = hsr.getSession();
			
			String username = session.getAttribute("username").toString();
		    System.out.println("Sales Create User : " + username);
		    Activity activity = new Activity();
		    Date time1 = new Date( );
		    SimpleDateFormat ft1 =new SimpleDateFormat ("yyyy-MM-dd");
		    SimpleDateFormat ft2 =new SimpleDateFormat ("hh:mm:ss");
		    activity.setLogdate(ft1.format(time1));
		    activity.setLogtime(ft2.format(time1));
		    activity.setActivity("Create new user "+user.getUsername());
		    activity.setAction("CREATE");
		    activity.setUser(username);
		    activityService.createActivity(activity);
			
			
			logger.info("createUser createUser createUser :  ");
			userService.createUser(user);

		
		return new ModelAndView("redirect:index.htm");
	}
	
	@RequestMapping(value={"editUser"})
	public ModelAndView editUser(@RequestParam String username, @ModelAttribute User user,HttpServletRequest hsr){
		
		HttpSession session = hsr.getSession();
		
		if(session.getAttribute("role").equals("Admin") || session.getAttribute("role").equals("User")){
			logger.info("Updating the Item for the Id "+username);
			user = userService.getUser(username);
			
			return new ModelAndView("update_user","user",user);
			
		}
		else{
			return new ModelAndView("redirect:index.htm");
		}
	}
	
	@RequestMapping(value={"updateUser"})
	public ModelAndView updateUser(@ModelAttribute User user,HttpServletRequest hsr){
		
		HttpSession session = hsr.getSession();
		String username = session.getAttribute("username").toString();
	    System.out.println("Sales Create User : " + username);
	    Activity activity = new Activity();
	    Date time1 = new Date( );
	    SimpleDateFormat ft1 =new SimpleDateFormat ("yyyy-MM-dd");
	    SimpleDateFormat ft2 =new SimpleDateFormat ("hh:mm:ss");
	    activity.setLogdate(ft1.format(time1));
	    activity.setLogtime(ft2.format(time1));
	    activity.setActivity("Edit user "+user.getUsername());
	    activity.setAction("EDIT");
	    activity.setUser(username);
	    activityService.createActivity(activity);
		
		logger.info("updateUser updateUser updateUser :  ");
		userService.updateUser(user);
		
		return new ModelAndView("redirect:index.htm");
	}
	
	@RequestMapping(value={"deleteUser"})
	public ModelAndView deleteUsersddd(@RequestParam String username,HttpServletRequest hsr){
		System.out.println("deleteUser  deleteUser deleteUser deleteUser deleteUser");
		
		HttpSession session = hsr.getSession();
		
		//String username22 = session.getAttribute("username").toString();
		//System.out.println("Delete Username : " + username22);
		
		if(session.getAttribute("role").equals("Admin")){
			logger.info("Updating the Item for the Id "+username);
			
			User user2 = userService.getUser(username);
			
			String username2 = session.getAttribute("username").toString();
		    System.out.println("Delete User ----> : " + username);
		    Activity activity = new Activity();
		    Date time1 = new Date( );
		    SimpleDateFormat ft1 =new SimpleDateFormat ("yyyy-MM-dd");
		    SimpleDateFormat ft2 =new SimpleDateFormat ("hh:mm:ss");
		    activity.setLogdate(ft1.format(time1));
		    activity.setLogtime(ft2.format(time1));
		    activity.setActivity("Delete user "+user2.getUsername());
		    activity.setAction("DELETE");
		    activity.setUser(username2);
		    activityService.createActivity(activity);
			
			userService.deleteUser(username);
			
			return new ModelAndView("redirect:user.htm");
			
		}
		else{
			return new ModelAndView("redirect:user.htm");
		}
		
	}
	
	
}
