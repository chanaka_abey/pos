package com.saminda.dao;

import java.util.List;

import com.saminda.entity.Activity;

public interface ActivityDAO {
	
	public long createActivity(Activity activity);
	
	public List<Activity> getAllActivies();
	
	public List<Activity> getAllActions();
	
	public List<Activity> getAllActiviesByDate(String logdate);
	
	public List<Activity> getAllActiviesByDateRange(String from_date, String to_date);
	
	public List<Activity> getAllActiviesByAction(String action);
	
	public List<Activity> getAllActiviesByUser(String user);

}
