package com.saminda.dao;

import java.util.List;

import com.saminda.entity.Item;

public interface ItemDAO {
	
	public long createItem(Item item);
	
	public Item updateItem(Item item);
	
	public void deleteItem(long id);
	
	public List<Item> getAllItem();
	
	public List<Item> getAllItemBelowReorderlevel();
	
	public Item getItem(long id);
	
	public Item getItembyItemID(String itemid);
	
	public List<Item> getAllItemByName(String itemname);
	
	

}
