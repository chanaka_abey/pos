package com.saminda.dao;

import java.util.List;

import com.saminda.entity.Stock;

public interface StockDAO {

	public long CreateStock(Stock stock);
	
	public List<Stock> getAllStock();
	
	public List<Stock> getStockById(long id);
	
	public List<Stock> getStockByItemId(String itemid);
	
	public List<Stock> getStockByItemName(String itemname);
	
	public List<Stock> getStockByDate(String update_date);
	
	public List<Stock> getStockByDateRange(String from_date, String to_date);
	
	public List<Stock> getStockByTime(String update_time);
	
}
