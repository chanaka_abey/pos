package com.saminda.dao;

import java.util.List;

import com.saminda.entity.SupplierStocks;

public interface SupplierStocksDAO {

	public long createSupplierStocks(SupplierStocks supplierStocks);

	public SupplierStocks updateSupplierStocks(SupplierStocks supplierStocks);

	public void deleteSupplierStocks(long id);

	public List<SupplierStocks> getAllSupplierStocks();
	
	public SupplierStocks getSupplierStock(long id);
	
	public List<SupplierStocks> getSupplierStocksByDate(String date);
	
	public List<SupplierStocks> getSupplierStocksByDateRange(String from_date, String to_date);
	
	public List<SupplierStocks> getSupplierStocksBySupplier(String supllier_name);
	
	public List<SupplierStocks> getSupplierStocksByItemName(String itemname);

}
