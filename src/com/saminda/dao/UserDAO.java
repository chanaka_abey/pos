package com.saminda.dao;

import java.util.List;

import com.saminda.entity.User;

public interface UserDAO {
	
	public String createUser(User user);
	
	public User updateUser(User user);
	
	public void deleteUser(String user);
	
	public List<User> getAllUsers();
	
	public User getUser(String username);
	
	public User getUserbyUsername(String username);
	
	public boolean checkLogin(String username, String password);

}
