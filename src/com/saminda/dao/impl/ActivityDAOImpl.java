package com.saminda.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.saminda.dao.ActivityDAO;
import com.saminda.entity.Activity;
import com.saminda.util.HibernateUtil;

@Repository
public class ActivityDAOImpl implements ActivityDAO{

	public ActivityDAOImpl(){
		System.out.println("ActivityDAOImpl()");
	}
	
	@Autowired
    private HibernateUtil hibernateUtil;
	
	@Override
	public List<Activity> getAllActivies() {
		return hibernateUtil.fetchAll(Activity.class);
	}

	@Override
	public List<Activity> getAllActiviesByDate(String logdate) {
		return hibernateUtil.fetchactivitybydate(logdate);
	}

	@Override
	public List<Activity> getAllActiviesByDateRange(String from_date,String to_date) {

		return hibernateUtil.fetchactivitydaterange(from_date, to_date);
	}

	@Override
	public List<Activity> getAllActiviesByAction(String action) {
		return hibernateUtil.fetchactivitybyaction(action);
	}

	@Override
	public List<Activity> getAllActiviesByUser(String user) {
		return hibernateUtil.fetchactivitybyusers(user);
	}

	@Override
	public long createActivity(Activity activity) {
		return (Long) hibernateUtil.create(activity);
	}

	@Override
	public List<Activity> getAllActions() {
		// TODO Auto-generated method stub
		return hibernateUtil.fetchallactivityactions22(Activity.class);
	}

}
