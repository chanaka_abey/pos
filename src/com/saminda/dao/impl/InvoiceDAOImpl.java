package com.saminda.dao.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.saminda.dao.InvoiceDAO;
import com.saminda.dao.ItemDAO;
import com.saminda.entity.Invoice;
import com.saminda.entity.Item;
import com.saminda.util.HibernateUtil;

@Repository
public class InvoiceDAOImpl implements InvoiceDAO{

	public InvoiceDAOImpl(){
		System.out.println("InvoiceDAOImpl()");
	}
	
	@Autowired
    private HibernateUtil hibernateUtil;

	@Override
	public long createInvoice(Invoice invoice) {
		return (Long) hibernateUtil.create(invoice);
	}

	@Override
	public Invoice updateInvoice(Invoice invoice) {
		return hibernateUtil.update(invoice);
	}

	@Override
	public void deleteInvoice(long id) {
		Invoice invoice = new Invoice();
		invoice.setId(id);
		 hibernateUtil.delete(invoice);
	}

	@Override
	public List<Invoice> getAllInvoice() {
		return hibernateUtil.fetchAll(Invoice.class);
	}

	@Override
	public Invoice getInvoice(long id) {
		return hibernateUtil.fetchById(id, Invoice.class);
	}

	@Override
	public Invoice getInvoice(String invoicenumber) {
		return hibernateUtil.fetchById(invoicenumber, Invoice.class);
	}

	@Override
	public List<Invoice> getAllInvoiceByDate(String purchasedate) {
		return hibernateUtil.fetchinvoicesbydate(purchasedate);
	}

	@Override
	public List<Invoice> getAllInvoiceByTime(Date purchasetime) {
		return null;
	}

	@Override
	public List<Invoice> getLastInvoiceNo() {
		return hibernateUtil.fetchAll("SELECT invoicenumber FROM possystem.invoice ORDER BY id DESC LIMIT 1");
	}

	@Override
	public List<Invoice> getSalesSum() {
		return hibernateUtil.fetchinvoiceSum(Invoice.class);
	}

	@Override
	public List<Invoice> getProfitSum() {
		return hibernateUtil.fetchprofitSum(Invoice.class);
	}

	@Override
	public List<Invoice> getTodayProfitSum() {
		return hibernateUtil.fetchAll("SELECT SUM(profit) FROM possystem.invoice where purchasedate = CURDATE()");
	}

	@Override
	public List<Invoice> getTodaySalesSum() {
		return hibernateUtil.fetchAll("SELECT SUM(totalsalesprice) FROM possystem.invoice where purchasedate = CURDATE()");
	}

	@Override
	public List<Invoice> getAllInvoiceByDateRange(String from_date,String to_date) {
		return hibernateUtil.fetchinvoicebydaterange(from_date, to_date);
	}

	@Override
	public List<Invoice> getOverallProfitByDate() {
		//return hibernateUtil.fetchAll("select purchasedate,SUM(totalsalesprice) as total_sale,SUM(totalunitprice) as total_unit,SUM(profit) as total_profit from possystem.invoice group by purchasedate;");
		return hibernateUtil.fetchoverallprofitbydate(Invoice.class);
	}

	@Override
	public List<Invoice> getAllProfitByDateRange(String from_date,String to_date) {
	
		return hibernateUtil.fetchprofitbydaterange(from_date, to_date);
	}

	@Override
	public List<Invoice> getAllInvoices(String invoicenumber) {
		// TODO Auto-generated method stub
		return hibernateUtil.getinvoicedetails(invoicenumber);
	}
	
	
}

