package com.saminda.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.saminda.dao.ItemDAO;
import com.saminda.entity.Employee;
import com.saminda.entity.Item;
import com.saminda.util.HibernateUtil;

@Repository
public class ItemDAOImpl implements ItemDAO{
	
	public ItemDAOImpl() {
		System.out.println("ItemDAOImpl()");
	}
	
	@Autowired
    private HibernateUtil hibernateUtil;

	@Override
	public long createItem(Item item) {
		return (Long) hibernateUtil.create(item);
	}

	@Override
	public Item updateItem(Item item) {
		return hibernateUtil.update(item);
	}

	@Override
	public void deleteItem(long id) {
		Item item = new Item();
		item.setId(id);
		 hibernateUtil.delete(item);
		
	}

	@Override
	public List<Item> getAllItem() {
		return hibernateUtil.fetchAll(Item.class);
	}

	@Override
	public Item getItem(long id) {
		return hibernateUtil.fetchById(id, Item.class);
	}

	@Override
	public List<Item> getAllItemByName(String itemname) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Item getItembyItemID(String itemid) {
		return hibernateUtil.fetchById(itemid, Item.class);
	}

	@Override
	public List<Item> getAllItemBelowReorderlevel() {
		return  hibernateUtil.fetchAll2(Item.class);
	}

	

}
