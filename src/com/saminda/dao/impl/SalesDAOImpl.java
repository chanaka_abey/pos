package com.saminda.dao.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.saminda.dao.SalesDAO;
import com.saminda.entity.Sales;
import com.saminda.util.HibernateUtil;

@Repository
public class SalesDAOImpl implements SalesDAO{

	public SalesDAOImpl(){
		System.out.println("SalesDAOImpl()");
	}
	
	@Autowired
    private HibernateUtil hibernateUtil;
	
	@Override
	public long createSales(Sales sales) {
		return (Long) hibernateUtil.create(sales);
	}

	@Override
	public Sales updateSales(Sales sales) {
		return hibernateUtil.update(sales);
	}

	@Override
	public void deleteSales(long id) {
		Sales sales = new Sales();
		sales.setId(id);
		 hibernateUtil.delete(sales);
		
	}

	@Override
	public List<Sales> getAllSales() {
		return hibernateUtil.fetchAll(Sales.class);
	}

	@Override
	public Sales getSales(long id) {
		return hibernateUtil.fetchById(id, Sales.class);
	}

	@Override
	public Sales getSales(String invoicenumber) {
		return hibernateUtil.fetchById(invoicenumber, Sales.class);
	}

	@Override
	public List<Sales> getAllSalesByDate(String purchasedate) {
		return hibernateUtil.fetchsalesbydate(purchasedate);
	}

	@Override
	public List<Sales> getAllSalesByTime(String purchasetime) {
		return null;
	}

	@Override
	public List<Sales> getAllSalesByDateRange(String from_date, String to_date) {
		return hibernateUtil.fetchsalesbydaterange(from_date, to_date);
	}

	@Override
	public List<Sales> getAllSalesByItemName(String itemname) {
		return hibernateUtil.fetchsalesbyitemname(itemname);
	}

	@Override
	public List<Sales> getTodaySales() {
		// TODO Auto-generated method stub
		
		return hibernateUtil.fetchAll("SELECT * FROM possystem.sales where purchasedate = CURDATE()");
	}

	@Override
	public List<Sales> getAllSalesByInvoicenumber(String invoicenumber) {
		// TODO Auto-generated method stub
		return hibernateUtil.fetchsalesbyinvoicenumber(invoicenumber);
	}

}
