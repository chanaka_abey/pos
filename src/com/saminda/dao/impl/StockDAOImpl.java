package com.saminda.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.saminda.dao.StockDAO;
import com.saminda.entity.Stock;
import com.saminda.util.HibernateUtil;

@Repository
public class StockDAOImpl implements StockDAO{

	public StockDAOImpl(){
		System.out.println("StockDAOImpl()");
	}
	
	@Autowired
    private HibernateUtil hibernateUtil;
	
	@Override
	public List<Stock> getAllStock() {
		return hibernateUtil.fetchAll(Stock.class);
	}

	@Override
	public List<Stock> getStockById(long id) {
		return hibernateUtil.fetchAll("query");
	}

	@Override
	public List<Stock> getStockByItemId(String itemid) {
		return hibernateUtil.fetchAll("query");
	}

	@Override
	public List<Stock> getStockByItemName(String itemname) {
		return hibernateUtil.fetchstotckupdatebyitemname(itemname);
	}

	@Override
	public List<Stock> getStockByDate(String update_date) {
		//return hibernateUtil.fetchAll("SELECT * FROM possystem.stock where update_date="+update_date);
		//return null;
		return hibernateUtil.fetchstotckupdatereportdate(update_date);
	}

	@Override
	public List<Stock> getStockByTime(String update_time) {
		return hibernateUtil.fetchAll("query");
	}

	@Override
	public long CreateStock(Stock stock) {
		return (Long) hibernateUtil.create(stock);
	}

	@Override
	public List<Stock> getStockByDateRange(String from_date, String to_date) {
		return hibernateUtil.fetchstotckupdatereportdaterange(from_date, to_date);
	}

}
