package com.saminda.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.saminda.dao.SupplierStocksDAO;
import com.saminda.entity.SupplierStocks;
import com.saminda.util.HibernateUtil;

@Repository
public class SupplierStocksDAOImpl implements SupplierStocksDAO{

	public SupplierStocksDAOImpl() {
		System.out.println("SupplierStocksDAOImpl()");
	}
	
	@Autowired
    private HibernateUtil hibernateUtil;
	
	@Override
	public long createSupplierStocks(SupplierStocks supplierStocks) {
		return (long) hibernateUtil.create(supplierStocks);
	}

	@Override
	public SupplierStocks updateSupplierStocks(SupplierStocks supplierStocks) {
		return hibernateUtil.update(supplierStocks);
	}

	@Override
	public void deleteSupplierStocks(long id) {
		SupplierStocks supplierStocks = new SupplierStocks();
		supplierStocks.setId(id);
		hibernateUtil.delete(supplierStocks);
		
	}

	@Override
	public List<SupplierStocks> getAllSupplierStocks() {
		return hibernateUtil.fetchAll(SupplierStocks.class);
	}

	@Override
	public SupplierStocks getSupplierStock(long id) {
		return hibernateUtil.fetchById(id, SupplierStocks.class);
	}

	@Override
	public List<SupplierStocks> getSupplierStocksByDate(String date) {
		return hibernateUtil.fetchsupplierstocksbydate(date);
	}

	@Override
	public List<SupplierStocks> getSupplierStocksByDateRange(String from_date,
			String to_date) {
		return hibernateUtil.fetchsupplierstockbydaterange(from_date, to_date);
	}

	@Override
	public List<SupplierStocks> getSupplierStocksBySupplier(String suppliername) {
		return hibernateUtil.fetchsupplierstockbysuppliername(suppliername);
	}

	@Override
	public List<SupplierStocks> getSupplierStocksByItemName(String itemname) {
		return hibernateUtil.fetchsupplierstocksbyitemname(itemname);
	}

}
