package com.saminda.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.saminda.dao.SuppliersDAO;
import com.saminda.entity.Item;
import com.saminda.entity.Suppliers;
import com.saminda.util.HibernateUtil;

@Repository
public class SuppliersDAOImpl implements SuppliersDAO{

	public SuppliersDAOImpl() {
		System.out.println("SuppliersDAOImpl()");
	}
	
	@Autowired
    private HibernateUtil hibernateUtil;
	
	@Override
	public long createSupplier(Suppliers suppliers) {
		return (Long) hibernateUtil.create(suppliers);
	}

	@Override
	public Suppliers updateSuppliers(Suppliers suppliers) {
		return hibernateUtil.update(suppliers);
	}

	@Override
	public void deleteSupplier(long id) {
		Suppliers suppliers = new Suppliers();
		suppliers.setId(id);
		hibernateUtil.delete(suppliers);
		
	}

	@Override
	public List<Suppliers> getAllSuppliers() {
		return hibernateUtil.fetchAll(Suppliers.class);
	}

	@Override
	public Suppliers getSupplier(long id) {
		return hibernateUtil.fetchById(id, Suppliers.class);
	}

}
