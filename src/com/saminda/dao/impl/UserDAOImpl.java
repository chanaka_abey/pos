package com.saminda.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.saminda.dao.UserDAO;
import com.saminda.entity.User;
import com.saminda.util.HibernateUtil;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Query;

@Repository
public class UserDAOImpl implements UserDAO{
	
	public UserDAOImpl() {
		System.out.println("UserDAOImpl()");
	}
	
	@Autowired
    private HibernateUtil hibernateUtil;
	
	@Autowired
    private SessionFactory sessionFactory;

	@Override
	public String createUser(User user) {
		return (String) hibernateUtil.create(user);
	}

	@Override
	public User updateUser(User user) {
		return hibernateUtil.update(user);
	}

	@Override
	public void deleteUser(String username) {
		User user = new User();
		user.setUsername(username);;
		hibernateUtil.delete(user);
		
	}

	@Override
	public List<User> getAllUsers() {
		return hibernateUtil.fetchAll(User.class);
	}

	@Override
	public User getUser(String username) {
		return hibernateUtil.fetchById(username, User.class);
	}

	@Override
	public boolean checkLogin(String username, String password) {
		System.out.println("In Check login");
		Session session = sessionFactory.openSession();
		boolean userFound = false;
		//Query using Hibernate Query Language
		String SQL_QUERY =" from User as o where o.username=? and o.password=?";
		Query query = session.createQuery(SQL_QUERY);
		query.setParameter(0,username);
		query.setParameter(1,password);
		//query.setParameter(2,role);
		List list = query.list();
		System.out.println("query list ----- > " + list);
		
		

		if ((list != null) && (list.size() > 0)) {
			userFound= true;
		}

		session.close();
		return userFound; 
	}

	@Override
	public User getUserbyUsername(String username) {
		//return (User) hibernateUtil.fetchAll("select * from possystem.user where username="+username);
		return hibernateUtil.fetchByUsername(username, User.class);
	}
	
	

}
