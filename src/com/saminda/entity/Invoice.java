package com.saminda.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "invoice")

public class Invoice implements Serializable {

private static final long serialVersionUID = -7988799579036225137L;
	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
	
	@NotNull
	@Column
    private String invoicenumber;
	
	@Column
    private String purchasedate;
	
	@Column
    private String purchasetime;
	
	@Column
	private Double totalsalesprice;
	
	@Column
	private Double totalunitprice;
	
	@Column
	private int totaldiscount;
	
	@Column
	private Double profit;
	
	public Invoice(){
		
	}
	

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getInvoicenumber() {
		return invoicenumber;
	}

	public void setInvoicenumber(String invoicenumber) {
		this.invoicenumber = invoicenumber;
	}

	public String getPurchasedate() {
		return purchasedate;
	}

	public void setPurchasedate(String purchasedate) {
		this.purchasedate = purchasedate;
	}

	public String getPurchasetime() {
		return purchasetime;
	}

	public void setPurchasetime(String purchasetime) {
		this.purchasetime = purchasetime;
	}

	public Double getTotalsalesprice() {
		return totalsalesprice;
	}

	public void setTotalsalesprice(Double totalsalesprice) {
		this.totalsalesprice = totalsalesprice;
	}
	
	

	public Double getTotalunitprice() {
		return totalunitprice;
	}


	public void setTotalunitprice(Double totalunitprice) {
		this.totalunitprice = totalunitprice;
	}


	public int getTotaldiscount() {
		return totaldiscount;
	}

	public void setTotaldiscount(int totaldiscount) {
		this.totaldiscount = totaldiscount;
	}

	public Double getProfit() {
		return profit;
	}

	public void setProfit(Double profit) {
		this.profit = profit;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
	
	
	
}
