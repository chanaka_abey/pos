package com.saminda.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "item")
public class Item implements Serializable{

	private static final long serialVersionUID = -7988799579036225137L;
	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
	
	@NotNull
	@Column
    private String itemid;
	
	@Column
    private String itemname;
	
	@Column
    private String category;
	
	@Column
    private String position;
	
	@Column
    private float unitprice;
	
	@Column
    private float salesprice;
	
	@Column
    private int quantity;
	
	@Column
    private int reorderlevel;
	
	
	
	
	public Item(){
		
	}


	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public String getItemid() {
		return itemid;
	}


	public void setItemid(String itemid) {
		this.itemid = itemid;
	}


	public String getItemname() {
		return itemname;
	}


	public void setItemname(String itemname) {
		this.itemname = itemname;
	}


	public String getCategory() {
		return category;
	}


	public void setCategory(String category) {
		this.category = category;
	}


	public float getUnitprice() {
		return unitprice;
	}


	public void setUnitprice(float unitprice) {
		this.unitprice = unitprice;
	}


	public float getSalesprice() {
		return salesprice;
	}


	public void setSalesprice(float salesprice) {
		this.salesprice = salesprice;
	}


	public int getQuantity() {
		return quantity;
	}


	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}


	public int getReorderlevel() {
		return reorderlevel;
	}


	public void setReorderlevel(int reorderlevel) {
		this.reorderlevel = reorderlevel;
	}


	public String getPosition() {
		return position;
	}


	public void setPosition(String position) {
		this.position = position;
	}
	
	 
	
}
