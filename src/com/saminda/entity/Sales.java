package com.saminda.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;


@Entity
@Table(name = "sales")
public class Sales implements Serializable{
	
private static final long serialVersionUID = -7988799579036225137L;
	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
	
	@NotNull
	@Column
    private String invoicenumber;
	
	@Column
    private String purchasedate;
	
	@Column
    private String purchasetime;
	
	@Column
	private String itemid;
	
	@Column
	private String itemname;
	
	@Column
    private Double salesprice;
	
	@Column
    private int quantity;
	
	@Column
    private int discount;
	
	@Column
    private Double totalsalesprice;
	
	@Column
    private int item_id;
	
	

	public Sales(){
		
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getInvoicenumber() {
		return invoicenumber;
	}

	public void setInvoicenumber(String invoicenumber) {
		this.invoicenumber = invoicenumber;
	}

	public String getPurchasedate() {
		return purchasedate;
	}

	public void setPurchasedate(String purchasedate) {
		this.purchasedate = purchasedate;
	}

	public String getPurchasetime() {
		return purchasetime;
	}

	public void setPurchasetime(String purchasetime) {
		this.purchasetime = purchasetime;
	}

	public String getItemname() {
		return itemname;
	}

	public void setItemname(String itemname) {
		this.itemname = itemname;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public int getDiscount() {
		return discount;
	}

	public void setDiscount(int discount) {
		this.discount = discount;
	}

	public Double getTotalsalesprice() {
		return totalsalesprice;
	}

	public void setTotalsalesprice(Double totalsalesprice) {
		this.totalsalesprice = totalsalesprice;
	}

	public String getItemid() {
		return itemid;
	}

	public void setItemid(String itemid) {
		this.itemid = itemid;
	}

	public Double getSalesprice() {
		return salesprice;
	}

	public void setSalesprice(Double salesprice) {
		this.salesprice = salesprice;
	}

	public int getItem_id() {
		return item_id;
	}

	public void setItem_id(int item_id) {
		this.item_id = item_id;
	}
	
	

}
