package com.saminda.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;


@Entity
@Table(name = "user")
public class User implements Serializable{

private static final long serialVersionUID = -7988799579036225137L;
	

   // @GeneratedValue(strategy = GenerationType.AUTO)
   // private long id;
	
	@Id
	@NotNull
	@Column
	private String username;
	
	@Column
	private String password;
	
	@Column
	private String role;
	
	public User(){
		
	}

	

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
	
	
}
