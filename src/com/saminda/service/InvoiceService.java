package com.saminda.service;

import java.util.Date;
import java.util.List;

import com.saminda.entity.Invoice;

public interface InvoiceService {

	public long createInvoice(Invoice invoice);

	public Invoice updateInvoice(Invoice invoice);

	public void deleteInvoice(long id);

	public List<Invoice> getAllInvoice();
	
	public List<Invoice> getLastInvoiceNo();
	
	public List<Invoice> getSalesSum();
	
	public List<Invoice> getProfitSum();
	
	public List<Invoice> getOverallProfitByDate();
	
	public List<Invoice> getTodayProfitSum();
	
	public List<Invoice> getTodaySalesSum();

	public Invoice getInvoice(long id);

	public Invoice getInvoice(String invoicenumber);
	
	public List<Invoice> getAllInvoices(String invoicenumber);

	public List<Invoice> getAllInvoiceByDate(String purchasedate);
	
	public List<Invoice> getAllInvoiceByDateRange(String from_date,String to_date);

	public List<Invoice> getAllInvoiceByTime(Date purchasetime);
	
	public List<Invoice> getAllProfitByDateRange(String from_date,String to_date);

}
