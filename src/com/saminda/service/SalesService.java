package com.saminda.service;

import java.util.Date;
import java.util.List;

import com.saminda.entity.Sales;

public interface SalesService {

	public long createSales(Sales sales);

	public Sales updateSales(Sales sales);

	public void deleteSales(long id);

	public List<Sales> getAllSales();
	
	public List<Sales> getTodaySales();

	public Sales getSales(long id);

	public Sales getSales(String invoicenumber);

	public List<Sales> getAllSalesByDate(String purchasedate);
	
	public List<Sales> getAllSalesByDateRange(String from_date, String to_date);
	
	public List<Sales> getAllSalesByItemName(String itemname);

	public List<Sales> getAllSalesByTime(String purchasetime);
	
	public List<Sales> getAllSalesByInvoicenumber(String invoicenumber);
	
}
