package com.saminda.service;

import java.util.List;

import com.saminda.entity.Suppliers;

public interface SuppliersService {

	public long createSupplier(Suppliers suppliers);

	public Suppliers updateSuppliers(Suppliers suppliers);

	public void deleteSupplier(long id);

	public List<Suppliers> getAllSuppliers();

	public Suppliers getSupplier(long id);

}
