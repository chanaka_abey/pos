package com.saminda.service;

import java.util.List;

import com.saminda.entity.User;

public interface UserService {

	public String createUser(User user);
	
	public User updateUser(User user);
	
	public void deleteUser(String username);
	
	public List<User> getAllUsers();
	
	public User getUser(String username);
	
	public User getUserbyUsername(String username);
	
	public boolean checkLogin(String username, String password);
	
}
