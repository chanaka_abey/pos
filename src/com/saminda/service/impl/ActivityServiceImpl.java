package com.saminda.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.saminda.dao.ActivityDAO;
import com.saminda.entity.Activity;
import com.saminda.service.ActivityService;

@Service
@Transactional
public class ActivityServiceImpl implements ActivityService{

	public ActivityServiceImpl() {
		System.out.println("ActivityServiceImpl()");
	}
	
	@Autowired
	private ActivityDAO activityDAO;
	
	@Override
	public List<Activity> getAllActivies() {
		return activityDAO.getAllActivies();
	}

	@Override
	public List<Activity> getAllActiviesByDate(String logdate) {
		return activityDAO.getAllActiviesByDate(logdate);
	}

	@Override
	public List<Activity> getAllActiviesByDateRange(String from_date,String to_date) {
		return activityDAO.getAllActiviesByDateRange(from_date, to_date);
	}

	@Override
	public List<Activity> getAllActiviesByAction(String action) {
		return activityDAO.getAllActiviesByAction(action);
	}

	@Override
	public List<Activity> getAllActiviesByUser(String user) {
		return activityDAO.getAllActiviesByUser(user);
	}

	@Override
	public long createActivity(Activity activity) {
		return activityDAO.createActivity(activity);
	}

	@Override
	public List<Activity> getAllActions() {
		return activityDAO.getAllActions();
	}

}
