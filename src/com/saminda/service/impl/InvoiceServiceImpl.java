package com.saminda.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.saminda.dao.InvoiceDAO;
import com.saminda.entity.Invoice;
import com.saminda.service.InvoiceService;

@Service
@Transactional
public class InvoiceServiceImpl implements InvoiceService{

	public InvoiceServiceImpl(){
		System.out.println("InvoiceServiceImpl()");
	}
	
	@Autowired
	private InvoiceDAO invoiceDAO;
	
	
	@Override
	public long createInvoice(Invoice invoice) {
		return invoiceDAO.createInvoice(invoice);
	}

	@Override
	public Invoice updateInvoice(Invoice invoice) {
		return invoiceDAO.updateInvoice(invoice);
	}

	@Override
	public void deleteInvoice(long id) {
		invoiceDAO.deleteInvoice(id);
	}

	@Override
	public List<Invoice> getAllInvoice() {
		return invoiceDAO.getAllInvoice();
	}

	@Override
	public Invoice getInvoice(long id) {
		return invoiceDAO.getInvoice(id);
	}

	@Override
	public Invoice getInvoice(String invoicenumber) {
		return invoiceDAO.getInvoice(invoicenumber);
	}

	@Override
	public List<Invoice> getAllInvoiceByDate(String purchasedate) {
		return invoiceDAO.getAllInvoiceByDate(purchasedate);
	}

	@Override
	public List<Invoice> getAllInvoiceByTime(Date purchasetime) {
		return invoiceDAO.getAllInvoiceByTime(purchasetime);
	}

	@Override
	public List<Invoice> getLastInvoiceNo() {
		return invoiceDAO.getLastInvoiceNo();
	}

	@Override
	public List<Invoice> getSalesSum() {
		return invoiceDAO.getSalesSum();
	}

	@Override
	public List<Invoice> getProfitSum() {
		return invoiceDAO.getProfitSum();
	}

	@Override
	public List<Invoice> getTodayProfitSum() {
		return invoiceDAO.getTodayProfitSum();
	}

	@Override
	public List<Invoice> getTodaySalesSum() {
		return invoiceDAO.getTodaySalesSum();
	}

	@Override
	public List<Invoice> getAllInvoiceByDateRange(String from_date,String to_date) {
		return invoiceDAO.getAllInvoiceByDateRange(from_date, to_date);
	}

	@Override
	public List<Invoice> getOverallProfitByDate() {
		return invoiceDAO.getOverallProfitByDate();
	}

	@Override
	public List<Invoice> getAllProfitByDateRange(String from_date,String to_date) {
		return invoiceDAO.getAllProfitByDateRange(from_date, to_date);
	}

	@Override
	public List<Invoice> getAllInvoices(String invoicenumber) {
		return invoiceDAO.getAllInvoices(invoicenumber);
	}

	
	
}
