package com.saminda.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.saminda.dao.ItemDAO;
import com.saminda.entity.Item;
import com.saminda.service.ItemService;


@Service
@Transactional
public class ItemServiceImpl implements ItemService{
	
	public ItemServiceImpl() {
		System.out.println("ItemServiceImpl()");
	}
	
	@Autowired
	private ItemDAO itemDAO;

	@Override
	public long createItem(Item item) {
		return itemDAO.createItem(item);
	}

	@Override
	public Item updateItem(Item item) {
		return itemDAO.updateItem(item);
	}

	@Override
	public void deleteItem(long id) {
		itemDAO.deleteItem(id);
		
	}

	@Override
	public List<Item> getAllItem() {
		return itemDAO.getAllItem();
	}

	@Override
	public Item getItem(long id) {
		return itemDAO.getItem(id);
	}

	@Override
	public List<Item> getAllItemByName(String itemname) {
		return itemDAO.getAllItemByName(itemname);
	}

	@Override
	public Item getItembyItemID(String itemid) {
		return itemDAO.getItembyItemID(itemid);
	}

	@Override
	public List<Item> getAllItemBelowReorderlevel() {
		return itemDAO.getAllItemBelowReorderlevel();
	}

}
