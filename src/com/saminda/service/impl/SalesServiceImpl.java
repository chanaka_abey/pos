package com.saminda.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.saminda.dao.SalesDAO;
import com.saminda.entity.Sales;
import com.saminda.service.SalesService;

@Service
@Transactional
public class SalesServiceImpl implements SalesService{

	public SalesServiceImpl(){
		System.out.println("SalesServiceImpl()");
	}
	
	@Autowired
	private SalesDAO salesDAO;
	
	@Override
	public long createSales(Sales sales) {
		return salesDAO.createSales(sales);
	}

	@Override
	public Sales updateSales(Sales sales) {
		return salesDAO.updateSales(sales);
	}

	@Override
	public void deleteSales(long id) {
		salesDAO.deleteSales(id);
		
	}

	@Override
	public List<Sales> getAllSales() {
		return salesDAO.getAllSales();
	}

	@Override
	public Sales getSales(long id) {
		return salesDAO.getSales(id);
	}

	@Override
	public Sales getSales(String invoicenumber) {
		return salesDAO.getSales(invoicenumber);
	}

	@Override
	public List<Sales> getAllSalesByDate(String purchasedate) {
		return salesDAO.getAllSalesByDate(purchasedate);
	}

	@Override
	public List<Sales> getAllSalesByTime(String purchasetime) {
		return salesDAO.getAllSalesByTime(purchasetime);
	}

	@Override
	public List<Sales> getAllSalesByDateRange(String from_date, String to_date) {
		return salesDAO.getAllSalesByDateRange(from_date, to_date);
	}

	@Override
	public List<Sales> getAllSalesByItemName(String itemname) {
		return salesDAO.getAllSalesByItemName(itemname);
	}

	@Override
	public List<Sales> getTodaySales() {
		return salesDAO.getTodaySales();
	}

	@Override
	public List<Sales> getAllSalesByInvoicenumber(String invoicenumber) {
		return salesDAO.getAllSalesByInvoicenumber(invoicenumber);
	}

}
