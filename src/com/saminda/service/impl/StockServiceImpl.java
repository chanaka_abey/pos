package com.saminda.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.saminda.dao.StockDAO;
import com.saminda.entity.Stock;
import com.saminda.service.StockService;

@Service
@Transactional
public class StockServiceImpl implements StockService{

	public StockServiceImpl() {
		System.out.println("StockServiceImpl()");
	}
	
	@Autowired
	private StockDAO stockDAO;
	
	@Override
	public List<Stock> getAllStock() {
		return stockDAO.getAllStock();
	}

	@Override
	public List<Stock> getStockById(long id) {
		return stockDAO.getStockById(id);
	}

	@Override
	public List<Stock> getStockByItemId(String itemid) {
		return stockDAO.getStockByItemId(itemid);
	}

	@Override
	public List<Stock> getStockByItemName(String itemname) {
		return stockDAO.getStockByItemName(itemname);
	}

	@Override
	public List<Stock> getStockByDate(String update_date) {
		return stockDAO.getStockByDate(update_date);
	}

	@Override
	public List<Stock> getStockByTime(String update_time) {
		return stockDAO.getStockByTime(update_time);
	}

	@Override
	public long CreateStock(Stock stock) {
		return stockDAO.CreateStock(stock);
	}

	@Override
	public List<Stock> getStockByDateRange(String from_date, String to_date) {
		return stockDAO.getStockByDateRange(from_date, to_date);
	}

}
