package com.saminda.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.saminda.dao.SupplierStocksDAO;
import com.saminda.entity.SupplierStocks;
import com.saminda.service.SupplierStocksService;


@Service
@Transactional
public class SupplierStocksServiceImpl implements SupplierStocksService{

	public SupplierStocksServiceImpl() {
		System.out.println("SupplierStocksServiceImpl()");
	}
	
	@Autowired
	private SupplierStocksDAO supplierStocksDAO;

	@Override
	public long createSupplierStocks(SupplierStocks supplierStocks) {

		return supplierStocksDAO.createSupplierStocks(supplierStocks);
	}

	@Override
	public SupplierStocks updateSupplierStocks(SupplierStocks supplierStocks) {
		return supplierStocksDAO.updateSupplierStocks(supplierStocks);
	}

	@Override
	public void deleteSupplierStocks(long id) {
		supplierStocksDAO.deleteSupplierStocks(id);;
		
	}

	@Override
	public List<SupplierStocks> getAllSupplierStocks() {
		return supplierStocksDAO.getAllSupplierStocks();
	}

	@Override
	public SupplierStocks getSupplierStock(long id) {
		return supplierStocksDAO.getSupplierStock(id);
	}

	@Override
	public List<SupplierStocks> getSupplierStocksByDate(String date) {
		return supplierStocksDAO.getSupplierStocksByDate(date);
	}

	@Override
	public List<SupplierStocks> getSupplierStocksByDateRange(String from_date,
			String to_date) {
		return supplierStocksDAO.getSupplierStocksByDateRange(from_date, to_date);
	}

	@Override
	public List<SupplierStocks> getSupplierStocksBySupplier(String supllier_name) {
		return supplierStocksDAO.getSupplierStocksBySupplier(supllier_name);
	}

	@Override
	public List<SupplierStocks> getSupplierStocksByItemName(String itemname) {
		return supplierStocksDAO.getSupplierStocksByItemName(itemname);
	}
	
	

}
