package com.saminda.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.saminda.dao.SuppliersDAO;
import com.saminda.entity.Suppliers;
import com.saminda.service.SuppliersService;

@Service
@Transactional
public class SuppliersServiceImpl implements SuppliersService{

	public SuppliersServiceImpl() {
		System.out.println("SuppliersServiceImpl()");
	}
	
	@Autowired
	private SuppliersDAO suppliersDAO;
	
	@Override
	public long createSupplier(Suppliers suppliers) {
		return suppliersDAO.createSupplier(suppliers);
	}

	@Override
	public Suppliers updateSuppliers(Suppliers suppliers) {
		return suppliersDAO.updateSuppliers(suppliers);
	}

	@Override
	public void deleteSupplier(long id) {
		suppliersDAO.deleteSupplier(id);
		
	}

	@Override
	public List<Suppliers> getAllSuppliers() {
		return suppliersDAO.getAllSuppliers();
	}

	@Override
	public Suppliers getSupplier(long id) {
		return suppliersDAO.getSupplier(id);
	}

}
