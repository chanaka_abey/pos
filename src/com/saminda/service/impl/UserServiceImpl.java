package com.saminda.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.saminda.dao.UserDAO;
import com.saminda.entity.User;
import com.saminda.service.UserService;


@Service
@Transactional
public class UserServiceImpl implements UserService{

	public UserServiceImpl() {
		System.out.println("UserServiceImpl()");
	}
	
	@Autowired
	private UserDAO userDAO;
	
	
	public UserDAO getUserDAO() {
		return userDAO;
	}

	public void setUserDAO(UserDAO userDAO) {
		this.userDAO = userDAO;
	}
	
	@Override
	public String createUser(User user) {
		return userDAO.createUser(user);
	}

	@Override
	public User updateUser(User user) {
		return userDAO.updateUser(user);
	}

	@Override
	public void deleteUser(String username) {
		userDAO.deleteUser(username);
		
	}

	@Override
	public List<User> getAllUsers() {
		return userDAO.getAllUsers();
	}

	@Override
	public User getUser(String username) {
		return userDAO.getUser(username);
	}

	@Override
	public boolean checkLogin(String username, String password) {
		System.out.println("In Service class...Check Login");
        return userDAO.checkLogin(username, password);
	}

	@Override
	public User getUserbyUsername(String username) {
		return userDAO.getUserbyUsername(username);
	}

	
	
	

}
