package com.saminda.util;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projection;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.saminda.entity.Activity;
import com.saminda.entity.Invoice;
import com.saminda.entity.Item;
import com.saminda.entity.Sales;
import com.saminda.entity.Stock;
import com.saminda.entity.SupplierStocks;
import com.saminda.entity.User;

/**
 * @author Chanaka
 */
@Repository
public class HibernateUtil {
	
	@Autowired
    private SessionFactory sessionFactory;
		
    public <T> Serializable create(final T entity) {
        return sessionFactory.getCurrentSession().save(entity);        
    }
    
   
    public <T> T update(final T entity) {
        sessionFactory.getCurrentSession().update(entity);   
        return entity;
    }
    
	public <T> void delete(final T entity) {
		sessionFactory.getCurrentSession().delete(entity);
	}

	public <T> void delete(Serializable id, Class<T> entityClass) {
		T entity = fetchById(id, entityClass);
		delete(entity);
	}
    
    @SuppressWarnings("unchecked")	
    public <T> List<T> fetchAll(Class<T> entityClass) {        
        return sessionFactory.getCurrentSession().createQuery(" FROM "+entityClass.getName()).list();        
    }
  
    @SuppressWarnings("rawtypes")
	public <T> List fetchAll(String query) {        
        return sessionFactory.getCurrentSession().createSQLQuery(query).list();        
    }
    
    
    @SuppressWarnings("rawtypes")
	public <T> List fetchAll2(Class<T> entityClass) {   
    	Criteria cr = sessionFactory.getCurrentSession().createCriteria(Item.class);
    	//Item item = new Item();
    	//cr.add(Restrictions.lt("quantity", item.getReorderlevel()));
    //	cr.add(Restrictions.le("quantity", value));
    	cr.add(Restrictions.sqlRestriction("quantity <= reorderlevel"));
    	//cr.add(Restrictions.sqlRestriction("ORDER BY id DESC LIMIT 1"));
    	
    	List results = cr.list();
       // return sessionFactory.getCurrentSession().createSQLQuery(query).list();     
    	return results;
    }
    
    
    @SuppressWarnings("rawtypes")
	public <T> List fetchinvoiceSum(Class<T> entityClass) {   
    	Criteria cr = sessionFactory.getCurrentSession().createCriteria(Invoice.class);

    	cr.setProjection(Projections.sum("totalsalesprice"));
    	List results = cr.list();    
    	return results;
    }
    
    @SuppressWarnings("rawtypes")
	public <T> List fetchstotckupdatereportdate(String update_date) {   
    	Criteria cr = sessionFactory.getCurrentSession().createCriteria(Stock.class);
    	System.out.println("Hibernate Util update_date : " + update_date);
    	cr.add(Restrictions.eq("update_date", update_date));
    	List results = cr.list();
    	return results;
    }
    
    @SuppressWarnings("rawtypes")
	public <T> List fetchinvoicesbydate(String purchasedate) {   
    	Criteria cr = sessionFactory.getCurrentSession().createCriteria(Invoice.class);
    	System.out.println("Hibernate Util purchasedate : " + purchasedate);
    	cr.add(Restrictions.eq("purchasedate", purchasedate));
    	List results = cr.list();
    	return results;
    }
    
    @SuppressWarnings("rawtypes")
	public <T> List fetchsalesbydate(String purchasedate) {   
    	Criteria cr = sessionFactory.getCurrentSession().createCriteria(Sales.class);
    	System.out.println("Hibernate Util purchasedate : " + purchasedate);
    	cr.add(Restrictions.eq("purchasedate", purchasedate));
    	List results = cr.list();
    	return results;
    }
    
    @SuppressWarnings("rawtypes")
	public <T> List fetchsupplierstocksbydate(String date) {   
    	Criteria cr = sessionFactory.getCurrentSession().createCriteria(SupplierStocks.class);
    	System.out.println("Hibernate Util purchasedate : " + date);
    	cr.add(Restrictions.eq("date", date));
    	List results = cr.list();
    	return results;
    }
    
    
    @SuppressWarnings("rawtypes")
	public <T> List fetchactivitybydate(String logdate) {   
    	Criteria cr = sessionFactory.getCurrentSession().createCriteria(Activity.class);
    	System.out.println("Hibernate Util purchasedate : " + logdate);
    	cr.add(Restrictions.eq("logdate", logdate));
    	List results = cr.list();
    	return results;
    }
    
    @SuppressWarnings("rawtypes")
	public <T> List fetchstotckupdatebyitemname(String itemname) {   
    	Criteria cr = sessionFactory.getCurrentSession().createCriteria(Stock.class);
    	System.out.println("Hibernate Itemname : " + itemname);
    	cr.add(Restrictions.eq("itemname", itemname));
    	List results = cr.list();
    	return results;
    }
    
    @SuppressWarnings("rawtypes")
	public <T> List fetchsupplierstockbysuppliername(String suppliername) {   
    	Criteria cr = sessionFactory.getCurrentSession().createCriteria(SupplierStocks.class);
    	System.out.println("Hibernate suppliername : " + suppliername);
    	cr.add(Restrictions.eq("supplier_name", suppliername));
    	List results = cr.list();
    	return results;
    }
    
    @SuppressWarnings("rawtypes")
   	public <T> List fetchsupplierstocksbyitemname(String itemname) {   
       	Criteria cr = sessionFactory.getCurrentSession().createCriteria(SupplierStocks.class);
       	System.out.println("Hibernate Itemname : " + itemname);
       	cr.add(Restrictions.eq("itemname", itemname));
       	List results = cr.list();
       	return results;
       }
    
    
    @SuppressWarnings("rawtypes")
	public <T> List fetchsalesbyitemname(String itemname) {   
    	Criteria cr = sessionFactory.getCurrentSession().createCriteria(Sales.class);
    	System.out.println("Hibernate Itemname : " + itemname);
    	cr.add(Restrictions.eq("itemname", itemname));
    	List results = cr.list();
    	return results;
    }
    
    @SuppressWarnings("rawtypes")
	public <T> List fetchsalesbyinvoicenumber(String invoicenumber) {   
    	Criteria cr = sessionFactory.getCurrentSession().createCriteria(Sales.class);
    	System.out.println("Hibernate Itemname : " + invoicenumber);
    	cr.add(Restrictions.eq("invoicenumber", invoicenumber));
    	List results = cr.list();
    	return results;
    }
    
    @SuppressWarnings("rawtypes")
	public <T> List fetchactivitybyusers(String username) {   
    	Criteria cr = sessionFactory.getCurrentSession().createCriteria(Activity.class);
    	System.out.println("Hibernate Activity user : " + username);
    	cr.add(Restrictions.eq("user", username));
    	List results = cr.list();
    	return results;
    }
    
    @SuppressWarnings("rawtypes")
   	public <T> List fetchactivitybyaction(String action) {   
       	Criteria cr = sessionFactory.getCurrentSession().createCriteria(Activity.class);
       	System.out.println("Hibernate Activity action : " + action);
       	cr.add(Restrictions.eq("action", action));
       	List results = cr.list();
       	return results;
       }
    
    @SuppressWarnings("rawtypes")
	public <T> List fetchallactivityactions22(Class<T> entityClass) {   
    	Criteria cr = sessionFactory.getCurrentSession().createCriteria(Activity.class);
    	//cr.add(Restrictions.sqlRestriction("quantity <= reorderlevel"));
    	ProjectionList projList = Projections.projectionList();
    	projList.add(Projections.property("action"));
    	cr.setProjection(Projections.distinct(projList));
    	
    	List results = cr.list();
    	System.out.println("fetchallactivityactions22 ----- > " + results);
    	return results;
    }
    
    
    @SuppressWarnings("rawtypes")
   	public <T> List fetchstotckupdatereportdaterange(String from_date, String to_date) {   
       	Criteria cr = sessionFactory.getCurrentSession().createCriteria(Stock.class);
       	System.out.println("Hibernate Util from date : " + from_date);
       	System.out.println("Hibernate Util to date : " + to_date);
       	
       	cr.add(Restrictions.ge("update_date", from_date)); 
       	cr.add(Restrictions.le("update_date", to_date));
       	List results = cr.list();
       	return results;
       }
    
    @SuppressWarnings("rawtypes")
   	public <T> List fetchsupplierstockbydaterange(String from_date, String to_date) {   
       	Criteria cr = sessionFactory.getCurrentSession().createCriteria(SupplierStocks.class);
       	System.out.println("Hibernate Util from date : " + from_date);
       	System.out.println("Hibernate Util to date : " + to_date);
       	
       	cr.add(Restrictions.ge("date", from_date)); 
       	cr.add(Restrictions.le("date", to_date));
       	List results = cr.list();
       	return results;
       }
    
    
    @SuppressWarnings("rawtypes")
   	public <T> List fetchactivitydaterange(String from_date, String to_date) {   
       	Criteria cr = sessionFactory.getCurrentSession().createCriteria(Activity.class);
       	System.out.println("Hibernate Util from date : " + from_date);
       	System.out.println("Hibernate Util to date : " + to_date);
       	
       	cr.add(Restrictions.ge("logdate", from_date)); 
       	cr.add(Restrictions.le("logdate", to_date));
       	List results = cr.list();
       	return results;
       }
    
    
    @SuppressWarnings("rawtypes")
   	public <T> List fetchinvoicebydaterange(String from_date, String to_date) {   
       	Criteria cr = sessionFactory.getCurrentSession().createCriteria(Invoice.class);
       	System.out.println("Hibernate Util from date : " + from_date);
       	System.out.println("Hibernate Util to date : " + to_date);
       	
       	cr.add(Restrictions.ge("purchasedate", from_date)); 
       	cr.add(Restrictions.le("purchasedate", to_date));
       	List results = cr.list();
       	return results;
       }
    
    @SuppressWarnings("rawtypes")
   	public <T> List fetchsalesbydaterange(String from_date, String to_date) {   
       	Criteria cr = sessionFactory.getCurrentSession().createCriteria(Sales.class);
       	System.out.println("Hibernate Util from date : " + from_date);
       	System.out.println("Hibernate Util to date : " + to_date);
       	
       	cr.add(Restrictions.ge("purchasedate", from_date)); 
       	cr.add(Restrictions.le("purchasedate", to_date));
       	List results = cr.list();
       	return results;
       }
    
    
    @SuppressWarnings("rawtypes")
	public <T> List fetchprofitSum(Class<T> entityClass) {   
    	Criteria cr = sessionFactory.getCurrentSession().createCriteria(Invoice.class);

    	cr.setProjection(Projections.sum("profit"));
    	List results = cr.list();    
    	return results;
    }
    
    @SuppressWarnings("rawtypes")
	public <T> List fetchoverallprofitbydate(Class<T> entityClass) {   
    	Criteria cr = sessionFactory.getCurrentSession().createCriteria(Invoice.class);
    	
    	cr.setProjection(Projections.projectionList()
                .add(Projections.sum("totalsalesprice"))
                .add(Projections.sum("totalunitprice"))
                .add(Projections.sum("profit"))
                .add(Projections.groupProperty("purchasedate")));
    	
    	List results = cr.list();   
    	return results;
    }
    
    
    @SuppressWarnings("rawtypes")
  	public <T> List getinvoicedetails(String invoicenumber) {   
      	Criteria cr = sessionFactory.getCurrentSession().createCriteria(Invoice.class);
      	System.out.println("Hibernate invoicenumber : " + invoicenumber);
      	cr.add(Restrictions.eq("invoicenumber", invoicenumber));
      	List results = cr.list();
      	return results;
      }
    
    
    @SuppressWarnings("rawtypes")
  	public <T> List fetchprofitbydaterange(String from_date, String to_date) {   
      	Criteria cr = sessionFactory.getCurrentSession().createCriteria(Invoice.class);
      	
      	cr.setProjection(Projections.projectionList()
                  .add(Projections.sum("totalsalesprice"))
                  .add(Projections.sum("totalunitprice"))
                  .add(Projections.sum("profit"))
                  .add(Projections.groupProperty("purchasedate")));
      	cr.add(Restrictions.ge("purchasedate", from_date)); 
       	cr.add(Restrictions.le("purchasedate", to_date));
      	
      	List results = cr.list();   
      	return results;
      }
    
    @SuppressWarnings("rawtypes")
   	public <T> List fetchTodayprofit(Class<T> entityClass) {   
       	Criteria cr = sessionFactory.getCurrentSession().createCriteria(Invoice.class);

       //	cr.setProjection(Projections.sum("profit"));
       	Date time1 = new Date( );
	      SimpleDateFormat ft1 =new SimpleDateFormat ("yyyy.MM.dd");
	    //cr.setProjection(Projections.sum("profit",ft1.format(time1)));
       	List results = cr.list();    
       	return results;
       }
    
    @SuppressWarnings("unchecked")
	public <T> T fetchById(Serializable id, Class<T> entityClass) {
        return (T)sessionFactory.getCurrentSession().get(entityClass, id);
    }
    
    @SuppressWarnings("unchecked")
   	public <T> T fetchByUsername(Serializable username, Class<T> entityClass) {
           return (T)sessionFactory.getCurrentSession().get(User.class, username);
       }
    
   
    
	
}
